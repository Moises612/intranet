<?php
  
  class DB{
    public $DBconnect;
    private $hostName = "localhost";
    private $userName = "root";
    private $userPass = "";
    private $DBName = "db_intranet";

    public function __construct(){
      $this->DBConnection();
    }

    public function DBConnection(){
      try{
        $this->DBconnect = new PDO('mysql:host='.$this->hostName.';dbname='.$this->DBName,$this->userName,$this->userPass);
      }catch(PDOException $e){
        echo 'Conexion Fallida'.$e->getMessage();
      }
    } 
    
    public function select($sql){
      $stmt = $this->DBconnect->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    

  }

?>
