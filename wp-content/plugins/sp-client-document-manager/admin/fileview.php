<?php
class sp_cdm_fileview {
	
	
function view(){
	
		global $wpdb;
	
	echo '
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("select").select2();
	
		
		
		
	});
	</script>
	';
	
	$dropdown =  '
	
	
	<h2>'.__("User Files","sp-cdm").'</h2>'.sp_client_upload_nav_menu().''.__("Choose a user below to view their files","sp-cdm").'<p>
	<div style="margin-top:20px;margin-bottom:20px">
	<script type="text/javascript">
	jQuery(document).ready(function() {

	jQuery("#user_uid").change(function() {
		jQuery.cookie("pid", 0, { expires: 7 , path:"/" });
		jQuery.cookie("uid", jQuery("#user_uid").val(), { expires: 7 , path:"/" });
	window.location = "admin.php?page=sp-client-document-manager-fileview&id=" + jQuery("#user_uid").val();
	
	
	})
	});
	</script>
	<form>';
	$dropdown .= 	wp_dropdown_users(array('name' => 'user_uid', 'show_option_none' => 'Choose a user', 'selected'=>sanitize_text_field(cdm_var('id')),'echo'=>false));
	 
	$dropdown .=  '</form></div>';

	echo apply_filters('sp_cdm/admin/fileview/dropdown', $dropdown);
		if(cdm_var('id') != ''){
			
			
			echo do_shortcode('[sp-client-document-manager uid="'.sanitize_text_field(cdm_var('id')).'"]');
			
		}



}
	
}

?>