jQuery(document).ready(function () {
	
	//show and hide instructions
    jQuery("#auth_help").click(function () {
        jQuery("#auth_troubleshoot").toggle();
    });
	jQuery("#conn_help").click(function () {
        jQuery("#conn_troubleshoot").toggle();
    });
	
	jQuery("#conn_help_user_mapping").click(function () {
        jQuery("#conn_user_mapping_troubleshoot").toggle();
    });
	
	//show and hide attribute mapping instructions
    jQuery("#toggle_am_content").click(function () {
        jQuery("#show_am_content").toggle();
    });

	 //Instructions
    jQuery("#help_curl_title").click(function () {
    	jQuery("#help_curl_desc").slideToggle(400);
    });

    jQuery("#help_ldap_title").click(function () {
    	jQuery("#help_ldap_desc").slideToggle(400);
    });
    
    jQuery("#help_ping_title").click(function () {
    	jQuery("#help_ping_desc").slideToggle(400);
    });

    jQuery("#help_selinuxboolen_title").click(function () {
        jQuery("#help_selinuxboolen_desc").slideToggle(400);
    });
    
    jQuery("#help_invaliddn_title").click(function () {
    	jQuery("#help_invaliddn_desc").slideToggle(400);
    });
    
    jQuery("#help_invalidsf_title").click(function () {
    	jQuery("#help_invalidsf_desc").slideToggle(400);
    });
    
    jQuery("#help_seracccre_title").click(function () {
    	jQuery("#help_seracccre_desc").slideToggle(400);
    });
    
    jQuery("#help_sbase_title").click(function () {
    	jQuery("#help_sbase_desc").slideToggle(400);
    });
    
    jQuery("#help_sfilter_title").click(function () {
    	jQuery("#help_sfilter_desc").slideToggle(400);
    });
    
    jQuery("#help_ou_title").click(function () {
    	jQuery("#help_ou_desc").slideToggle(400);
    });
    
    jQuery("#help_loginusing_title").click(function () {
    	jQuery("#help_loginusing_desc").slideToggle(400);
    });
    
    jQuery("#help_diffdist_title").click(function () {
    	jQuery("#help_diffdist_desc").slideToggle(400);
    });
    
    jQuery("#help_rolemap_title").click(function () {
    	jQuery("#help_rolemap_desc").slideToggle(400);
    });
    
    jQuery("#help_multiplegroup_title").click(function () {
    	jQuery("#help_multiplegroup_desc").slideToggle(400);
    });
    
    jQuery("#help_curl_warning_title").click(function () {
    	jQuery("#help_curl_warning_desc").slideToggle(400);
    });
    
    jQuery("#help_ldap_warning_title").click(function () {
    	jQuery("#help_ldap_warning_desc").slideToggle(400);
    });

    if(jQuery('#standard_single_site_radio_button').prop('checked') === true) {
        jQuery('#standard_number_of_subsites_dropdown_div').hide();
        jQuery("#standard_no_of_instances_drop_down").val("1");
        showTotalPrice("standard",0,null);
    }

    if(jQuery('#standard_multi_site_radio_button').prop('checked') === true) {
        jQuery('#standard_number_of_subsites_dropdown_div').show();
        jQuery("#standard_no_of_instances_drop_down").val("1");
        jQuery("#standard_number_of_subsites_dropdown").val("3");
        showTotalPrice("standard",0,0);
    }

    jQuery('#standard_single_site_radio_button').click(function () {
        jQuery('#standard_number_of_subsites_dropdown_div').hide();
        jQuery("#standard_no_of_instances_drop_down").val("1");
        showTotalPrice("standard",0,null);
    });

    jQuery('#standard_multi_site_radio_button').click(function () {
        jQuery('#standard_number_of_subsites_dropdown_div').show();
        jQuery("#standard_no_of_instances_drop_down").val("1");
        jQuery("#standard_number_of_subsites_dropdown").val("3");
        showTotalPrice("standard",0,0);
    });

    jQuery('#standard_no_of_instances_drop_down').change(function () {
        var selected_instance_index = jQuery("#standard_no_of_instances_drop_down").prop('selectedIndex');
        showTotalPrice("standard",selected_instance_index,null);
    });

    jQuery('#standard_number_of_subsites_dropdown').change(function () {
        var selected_instance_index = jQuery("#standard_no_of_instances_drop_down").prop('selectedIndex');
        var selected_subsite_index = jQuery('#standard_number_of_subsites_dropdown').prop('selectedIndex');
        showTotalPrice("standard",selected_instance_index,selected_subsite_index);
    });

    if(jQuery('#enterprise_single_site_radio_button').prop('checked') === true) {
        jQuery('#enterprise_number_of_subsites_dropdown_div').hide();
        jQuery("#enterprise_no_of_instances_drop_down").val("1");
        showTotalPrice("enterprise",0,null);
    }

    if(jQuery('#enterprise_multi_site_radio_button').prop('checked') === true) {
        jQuery('#enterprise_number_of_subsites_dropdown_div').show();
        jQuery("#enterprise_no_of_instances_drop_down").val("1");
        jQuery("#enterprise_number_of_subsites_dropdown").val("3");
        showTotalPrice("enterprise",0,0);
    }

    jQuery('#enterprise_single_site_radio_button').click(function () {
        jQuery('#enterprise_number_of_subsites_dropdown_div').hide();
        jQuery("#enterprise_no_of_instances_drop_down").val("1");
        showTotalPrice("enterprise",0,null);
    });

    jQuery('#enterprise_multi_site_radio_button').click(function () {
        jQuery('#enterprise_number_of_subsites_dropdown_div').show();
        jQuery("#enterprise_no_of_instances_drop_down").val("1");
        jQuery("#enterprise_number_of_subsites_dropdown").val("3");
        showTotalPrice("enterprise",0,0);
    });

    jQuery('#enterprise_no_of_instances_drop_down').change(function () {
        var selected_instance_index = jQuery("#enterprise_no_of_instances_drop_down").prop('selectedIndex');
        showTotalPrice("enterprise",selected_instance_index,null);
    });

    jQuery('#enterprise_number_of_subsites_dropdown').change(function () {
        var selected_instance_index = jQuery("#enterprise_no_of_instances_drop_down").prop('selectedIndex');
        var selected_subsite_index = jQuery('#enterprise_number_of_subsites_dropdown').prop('selectedIndex');
        showTotalPrice("enterprise",selected_instance_index,selected_subsite_index);
    });


    function showTotalPrice(planType,selected_instance_index,selected_subsite_index) {
        var instance_price = [0,60,117,171,223,273,321,366,409,449,530,580,630,680,730,880,1080];
        var subsite_price = [45,60,120,160,200,240,280,320,360,400,440,480,520,999];
        var total_price = 0;
        if (selected_subsite_index === null) {
            total_price = instance_price[selected_instance_index] + (planType === "standard" ? 119 : 199);
        }
        else {
            total_price = instance_price[selected_instance_index] + subsite_price[selected_subsite_index] + (planType === "standard" ? 119 : 199);
        }
        if (planType === "standard") {
            var span = document.getElementById("standard_total_price"),
                text = document.createTextNode('$'+total_price);
            span.innerHTML = ''; // clear existing
            span.appendChild(text);
        }
        else if(planType === "enterprise") {
            var span = document.getElementById("enterprise_total_price"),
                text = document.createTextNode('$'+total_price);
            span.innerHTML = ''; // clear existing
            span.appendChild(text);
        }
    }


});