// JavaScript Document




function cdm_copyToClipboard(url){
	
	window.prompt("Copy to clipboard: Ctrl+C, Enter", url);
}



function sp_cdm_publish_doc(id){
	var data = {
			'action': 'sp_cdm_publish_draft',
			'fid': id,
		};

	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post(cdm_scripts.ajax_url, data, function(response) {
		alert(response);
		cdmCloseModal('file');
		sp_cdm_load_project(0);
	});
}
function cdm_print_div(div,name){

jQuery(div).printElement({pageTitle: name});	
	
}


function sp_cu_dialog_premium_thanks(){
	
	jQuery('#sp_cu_thankyou' ).dialog();	
	
}
function sp_cu_dialog_premium(){
	
	var selected_val = jQuery('#cdm_current_folder').val();
	jQuery('.pid_select option[value="'+selected_val+'"]').attr("selected", "selected");
	jQuery('.uploadifive-button-go').html('<a href="javascript:cdmPremiumValidate();">'+ cdm_scripts.upload_button+ '</a>');
	jQuery('#cdm_upload_premium_form')[0].reset();
//jQuery("#cdm_uploader_premium").uploadify("destroy");
			
			//jQuery('[data-remodal-id=cdm_uploader_premium_document_upload]').remodal();
		
		var inst = jQuery.remodal.lookup[jQuery('[data-remodal-id=premium-upload]').data('remodal')];
		inst.open();
			
}




function cdm_ajax_load_clients(div){
	
	jQuery.ajax({					
				type: "POST",
				url: ajaxurl,
				data: {'action': 'cdm_ajax_load_clients'},		
				success: function (msg) { 
					
					jQuery(div).html(msg);
				}
		
			});	
	
}
function sp_cdm_check_categories(){
	
		var queue_count = 	jQuery('#cdm_pluploader').plupload('getUploader').total.queued;
		var cat_id = jQuery('.sp-category-select').val();
		var data = {action:'sp_cdm_check_categories', 'cat_id':cat_id ,'folder_id':jQuery.cookie("pid"),'count': queue_count,'uid': sp_vars.uid};
		console.log(data);
		jQuery.ajax({					
				type: "POST",
				url: sp_vars.ajax_url,
				data: data,		
				success: function (msg) { 
					var obj = jQuery.parseJSON(msg);
					console.log(obj);
					if(obj.can_upload == 0){
					jQuery('.cat-upload-message').html('<span style="color:red">' + obj.message + '</span>');
					jQuery('.can-upload-cat').val('0');
						
					}else{
					jQuery('.can-upload-cat').val('1');
					jQuery('.cat-upload-message').html('');
				
					}
				}
		
			});		
	
}

jQuery( document ).ready(function($) {
	
	
	
	$('.plupload_start').on("click",function(){
	var uploader = jQuery('#cdm_pluploader').plupload('getUploader');	
	uploader.settings.multipart_params["convert_to_pdf"] =  jQuery('.convert_to_pdf').is(":checked");
	});
	
	$('.convert_to_pdf').on("click",function(){
		
		if($(this).is(":checked")){
		
			$(this).attr("data-convert-pdf",1);	
		}else{
			$(this).attr("data-convert-pdf",0);	
		}
		
	});
	
	$(".sp-cdm-search-form input:checkbox").on("click",function(){
		
		cdm_ajax_search();					
	});
	
	
	
	$(".sp-cdm-search-form select").on("change",function(){
		
		cdm_ajax_search();					
	});
	
	 $( "#search_from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#search_to" ).datepicker( "option", "minDate", selectedDate );
		if($( "#search_from" ) &&  $( "#search_to" )){
		cdm_ajax_search();	
		}
      }
    });
    $( "#search_to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#search_from" ).datepicker( "option", "maxDate", selectedDate );
		
		if($( "#search_from" ) &&  $( "#search_to" )){
		cdm_ajax_search();	
		}
      }
    });
	
	
	
	
	$(document).on('click','.cdm-edit-file-delete-new-upload', function(e){
		
		$('.cdm-replace-file-val').empty();
		
	return false;	
	});
	
	$(document).on('click','.cdm-toggle-search', function(e){
		
		$(".cdm-advanced-search").toggle();
		console.log($(".cdm_enable_asearch").val());
		
		
		if($(".cdm_enable_asearch").val() == 0 ){

		$(".cdm_enable_asearch").val(1);	
		}else{
			
				$(".cdm_enable_asearch").val(0);	
		
		}
		
		
		return false;
	});
	
	
	
	
	
		new Clipboard('.cdm_clipboard');
		
		jQuery(".cdm-hover-thumb").hover(function() {
			
		
			jQuery(this).before("<div  class=cdm-preview-image><img src=" + jQuery(this).attr("rel") + " ></div>");
		}, function () {
			jQuery(".cdm-preview-image").remove();
    });
	
	
	
	$(document).on('click','.cdmViewFolderLog', function(e){
		
			var pid =  $(this).attr('data-id');
			 $('.cdm-utility-modal').empty();
		
				var data = {
					'action': 'sp_view_folder_log',
					'pid':pid
				};

					
				jQuery.post(cdm_scripts.ajax_url, data, function(response) {
					
					$('.cdm-utility-modal').html(response);
					cdmOpenModal('cdm-utility');
					
				});
		
		
		return false;
	});
	
	
	

	
		$(document).on('click','.cdm-r-toolbox-link', function(e){
				
				var file = $(this).attr('data-id');
				var tab = $(this).attr('tab-id');
				
				
				jQuery(".view-file-content").empty();
				
				jQuery.get(sp_vars.ajax_url, {action: "cdm_view_file", id: file}, function(response){
						jQuery(".view-file-content").html(response);
						 jQuery(".cdm-modal").remodal({ hashTracking: false});	
						 var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id=file]").data("remodal")];
						 inst.open();
						 
						 
						 
						 
						 			 
							
							if(tab){
			
									var active_tab = tab; 
										
									}else{
										
									var active_tab = 0;	
									}
							
							
							
							var tab_id = 0;
						
							
							$( ".viewFileTabs li" ).each(function(item) {
									
								if($('a',this).attr('href') == tab){
									
								
									tab_id =  item;
								
								}
								
							});
							
						
						
							 $.cookie("viewfile_tab", tab_id, { expires: 7 , path:"/" }); 
							  cdm_refresh_file_view(file);
						 
						 
						 
						 
				})
			 
		return false;
	
	
		});
	
	
	
	
	
	
	
		$(document).on('click','.show-most-popular-files', function(e){
		
		
				sp_cdm_loading_image();
				var data = {
					'action': 'sp_top_downloaded_files_button',
					'uid': $(this).attr('data-uid')
				};

	
				jQuery.post(cdm_scripts.ajax_url, data, function(response) {
					
					$('#cdm-responsive-view').html(response);
				});
		
				return false;
		
		
	});
	
	
	
	$(document).on('click','.show-recently-uploaded-files', function(e){
		
		
				sp_cdm_loading_image();
				var data = {
					'action': 'sp_latest_files_button',
					'uid': $(this).attr('data-uid')
				};

	
				jQuery.post(cdm_scripts.ajax_url, data, function(response) {
					
					$('#cdm-responsive-view').html(response);
				});
		
				return false;
		
		
	});
	
	$(document).on('click', '.cdm-project-order', function (e) {
	
		var data = {
			'action': 'cdm_admin_change_project_order',
			'order_id': $(this).attr('data-id'),
			'direction': $(this).attr('data-direction'),
			'order_current': $(this).attr('data-order'),
		};

	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post(cdm_scripts.ajax_url, data, function(response) {
							
			jQuery.post(cdm_scripts.ajax_url, {'action': 'cdm_admin_change_project_order_load'}, function(view) {
				
				$('.sortable-wrapper').html(view);
				});
							
	});
	
	
	return false;
	
});
	
	
	
	$(document).on('change', '.sp-category-select', function (e) {
	
	sp_cdm_check_categories();
	cdmPremiumValidate();
	cdmPremiumValidate();
	});

$(document).on('closed', '.remodal', function (e) {

  // Reason: 'confirmation', 'cancellation'
 
  if($(e.target).hasClass("gdocs-preview")){
  var file_id = $(e.target).attr("data-parent");
  $(".gdocs-preview-container").html('');
  $(".gdocs-preview").attr('data-id', '');
  $(".gdocs-preview").attr('data-parent', '');
  cdmViewFile(file_id);
  }
});






	$( document ).on( "click", ".doc-preview-item, .doc-preview-item-standalone", function() {
		
		var file_id = $(this).attr('data-id');
		var parent_id = $(this).attr('data-parent');
		var clicked = $(this);
		
		if(clicked.hasClass('doc-preview-item-standalone')){
			
		var modal_id = 'gdocs_preview_standalone';	
		var modal_class = '.gdocs-preview-standalone';
		var modal_content = '.gdocs-preview-container-standalone';
		}else{
		var modal_id = 'gdocs_preview';	
		var modal_class = '.gdocs-preview';
		var modal_content = '.gdocs-preview-container';
		}
		$(modal_content).empty();
		$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_gdocs_url_enable',
					   'file_id': file_id
					  },		
				success: function (msg) { 
					
					var obj = $.parseJSON(msg);
						
					if(obj.error != ''){
					alert(obj.error);	
					}else{
					cdmOpenModal(modal_id);
					
					jQuery(".cdm-modal").remodal({ hashTracking: false});	
					var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id="+modal_id+"]").data("remodal")];
					inst.open();
					console.log(obj.file_id);	
					$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_gdocs_preview_container',
					   'file_id': obj.file_id,
					   'fid': file_id
					  },		
				success: function (msg) { 
					$(modal_content).html(msg);
					$(modal_class).attr('data-id', file_id);
					$(modal_class).attr('data-parent', parent_id);
					console.log(parent_id);
				}
					});
					
					
					}
				}
		
			});	
		
		return false;
		
	});
	
	 $('.cdm-tags').tagsInput({width:'auto'});
	
		$( document ).on( "click", ".cdm_edit_file_button", function() {
		var fid = $(this).closest("form").attr('data-id');
		var form = $(this).closest("form");
		$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: form.serialize(),		
				success: function (msg) { 
					
						var obj = $.parseJSON(msg);
						
					if(obj.error != ''){
					alert(obj.error);	
					}else{	
					alert(obj.message);
					cdm_refresh_file_view(fid );
					cdm_ajax_search();
					}
				}
		
			});	
		 
	return false;
	});
	
	
	
	
	$( document ).on( "click", ".cdm_share_email_form_save", function() {
		var fid = $(this).attr('data-id');
		$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_view_file_share_send_email',
					   'email_name': jQuery(".cdm_share_email_form_name").val(), 
					   'email_email': jQuery(".cdm_share_email_form_email").val(), 
					    'email_message': jQuery(".cdm_share_email_form_message").val(), 
					   'fid': fid
					  },		
				success: function (msg) { 
					
						var obj = $.parseJSON(msg);
						
					if(obj.error != ''){
					alert(obj.error);	
					}else{
					jQuery(".cdm_share_email_form_name").val('');
					jQuery(".cdm_share_email_form_email").val('');
					jQuery(".cdm_share_email_form_message").val('');
					alert(obj.message);
					}
				}
		
			});	
		 
	return false;
	});
		
	$( document ).on( "click", ".cdm_set_share_length", function() {
		 
		 
		 	$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_view_file_share_save',
					   'cdm_set_share_length': jQuery("#cdm_share_length").val(), 
					   'fid': jQuery("#cdm_share_length").attr('data-id'),
					   'share-password': jQuery("#cdm_share_password").val(),
					  },		
				success: function (msg) { 
					
					cdm_refresh_file_view(jQuery("#cdm_share_length").attr('data-id'));
				}
		
			});	
		 
	return false;
	});
		
	
	
	
	
	// User: Filter Group
		$( document ).on( "click", ".sp-cdm-load-your-files", function() {
					
								$.removeCookie('pid', { path: '/' }); 
								$.removeCookie('cdm_group_id', { path: '/' }); 
								$.removeCookie('cdm_client_id', { path: '/' }); 
					
					$('.sp-cdm-load-client-group-files').removeClass('selected_group');
					$('.sp-cdm-load-client-files').removeClass('selected_group');
					
					cdm_ajax_search();
		
		return false;
		});
	
		// User: Filter Group
		$( document ).on( "click", ".sp-cdm-load-client-files", function() {
								$.removeCookie('pid', { path: '/' }); 
								$.removeCookie('cdm_group_id', { path: '/' }); 
									sp_cdm_load_project(0)
								cdm_check_file_perms(0);
								cdm_check_folder_perms(0);
					
					$.cookie('cdm_client_id', $(this).attr('data-id'), { expires: 7, path: '/' });
					$('.sp-cdm-load-client-files').removeClass('selected_group');
					$('.sp-cdm-load-client-group-files').removeClass('selected_group');
					$(this).addClass('selected_group');
				
					cdm_ajax_search();
		
		return false;
		});
	
	
	
		// add client	
		$( document ).on( "submit", ".sp_cu_client_form", function() {
	
			$.ajax({					
				type: "POST",
				url: ajaxurl,
				data: $('.sp_cu_client_form').serialize(),		
				success: function (msg) { 
					var obj = $.parseJSON(msg);
					if(obj.error != ''){
					alert(obj.error);
					}else{
					window.location = 'admin.php?page=sp-client-document-manager-clients&function=edit&id=' + obj.id;
					}
				}
		
			});		
		
		return false;
		});
	
	// add client User	
		$( document ).on( "submit", ".cdm_ajax_add_client_user", function() {
	
			$.ajax({					
				type: "POST",
				url: ajaxurl,
				data: $('.cdm_ajax_add_client_user').serialize(),		
				success: function (msg) { 
					var obj = $.parseJSON(msg);
				if(obj.error != ''){
					alert(obj.error);
					}else{
				window.location = 'admin.php?page=sp-client-document-manager-clients&function=edit&id=' + obj.gid;
					}
				}
		
			});		
		
		return false;
		});
		// delete client user
		$( document ).on( "click", ".cdm_ajax_delete_client_user", function() {
				
			$.ajax({					
				type: "POST",
				url: ajaxurl,
				data: {'action' : 'cdm_ajax_delete_client_user','id' : jQuery(this).attr('data-id')},		
				success: function (msg) { 
					var obj = $.parseJSON(msg);
					if(obj.error != ''){
					alert(obj.error);
					}else{
					window.location = 'admin.php?page=sp-client-document-manager-clients&function=edit&id=' + obj.id;
					}
				}
		
			});		
		
		return false;
		});
		
	
		
		
		//Remove Group
		$( document ).on( "click", ".cdm_ajax_delete_client", function() {		
			
			var r = confirm("Are you sure you want to delete this Client? This will also remove all groups and remove connections with users.");
			if (r == true) {			
			
			$.ajax({					
				type: "POST",
				url: ajaxurl,
				data: {'action': 'cdm_ajax_delete_client','id': $(this).attr('data-id')},		
				success: function (msg) { 
					var obj = $.parseJSON(msg);
					if(obj.error != ''){
					alert(obj.error);
					}else{
					
					cdm_ajax_load_clients("#sp_cdm_clients_list");
					
					
					}
				}
		
			});	
			}
		
		return false;
		});
		
		$(document).on('click','.cdm_add_zip_to_file', function(e){
		
		
				
				if ( $('#cdm_pluploader_add_file').css('display') == 'none' ){
   				$('.cdm_pluploader_add_file_dialog').show();
				$('#cdm_pluploader_add_file').show();
				$('#view_file_refresh').hide();
			
				}else{
				$('.cdm_pluploader_add_file_dialog').hide();
				$('#cdm_pluploader_add_file').hide();
				$('#view_file_refresh').show();	
				}
				
				
				return false;
		
		
	});
		
		
	$( document ).on( "click", ".cdm-add-to-cart", function() {
		 
		 	var file_id = jQuery(this).attr('data-id');
		 
		 	$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_woocommerce_add_to_cart',
					   'file_id': file_id, 
					  },		
				success: function (msg) { 					
					obj = jQuery.parseJSON( msg );
					jQuery('.view-file-content').html('<div class="modal-error"><h2>'+obj.cart_message+'</h2><a href="'+obj.cart_url+'" class="cdm_button">View Cart</a> <a href="" class="cdm_button cdm-refresh-file" data-id="'+ obj.file_id+'">Back to file</a> </div>');
					//cdm_refresh_file_view(file_id);
					
				}
		
			});	
		 
	return false;
	});	
		
	$( document ).on( "click", ".cdm-refresh-file", function() {
		var file_id = jQuery(this).attr('data-id');
		cdm_refresh_file_view(file_id);
		return false;
	});
	$( document ).on( "click", ".cdm-refresh-files", function() {
		cdm_ajax_search();
		cdmCloseModal('cdm-woocommerce-message');
		return false;
	});
	
	
	
	$( document ).on( "click", ".cdm-view-file", function() {
		var file_id = jQuery(this).attr('data-id');
		console.log(file_id);
		
		 cdmViewFile(file_id);
		return false;
	});
	
	
	$('.sp-cdm-project-login').on('submit', function(){
		
		var formdata = $(this).serialize();
		$.ajax({					
				type: "POST",
				url: cdm_scripts.ajax_url,
				data: {'action': 'cdm_ajax_cdm_login',
					   'post':formdata, 
					  },		
				success: function (msg) { 					
					obj = jQuery.parseJSON( msg );
					console.log(obj);
					
					if(obj.error == 0){
					$('.sp-cdm-project-login-message').html(obj.message);	
					window.location.reload();
											
					}else{
					$('.sp-cdm-project-login-message').html('<div class="sp-cdm-project-login-error">' + obj.message + '</div>');		
					}
					
				}
		
			});	
		 
	
	
	return false;
	});
	 
	
});