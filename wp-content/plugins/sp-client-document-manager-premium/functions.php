<?php


function sp_premium_is_used(){
	global $post;
	$used = false;
	$page = $_GET['page'];
	$admin_pages = array('sp-client-document-manager',
						  'sp-client-document-manager-settings',
						  'sp-client-document-manager-vendors',
						  'sp-client-document-manager-projects',
						  'sp-client-document-manager-user-logs',
						  'sp-client-document-manager-local-import',
						  'sp-client-document-manager-fileview',
						  'sp-client-document-manager-help',
						  'sp-client-document-manager-recycle',
						  'sp-client-document-manager-groups',
						  'sp-client-document-manager-forms',
						  'sp-client-document-manager-categories',
						  'sp-client-document-manager-security',
						  'sp-client-document-manager-groups-addon',
						  'sp-client-document-manager-forms-manager',
						  'sp-client-document-manager-forms-manager-category', 
						  'sp-client-document-manager-clients',
						  'sp-client-document-manager-logs',
						  'sp-client-document-manager-advanced-groups'
						 );
						 
				if(is_admin()){
						
					if (strpos(	$page , 'sp-client-document-manager') !== false) {
					$used = true;
					}
				}else{
					
				$shortcodes = array('cdm-project','cdm-link','sp-client-media-manager','sp-client-document-manager','cdm_public_view');
					foreach($shortcodes as $shortcode){
					if (strpos(	$post->post_content ,$shortcode) !== false) {
					$used = true;
					}	
					}
					
				}
				
				
				return $used ;		 
	
}

function cdm_limit_filename($input){

$maxLen = 35;

if (strlen($input) > $maxLen) {
    $characters = floor($maxLen / 2);
    return substr($input, 0, $characters) . '...' . substr($input, -1 * $characters);
}	
	
}
function cdm_get_download_date($file_id){
	
	global $wpdb;
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d ", $file_id), ARRAY_A);
	
	if($r[0]['last_downloaded'] == '0000-00-00 00:00:00'){
		
	return __('Not downloaded yet.','sp-cdm')	;
	}else{
		
	return date("F jS Y g:i A", strtotime($r[0]['last_downloaded']));	
	}
	
	
}
	

function cdm_get_modified_date($file_id){
	
	global $wpdb;
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d ", $file_id), ARRAY_A);
	
	if($r[0]['last_modified'] == '0000-00-00 00:00:00'){
	return $r[0]['date'];	
	}else{
		
	return $r[0]['last_modified'];	
	}
	
	
}
	
function cdm_upate_download_date($file_id){
	global $wpdb;
	
	
	$update['last_downloaded'] = current_time( 'mysql' );;	
	$update['last_viewed'] =  current_time( 'mysql' );
	$where['id'] = $file_id;
	$wpdb->update($wpdb->prefix . "sp_cu",$update,$where);
	
	
}
function cdm_upate_modified_date($file_id){
	global $wpdb;
	
	
	$update['last_modified'] = current_time( 'mysql' );;	
	$update['last_viewed'] =  current_time( 'mysql' );
	$where['id'] = $file_id;
	$wpdb->update($wpdb->prefix . "sp_cu",$update,$where);
	
	
}

	
	
	function cdm_get_file_owner($file_id){
		
		global $wpdb;
			$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d ", $file_id), ARRAY_A);
		if($r == false){
		return false;	
		}else{
			
		return get_userdata($r[0]['uid']);
			
		}
		
	}
	
	
	if(!function_exists('cdm_get_category')){
	
	
	
	function cdm_add_file_to_user($file,$user_id,$folder_id =0,$type='url'){
		global $wpdb;
		
					
					
					$file_contents = file_get_contents($file);
						
					
					
					
					
					if($type == 'url'){						
						$parsed = parse_url($file);
						$filename = basename($parsed['path']);						
					}else{
					 $filename = basename($file);	
					}
					 
					
					
					 $dir = ''.SP_CDM_UPLOADS_DIR.''.$user_id.'/';						
					 if (!is_dir($dir)) {
						 mkdir($dir, 0777);
					 }
					
					$filename 	 = apply_filters('sp_cdm/premium/upload/file_name',$filename,$user_id);
					if(sp_cdm_is_featured_disabled('premium', 'file_sanitization') == false){
					$filename    = strtolower($filename);
					$filename    = sanitize_file_name($filename);
					}
					$filename    = remove_accents($filename);
					
					$filename 	 = apply_filters('sp_cdm/premium/upload/file_rename',$filename,$user_id);
					
					
					file_put_contents($dir.''. $filename  ,$file_contents );
		
					$insert['uid'] = $user_id;
					$insert['pid'] = $folder_id;
					$insert['file'] = $filename ;
					$insert['name'] = $filename ;					
					$insert['date'] = current_time( 'mysql' );
					
				
					
										
					$insert_id = $wpdb->insert(  "".$wpdb->prefix."sp_cu", $insert);	
					$insert['insert_id'] = $insert_id;
			
		return $insert;
		
		
	}
	
	function cdm_find_latest_revision_id($file_id){
		
		global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where parent = %d order by id desc", $file_id), ARRAY_A);
		if($r == false){
		return $file_id;
		}else{
		return $r[0]['id'];	
		}
		
	
	}
	function cdm_find_latest_revision($file_id){
		
		global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where parent = %d order by id desc", $file_id), ARRAY_A);
		if($r == false){
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d", $file_id), ARRAY_A);
		}
		
		return $r;
	}
	function cdm_revision_file_link($file_id,$type,$ext=NULL,$file_types= NULL,$original = false){
		$showoriginal = '';
		if($original == true){
			
		$showoriginal = '&original=1';	
		}
		if(get_option('sp_cu_file_direct_access') == 1){
		return 	admin_url( 'admin-ajax.php?cdm-download-file-id='.$file_id.''.$showoriginal.'' );;	
		}
		
		if($ext != NULL && $file_types != NULL){
	   if ($type == 1 or in_array($ext,$file_types) ) {
           $link .=  'target="_blank" ';
        } else {
           $link .= ' ';
        }
		}
		
		$url = 	admin_url( 'admin-ajax.php?cdm-download-file-id='.$file_id.''.$showoriginal.'' );
		$link .= ' href="'.$url.'" ';
		if(get_option('sp_cu_js_redirect') == 1){
		$link .=' target="_blank"';	
		}
		$link = apply_filters('cdm/file/link',$link, $file_id,$type,$ext,$file_types);
		return $link;
}
	
	
	add_action('init','cdm_download_revision');
	function cdm_download_revision(){
		global $wpdb,$current_user;
		
		
		if($_GET['download-file-revision'] != '' && $current_user->ID != ''){
		
		$cdm_download_file = new cdm_download_file;
		$cdm_download_file->download($_GET['download-file-revision']);
		}
		
	}
	
	function cdm_get_image($r,$size=250){
		
	  $ext        = preg_replace('/^.*\./', '', $r['file']);
        $images_arr = array(
            "jpg",
            "png",
            "jpeg",
            "gif",
            "bmp"
        );
      
	  
	  
			if(get_option('sp_cu_user_projects_thumbs_pdf') == 1 && class_exists('imagick')){
	
			$info = new Imagick();
			$formats = $info->queryFormats();
			
			}else{
				$formats = array();
			}
	  
	  
	  
	    if (in_array(strtolower($ext), $images_arr)) {
            if (get_option('sp_cu_overide_upload_path') != '' && get_option('sp_cu_overide_upload_url') == '') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png">';
            } else {
				
                $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r['uid'] . '/' . $r['file'] . '', $size) . '">';
            }
        } elseif ($ext == 'xls' or $ext == 'xlsx') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_excel.png">';
        } elseif ($ext == 'doc' or $ext == 'docx') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_word.png">';
        } elseif ($ext == 'pub' or $ext == 'pubx') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_publisher.png">';
        } elseif ($ext == 'ppt' or $ext == 'pptx') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_powerpoint.png">';
        } elseif ($ext == 'adb' or $ext == 'accdb') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_access.png">';
        } elseif (in_array(strtoupper($ext),$formats)) {
            if (file_exists('' . SP_CDM_UPLOADS_DIR . '' . $r['uid'] . '/' . $r['file'] . '_big.png')) {
                $img = '<img src="' . SP_CDM_UPLOADS_DIR_URL . '' . $r['uid'] . '/' . $r['file'] . '_big.png" width="'.$size.'">';
            } else {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png">';
            }
        } elseif ($ext == 'pdf' or $ext == 'xod') {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png">';
        } else {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png">';
        }
		
		$img = apply_filters('sp_cdm_viewfile_image', $img,$r);
		
		return $img;	
		
		
	}
	
	function cdm_get_meta($file_id,$meta_key,$single=false,$default = ''){
		global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu_meta   where fid = %d and meta_key = %s", $file_id,$meta_key), ARRAY_A);
		
		if($r == false){
			if($default == ''){
			return false;
			}else{
			return $default;	
			}
		}
		
		if($single == true){
		if(is_serialized($r[0]['meta_value'])){
		return unserialize($r[0]['meta_value']);
		}else{
		return $r[0]['meta_value'];	
		}
		}else{
			for ($i = 0; $i < count($r); $i++) {
			$meta[] = $r[$i]['meta_value'];
			}
		return $meta;	
		}
	}
	
	function cdm_update_meta($file_id,$meta_key,$meta_value){
		global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu_meta   where fid = %d and meta_key = %s ", $file_id,$meta_key,$meta_value), ARRAY_A);
		
		if($r == false){
			
			$insert['meta_value'] = $meta_value;
			$insert['meta_key'] = $meta_key;
			$insert['fid'] = $file_id;
			$wpdb->insert("" . $wpdb->prefix . "sp_cu_meta",$insert);
		}else{
			
			$update['meta_value'] = $meta_value;
			$where['meta_key'] = $meta_key;
			$where['fid'] = $file_id;
			$wpdb->update("" . $wpdb->prefix . "sp_cu_meta",$update,$where);
		}
		
		
	}
	
	function cdm_is_folder_hidden($fid){
		global $wpdb;
		
	$hidden = false;
	
	$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu_project   where id = %d", $fid), ARRAY_A);
	
	if($r[0]['hide'] == 1){
	$hidden = true;	

	}
	
	
	return $hidden;
		
	}
	
	function cdm_is_encrypted($fid){
		global $wpdb;
	
	$r = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = '".$wpdb->escape($fid)."'", ARRAY_A);
	
	$encrypted_file = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'.lock';	

	if(is_file($encrypted_file )){
	
		return true;
		
	}else{
		return false;	
	}
	
	}
	
	
	function cdm_delete_dir($path) {
  if (!is_dir($path)) {
    throw new InvalidArgumentException("$path must be a directory");
  }
  if (substr($path, strlen($path) - 1, 1) != DIRECTORY_SEPARATOR) {
    $path .= DIRECTORY_SEPARATOR;
  }
  $files = glob($path . '*', GLOB_MARK);
  foreach ($files as $file) {
    if (is_dir($file)) {
      cdm_delete_dir($file);
    } else {
      unlink($file);
    }
  }
  rmdir($path);
}
	
	
	
		function cdm_locate_template( $template_name, $template_path = '', $default_path = '' ) {
			
			if ( ! $template_path ) {
				$template_path =SP_CDM_PREMIUM_TEMPLATE_DIR;
			}
		
			if ( ! $default_path ) {
				$default_path = SP_CDM_PREMIUM_PLUGIN_DIR. 'templates/';
			}
		
			// Look within passed path within the theme - this is priority.
			$template = locate_template(
				array(
					trailingslashit( $template_path ) . $template_name,
					$template_name
				)
			);
			
			
			// Get default template/
			if ( ! $template  ) {
				$template = $default_path . $template_name;
			}
				if(file_exists( get_stylesheet_directory() . '/sp-client-document-manager/'.$template_name)){					
					$template = get_stylesheet_directory() . '/sp-client-document-manager/'.$template_name;
				}	
			// Return what we found.
			return apply_filters( 'cdm_locate_template', $template, $template_name, $template_path );
		}
			
		function cdm_get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
			if ( ! empty( $args ) && is_array( $args ) ) {
				extract( $args );
			}
	
			$located = cdm_locate_template( $template_name, $template_path, $default_path );
			
			if ( ! file_exists( $located ) ) {
				echo 'Template not found';
				return;
			}
		
			// Allow 3rd party plugin filter template file from their plugin.
			$located = apply_filters( 'cdm_get_template', $located, $template_name, $args, $template_path, $default_path );
		
			do_action( 'cdm_before_template_part', $template_name, $template_path, $located, $args );
		
			include( $located );
		
			do_action( 'cdm_after_template_part', $template_name, $template_path, $located, $args );
		}
			
	
	
	
	
	
	
	function sp_client_upload_settings_field($label,$name,$type,$disable_features){
		
		
		
		
		echo ' <tr>
 <td >'.$label.'</td><td>
 <input type="checkbox" name="sp_cdm_disable_features['.$type.']['.$name.']"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, $type, $name). '></td>
 </tr>';
 
 
		
	}
	
	function cdm_get_file($file_id){
		global $wpdb;	
		
			$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu WHERE  id  = %d", $file_id), ARRAY_A);	
			return $r;
	}	
	
	function cdm_get_total_downloads($file_id){
		
		global $wpdb;	
		
			$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_event_log where item_id = %d and log = 'File Downloaded'", $file_id), ARRAY_A);	
			if($r == false){
			return 0;	
			}else{
			return count($r);	
			}
	}
	
	
	function cdm_get_category($id){
		
		global $wpdb;	
		
			$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_cats where id = %d", $id), ARRAY_A);	
			
			
			if($r != false){
				
			return $r[0]['name'];	
			}else{
			return false;	
			}
		
	}
	function cdm_premium_projects_dropdown($class,$selected = false){
		
		
	

	global $wpdb,$current_user;
	
	
	
$html =  apply_filters('wpfh_sub_projects', $select_dropdown ,$selected); 	

	

	

	



	return $html;

		
	}

	function cdm_public_shortcode_url($and){
			global $wpdb;

	 $r = $wpdb->get_results("SELECT * FROM  " . $wpdb->prefix . "posts where post_content LIKE   '%[cdm_public_view]%' and post_type = 'page' and post_status = 'publish'", ARRAY_A);
			
		if($r[0]['ID'] == ""){
		return false;
		}else{
			
			$url = get_permalink( $r[0]['ID'] );	

				if ( get_option('permalink_structure') != '' ) { 
				
				
				return ''.$url.'?'.$and.''	;
				
				
				}else{
					
						return ''.$url.'&'.$and.''	;
					
					
				}
			
					
			
		
		}
	}

function sp_cdm_public_file_link($fid){
			
				
		return ''.get_site_url().'/?sp-cdm-public-link='.base64_encode($fid).'&view=1';		
			
}
function sp_cdm_is_public_mode(){
			global $wpdb;

	 $r = $wpdb->get_results("SELECT * FROM  " . $wpdb->prefix . "posts where post_content LIKE   '%[cdm_public_view]%' and post_type = 'page' and post_status = 'publish'", ARRAY_A);
			
		if($r == false){
			
			return false;
			
		}else{
			
			return true;
		}
	}
function sp_cdm_is_client_attached($client_id){
	
	
}
function sp_cdm_is_project_in_client($project_id,$client_id){
	global $wpdb;
	
	
	
}

function sp_cdm_get_client_id_by_group_id($group_id){
	
	global $wpdb;
	
	$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups where id = %d ", $group_id), ARRAY_A);
	
		return $r[0]['cid'];
	
}
function sp_cdm_get_groups_by_client($client_id){
	
		global $wpdb;
	
	
	$query = "SELECT * FROM  " . $wpdb->prefix . "options where option_name LIKE  'sp_cdm_groups_addon_groups_permission_%'";
								
	   $projects_info = $wpdb->get_results($query, ARRAY_A);
	 
	   	if(count($projects_info) > 0){
				
				 for ($i = 0; $i < count($projects_info ); $i++) {
				$permissions = @unserialize(get_option($projects_info[$i]['option_name']));
		
					foreach($permissions as $permission){
						
						if(sp_cdm_get_client_id_by_group_id($permission) == $client_id){
							
							$new_permissions[] = $permission;
						}
						
						
					}
			
					
				$pbids = explode('_',$projects_info[$i]['option_name']);  
				
				
				
					if(in_array($client_id,$new_permissions) && is_numeric($pbids[6])){
						
						
						$project[] = $pbids[6];
					}
				
					 
				 }
			
		}
		
	
		return $project;
	
	
}
function sp_cdm_get_folders_by_group_id($client_id){
	
	global $wpdb;
	

	
	$query = "SELECT * FROM  " . $wpdb->prefix . "options where option_name LIKE  'sp_cdm_groups_addon_groups_permission_%'";
								
	   $projects_info = $wpdb->get_results($query, ARRAY_A);

	   	if(count($projects_info) > 0){
				
				 for ($i = 0; $i < count($projects_info ); $i++) {
				$permissions = @unserialize(get_option($projects_info[$i]['option_name']));
		#echo '<pre> '.$client_id.'';print_r($permissions);echo '</pre>';
				$pbids = explode('_',$projects_info[$i]['option_name']);  
				
				
				if(in_array($client_id, $permissions) && is_numeric($pbids[6]) ){
					$project[] =$pbids[6] ;
				}
					 
				 }
			
		}

		return $project ;
}



function sp_cdm_get_groups_by_client_assigned($uid,$client_id){
	
	global $wpdb;
	
	$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients_assign where uid = %d AND gid = %d ", $uid,$client_id), ARRAY_A);

		for ($i = 0; $i < count($r ); $i++) {
			
			$clients[] = $r[$i]['gid'];
			
		}
	
		foreach($clients as $client_id){
			
				$r_g = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups where cid = %d ", $client_id), ARRAY_A);
				
					for ($i = 0; $i < count($r_g ); $i++) {
						
						$groups[] = $r_g[$i]['id'];
					}
				
			
		}
		

return $groups;

}




function sp_cdm_get_folders_by_client_assigned($uid){
	
	global $wpdb;
	
	$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients_assign where uid = %d ", $uid), ARRAY_A);

		for ($i = 0; $i < count($r ); $i++) {
			
			$clients[] = $r[$i]['gid'];
			
		}
	
		foreach($clients as $client_id){
			
				$r_g = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups where cid = %d ", $client_id), ARRAY_A);
				
					for ($i = 0; $i < count($r_g ); $i++) {
						
						$groups[] = $r_g[$i]['id'];
					}
				
			
		}
		
			$groups = sp_cdm_array_flatten($groups, array());
		
		foreach($groups as $group){
				
				$projects[] = sp_cdm_get_folders_by_group_id($group);	
					
		 		}
		$projects = sp_cdm_array_flatten($projects, array());
return $projects;

}



function sp_cdm_get_folders_by_client_id($client_id){
	
	global $wpdb;
	

	
	$query = "SELECT * FROM  " . $wpdb->prefix . "options where option_name LIKE  'sp_cdm_groups_addon_clients_permission_%'";
								
	   $projects_info = $wpdb->get_results($query, ARRAY_A);

	   	if(count($projects_info) > 0){
				
				 for ($i = 0; $i < count($projects_info ); $i++) {
				$permissions = @unserialize(get_option($projects_info[$i]['option_name']));
		#echo '<pre> '.$client_id.'';print_r($permissions);echo '</pre>';
				$pbids = explode('_',$projects_info[$i]['option_name']);  
				
				
				if(in_array($client_id, $permissions) && is_numeric($pbids[6]) ){
					$project[] =$pbids[6] ;
				}
					 
				 }
			
		}
	
		return $project ;
}


function sp_cdm_get_folders_by_client($client_id){
	
	global $wpdb;
	
	
	$query = "SELECT * FROM  " . $wpdb->prefix . "options where option_name LIKE  'sp_cdm_groups_addon_clients_permission_%'";
								
	   $projects_info = $wpdb->get_results($query, ARRAY_A);

	   	if(count($projects_info) > 0){
				
				 for ($i = 0; $i < count($projects_info ); $i++) {
				$permissions = @unserialize(get_option($projects_info[$i]['option_name']));
			
				$pbids = explode('_',$projects_info[$i]['option_name']);  
				
				
				
					if(in_array($client_id,$permissions) && is_numeric($pbids[6])){
						
						
						$project[] = $pbids[6];
					}
				
					 
				 }
			
		}
	
	#	$group_projects = sp_cdm_get_groups_by_client($client_id);
		
	#	$merged  = array_merge($project, $group_projects );
		
	#	print_r($merged );
		return $project ;
}
function sp_cdm_group_client($group_id){
	
		global $wpdb;
		
		$groups = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups where id = %d",$group_id), ARRAY_A);
		
		if($groups[0]['cid'] != 0){
			return $groups[0]['cid'] ;	
		}else{
			return false;
		}
}
function sp_cdm_get_files_by_client($client_id){
	
			global $wpdb;
			
			$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu where client_id = %d  order by date desc",$client_id), ARRAY_A);	
				
			return $r;
	
}

function sp_cdm_get_groups_by_uid($uid){
	global $wpdb;
		$clients = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups_assign where uid = %d",$uid), ARRAY_A);	
			 
			 if(count($clients)){
				 for ($i = 0; $i < count( $clients ); $i++) {
					 
					 $clients_array[] = $clients[$i]['gid'];
					 
				 }
			 }
			
	return $clients_array;
	
}
function sp_cdm_get_clients_by_uid($uid){
	global $wpdb;
	

		$clients = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients_assign where uid = %d",$uid), ARRAY_A);	
			 
			 if(count($clients)){
				 for ($i = 0; $i < count( $clients ); $i++) {
					 
					 $clients_array[] = $clients[$i]['gid'];
					 
				 }
			 }
			
	return $clients_array;
	
}
function sp_cdm_get_projects_by_group_id($group_id){
	global $wpdb;
		$groups = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups_assign where gid = %d",$group_id), ARRAY_A);	
			 
			 
			 for ($i = 0; $i < count( $groups ); $i++) {
				 
				 
			 }
			
	
	
}
function sp_cdm_get_groups_by_client_id($client_id){
		global $wpdb;
			
		$groups = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_advanced_groups where cid = %d",$client_id), ARRAY_A);
		 for ($i = 0; $i < count( $groups ); $i++) {
			 
			$groups_array[]  = $groups[$i]['id']; 
		 }
		 
		
		 
		 
		 
		return $groups_array;
	
}
function sp_cdm_get_client_name($client_id){
	
			global $wpdb;
			
				$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients where id = %d ",$client_id), ARRAY_A);		
				
			return stripslashes($r[0]['name']);
	
}
function sp_cdm_advanced_get_clients_by_id($client_id){
				
				global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients_assign where gid = %d ",$client_id), ARRAY_A);	

			for($i=0; $i<count($r); $i++){
					
					
					$clients[] = $r[$i]['uid'];
					
				
			}
	
		return array_values(array_unique($clients));			
		
	}
	
function sp_cdm_get_clients_dropdown($id,$selected,$text = NULL){
				
				global $wpdb;
		if($text == NULL){
		$text = __('Choose A Client','sp-cdm');
		}
		$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_clients",$uid), ARRAY_A);	
		
		$clients = array();
		$h .= '<select name="'.$id.'" class="'.$id.'" id="'.$id.'">';
			if($selected == ''){
			$h .='<option value="">'.$text.'</option>';	
			}
			for($i=0; $i<count($r); $i++){
					
					if($r[$i]['id'] == $selected){
					$selected = 'selected="selected"';
					}else{
					$selected = '';	
					}
					
				$h .='<option value="'.$r[$i]['id'].'">'.$r[$i]['name'] .'</option>';	
				
			}
		$h .='</select>';
		return $h;			
		
	}
function sp_cdm_folder_dropdown_parents($name,$selected){

	

	

	global $wpdb,$current_user;



$html = '';



if(@$_GET['id'] != '' && user_can($current_user->ID,'manage_options')){

	$uid = @$_GET['id'];	

}else{

	$uid = $current_user->ID;

}



if(@$_POST['add-project'] != ""){

	

			$insert['name'] = $_POST['project-name'];

			$insert['uid'] = $uid;
 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
	$wpdb->insert( "".$wpdb->prefix . "sp_cu_project",$insert );

}





if (@CU_PREMIUM == 1){  	

		$find_groups = cdmFindGroups($uid,'_project');

			 }else{
				$find_groups = ''; 
			 }






$r_projects_query = "SELECT *

	

									 FROM ".$wpdb->prefix."sp_cu_project

									WHERE parent = 0

									 ";
					 
  $projects = $wpdb->get_results($r_projects_query, ARRAY_A);	





  if(count($projects) > 0 or get_option('sp_cu_user_projects') == 1 or get_option('sp_cu_user_projects_required') ==1){

	  $html .= ' 
		
';

	
if(get_option('sp_cu_user_projects_required') == 1){
	$requird = ' required';	
	}else{
		$requird = '';	
  }

	$select_dropdown .='

	<select name="'.$name.'" class="'.$name.'" id="'.$name.' >';

	


		for($i=0; $i<count($projects); $i++){

								

		if($selected == $projects[$i]['id'] ){	

			$required = ' selected="selected" '	;

		}else{

			$required = ''	;

		}

		

		if($projects[$i]['name'] != ''){

	 $select_dropdown .='<option value="'.$projects[$i]['id'].'" '.$required.'>'.stripslashes($projects[$i]['name']).'</option>';	

		}

		}

	

	$select_dropdown .='</select>';

	

	$html  .= $select_dropdown;

	



	  

  }



	return $html;

	

}	
function sp_cdm_folder_dropdown($name,$selected){

	

	

	global $wpdb,$current_user;



$html = '';



if($_GET['page'] == 'sp-client-document-manager-fileview'){

	$uid = @$_GET['id'];	

}else{

	$uid = $current_user->ID;

}




if(@$_POST['add-project'] != ""){

	

			$insert['name'] = $_POST['project-name'];

			$insert['uid'] = $uid;
 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
	$wpdb->insert( "".$wpdb->prefix . "sp_cu_project",$insert );

}





if (@CU_PREMIUM == 1){  	

		$find_groups = cdmFindGroups($uid,'_project');

			 }else{
				$find_groups = ''; 
			 }






$r_projects_query = "SELECT *

	

									 FROM ".$wpdb->prefix."sp_cu_project

									WHERE  ( uid = '".$uid ."' ".$find_groups.")  

									 ";
			$r_projects_query = apply_filters('sp_cdm_projects_query_dropdown', $r_projects_query ,$uid);							 
			$r_projects_query .="

										ORDER by name";						 
  $projects = $wpdb->get_results($r_projects_query, ARRAY_A);	





  if(count($projects) > 0 or get_option('sp_cu_user_projects') == 1 or get_option('sp_cu_user_projects_required') ==1){

	  $html .= ' 
		
';

	
if(get_option('sp_cu_user_projects_required') == 1){
	$requird = ' required';	
	}else{
		$requird = '';	
  }

	$select_dropdown .='

	<select name="'.$name.'" class="'.$name.'" id="'.$name.' >';

	


		for($i=0; $i<count($projects); $i++){

								

		if($selected == $projects[$i]['id'] ){	

			$required = ' selected="selected" '	;

		}else{

			$required = ''	;

		}

		

		if($projects[$i]['name'] != ''){

	 $select_dropdown .='<option value="'.$projects[$i]['id'].'" '.$required.'>'.stripslashes($projects[$i]['name']).'</option>';	

		}

		}

	

	$select_dropdown .='</select>';

	

	$html  .= $select_dropdown;

	



	  

  }



	return $html;

	

}
function sp_cdm_add_meta($option_name,$option_value,$post_id,$multi = false){
	
	global $wpdb;
	
	
	$insert['meta_key'] = $option_name;
	$insert['meta_value'] = $option_value;
	$insert['post_id'] = $post_id;
	 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
	$wpdb->insert("".$wpdb->prefix . "sp_rm_rentals_meta", $insert);
	
}




if(!function_exists('set_html_content_type')){
function set_html_content_type() {

	return 'text/html';
}	
}
if(!function_exists('sp_cdm_folder_name')){
function sp_cdm_folder_name($type = 0){
	
	
		if($type == 1){
			
			if(get_option('sp_cu_folder_name_plural') == ''){
			return  __("Folders", "sp-cdm");	
			}else{
			return  stripslashes(get_option('sp_cu_folder_name_plural'));
			}
		}else{
			if(get_option('sp_cu_folder_name_single') == ''){
				return  __("Folder", "sp-cdm");
			}else{
			return  stripslashes(get_option('sp_cu_folder_name_single'));
			
			}
		}
				
	
}
}
function sp_cdm_premium_settings(){
	
	
	global $wpdb;
	
	
	
	
	
	
	$html .= ' <tr>
    <td ><strong>Premium License Key</strong><br><em>License key for premium version</em></td>
    <td><input type="text" name="sp_cdm_premium_license" value="'.get_option('sp_cdm_premium_license').'" style="width:400px"> </td>
  </tr>';
	
	return $html;
}
	}
?>