<?php

$cdmPremiumUploader = new cdmPremiumUploader;
add_action('sp_cdm_premium_scripts',array($cdmPremiumUploader,'scripts'));
add_action('init',array($cdmPremiumUploader,'sessionator'));
add_action('sp_cdm_premium_styles',array($cdmPremiumUploader,'styles'));
add_action('cdm_upload_dialog',array($cdmPremiumUploader,'upload_dialog'));
add_filter('sp_cdm_file_search_query',array($cdmPremiumUploader,'draft_query'));
add_filter('sp_cdm_file_loop_array',array($cdmPremiumUploader,'draft_query_loop'),10,2);


add_action('spdm_file_list_top',array($cdmPremiumUploader,'drafts_folder_file_list'));
add_action('spdm_file_thumbs_top',array($cdmPremiumUploader,'drafts_folder_thumbs'));
add_action('spdm_file_responsive_top',array($cdmPremiumUploader,'drafts_folder_responsive'));


add_filter('sp_cdm_view_file_first_add_button',array($cdmPremiumUploader,'publish_button'),1,2);

	 add_action( 'wp_ajax_sp_cdm_publish_draft',array($cdmPremiumUploader,'sp_cdm_publish_draft'));
    add_action( 'wp_ajax_nopriv_sp_cdm_publish_draft', array($cdmPremiumUploader,'sp_cdm_publish_draft') );
	
	
	add_action( 'wp_ajax_sp_cdm_check_categories',array($cdmPremiumUploader,'sp_cdm_check_categories'));
    add_action( 'wp_ajax_nopriv_sp_cdm_check_categories', array($cdmPremiumUploader,'sp_cdm_check_categories') );
	
	add_filter('sp_cdm/mail/headers', array($cdmPremiumUploader,'email_headers'), 100,5);
class cdmPremiumUploader
{	



	function email_headers($headers,$from,$to,$subject,$message){
		
	

		if(!is_array($headers)){
		$headers = array();	
		}
	
		foreach($headers as $key=>$header){
			
			
			if( strpos( $header, 'From:' ) !== false  or  strpos( $header, 'Reply-to:' ) !== false ) {
				
				unset($headers[$key]);
			}
		}
		
		if(is_object($from)){
		$user_info = get_userdata($from->ID);
		
		#$headers[] = "" . __("From:", "sp-cdm") . " <" .  apply_filters('sp_cdm/mail/admin_email',get_option('admin_email')) . ">";
		#$headers[] = "" . __("Reply-to:", "sp-cdm") . "<" . $user_info->user_email . ">";
	
		$headers[] = 'From: '. apply_filters('sp_cdm/mail/admin_email',get_option('admin_email')).'';
		$headers[] = 'Reply-to: '.$user_info->user_email .'';
		
		}else{
		$headers[] = "" . __("From:", "sp-cdm") . " " . apply_filters('sp_cdm/mail/admin_email',get_option('admin_email')) . "";
		$headers[] = "" . __("Reply-to:", "sp-cdm") . " " . $from . "";	
			
		}
		
		
		return $headers;
		
	}
	
    function styles()
    {
        wp_register_style('sp-cdm-multi-uploader-style', plugins_url('sp-client-document-manager-premium/js/plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css'));
        wp_enqueue_style('sp-cdm-multi-uploader-style');
    }
    function sessionator()
    {
      @session_start();
    }
	function publish_button($html,$r){
		
		if($r[0]['status'] == 1){
		
		$html .='<a  href="javascript:sp_cdm_publish_doc('.$r[0]['id'].')"   style="background-color:green;color:#FFF;padding:5px;margin-right:15px;" > ' . __("Publish Document", "sp-cdm") . '</a> ';	
			
		}
		return $html;
	}
	
	function sp_cdm_check_categories(){
		
		global $wpdb,$current_user;
		
		$cat_id = $_POST['cat_id'];
		$count = 0;
		$count = $_POST['count'];
		$folder_id = $_POST['folder_id'];
		$user_id = $_POST['uid'];


		$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_cats where id = %d", $cat_id), ARRAY_A);	
	
		$data['uid'] = $user_id;
		$data['message'] = __("Category upload available","sp-cdm");
		$data['can_upload'] = 1;
		$data['cat_limit'] = $r[0]['cat_limit'] ;
		$data['cat_limit_type'] = $r[0]['cat_limit_type'] ;
		if($r[0]['cat_limit'] != 0){
			
				
				if($r[0]['cat_limit_type'] == 0){
					$query = $wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu where cid = %d AND uid = %d ", $cat_id,$user_id);
					
				$x = $wpdb->get_results($query, ARRAY_A);		
					
					$total = count($x);
					$final = $count + $total;
				$data['combined_total'] = $final;
					if($total !=0){
						if($final  >=  $r[0]['cat_limit']){
						$data['message'] = __(sprintf("You are only allowed %d file for this %s",$r[0]['cat_limit'], get_option('sp_cu_cat_text')),"sp-cdm");
						$data['can_upload'] = 0;	
						}
					}
					
				}else{
					$x  = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu where cid = %d and pid = %d and uid = %d", $cat_id,$folder_id,$user_id), ARRAY_A);		
				
					$total = count($x);
					$final = $count + $total;
					$data['combined_total'] = $final;
					if($total !=0){
						if($final  >=  $r[0]['cat_limit']){
						$data['message'] = __(sprintf("You are only allowed %d file for this %s and %s",  $r[0]['cat_limit'],get_option('sp_cu_cat_text'),sp_cdm_folder_name()),"sp-cdm");
						$data['can_upload'] = 0;	
						}
					}
					
				}
					$message['total'] = $final;
					$message['limit'] = $r[0]['cat_limit'];
		
		
		}
			
			
			echo json_encode($data);
		
	die();	
	}
	
	function sp_cdm_publish_draft(){
		
		$this->finish($post, $uid,$_POST['fid']);
		
		echo'Published File!';
		die();
		
	}
    function drafts_folder_responsive($pid){
	   global $wpdb,$current_user;
	
	   if($pid == 0 && $pid != 'drafts'){
	
	   	$r = $wpdb->get_results(  "SELECT *  FROM " . $wpdb->prefix . "sp_cu  where status = 1 and uid = ".$current_user->ID." " , ARRAY_A);
	  

	  if(count($r) > 0){
	   echo '<div class="sp-cdm-r-folder" onclick="sp_cdm_load_project(\'drafts\')">';
	   echo '<a onclick="sp_cdm_load_project(\'drafts\')"><img src="' . SP_CDM_PLUGIN_URL . 'images/my_projects_folder.png" width="42" style="opacity: 0.6; filter: alpha(opacity=60); ">' . __("Drafts", "sp-cdm") . ' ('.count($r).')</a>';
	   
	   echo '</div>';
	  }
	   }
	
	   if($pid === 'drafts'){
		echo '<div class="sp-cdm-r-folder" onclick="sp_cdm_load_project(0)">';
		echo '<a href="javascript:sp_cdm_load_project(0)"><img src="' . SP_CDM_PLUGIN_URL . 'images/my_projects_folder.png" width="42"> &laquo; ' . __("Go Back", "sp-cdm") . '</a>';   
		echo '</div>';   
		
	   }
	   
   }
   function get_drafts(){
	   global $wpdb;
	   
	   
   }
    function drafts_folder_thumbs(){
	   
	   
	   
   }
   function drafts_folder_file_list(){
	   
	   
	   
   }
    function draft_query_loop($r,$pid){
		global $wpdb,$current_user;
		 if($pid == 'drafts' && get_option('sp_cu_release_the_kraken') == 1){
			 unset($r);
		  	$r = $wpdb->get_results(  "SELECT *  FROM " . $wpdb->prefix . "sp_cu  where status = 1 and uid = ".$current_user->ID." " , ARRAY_A);
		 }
		return $r;
	}
   function draft_query($search_filter){
	global $current_user;
	  
	
	   if($_GET['pid'] == '0'){
		$search_filter .= " and status = 0 "; 
	   }elseif($_GET['pid'] == 'drafts'){
		 
	   $search_filter = " and status = 1 and uid =  '".$current_user->ID."' ";
	   }else{
		  $search_filter .= " and status = 0 ";    
	   }
	  
	   
	   return $search_filter;
	   
	   
	   
   }
  function translate_scripts(){
	   
	   $cdm_plupload_translations = array(
										 'upload_button' => __( 'Click to upload' ,'sp-cdm'),
										  'ajax_url' => admin_url( 'admin-ajax.php' ),
										  );
	   $cdm_plupload_translations = apply_filters('sp_cdm_translate_scripts',$cdm_plupload_translations);
	   
	   return $cdm_plupload_translations;
   }
   function translate_plupload(){
	   
	   $cdm_plupload_translations = array(
										 'some_string' => __( 'Some string to translate' ,'sp-cdm'),
										 );
	   $cdm_plupload_translations = apply_filters('sp_cdm_translate_plupload',$cdm_plupload_translations);
	   
	   return $cdm_plupload_translations;
   }
   
     function translate_jquery_plupload(){
	   
	   $cdm_plupload_translations = array(
										 'add_files' => __( 'Adjuntar Archivos' ,'sp-cdm'),
										 'filename' => __( 'Filename' ,'sp-cdm'),
										 'status' => __( 'Status' ,'sp-cdm'),
										 'size' => __( 'Size' ,'sp-cdm'),
										 'drag_files' => __( 'Arrastre los archivos aqu' ,'sp-cdm'),
										 'filecount' => __( 'File count error' ,'sp-cdm'),
										 'queued' => __( 'Archivos en cola' ,'sp-cdm'),
										 'error_image_format' => __( 'Image format either wrong or not supported.' ,'sp-cdm'),
										 'error_memory' => __( 'Runtime ran out of available memory.' ,'sp-cdm'),
										 'error_type' => __( 'File: %s' ,'sp-cdm'),
										 'error_size' => __( 'File: %s, size: %d, max file size: %d' ,'sp-cdm'),
									     'error_duplicate' => __( '%s already present in the queue.' ,'sp-cdm'),
										 'error_count' => __( 'Upload element accepts only %d file(s) at a time. Extra files were stripped.' ,'sp-cdm'),										  										 'error_url' => __( 'Upload URL might be wrong or doesn\'t exist.' ,'sp-cdm'),
										 'close_button' => __( 'Close' ,'sp-cdm'),
										  'uploaded_files' => __( 'Subido %d/%d archivos' ,'sp-cdm'), 
										   
										    'uploaded' => __( 'Uploaded' ,'sp-cdm'),
											 'files' => __( 'files' ,'sp-cdm'),
											
										 );
										 
										 
	   $cdm_plupload_translations = apply_filters('sp_cdm_translate_jquery_plupload',$cdm_plupload_translations);
	   
	   return $cdm_plupload_translations;
   }
    function scripts()
    {
		global $sp_client_upload_premium;
        wp_enqueue_script('jquery-form');
		wp_enqueue_script('jquery-ui-widget',array('jquery','jquery-ui-core'));
		wp_enqueue_script('jquery-ui-progressbar',array('jquery','jquery-ui-core'));
			 
		wp_enqueue_script('jquery-clipboard',plugins_url('sp-client-document-manager-premium/js/clipboard.js'),array('jquery'));
		wp_register_script('cdm-plupload', plugins_url('sp-client-document-manager-premium/js/plupload/js/plupload.full.min.js'), array(
            'jquery',
            'jquery-ui-core',
			'jquery-ui-button',
			'jquery-ui-widget',
			 'jquery-form',
			  'sp-cdm-multi-premium-scripts',
        ));
										 
		wp_localize_script( 'cdm-plupload', 'cdm_plupload',$this->translate_plupload());
        wp_enqueue_script('cdm-plupload');
		
		
		wp_register_script('cdm-plupload-jquery', plugins_url('sp-client-document-manager-premium/js/plupload/js/jquery.ui.plupload/jquery.ui.plupload.js'), array(
            'cdm-plupload',
			   'jquery',
            'jquery-ui-core',
			'jquery-ui-button',
			'jquery-ui-widget',
			 'jquery-form',
			  'sp-cdm-multi-premium-scripts',
        ));
        wp_localize_script( 'cdm-plupload-jquery', 'cdm_plupload_jquery',$this->translate_jquery_plupload());
		wp_enqueue_script('cdm-plupload-jquery');
		
	   
	   
	    wp_register_script('sp-cdm-multi-premium-scripts', plugins_url('sp-client-document-manager-premium/js/scripts.js'), array(
            'jquery',           
            'jquery-form',
			  'jquery-ui-core',
        ),$sp_client_upload_premium);
		
		
		wp_localize_script( 'sp-cdm-multi-premium-scripts', 'cdm_scripts',$this->translate_scripts());
		wp_enqueue_script('sp-cdm-multi-premium-scripts');
		
    }
	
	function upload_temp($file){
		
		
		
	}
	
	
	
    function upload($files, $uid,$temp=false,$overwrite=false)
    {
       
        global $wpdb;
		
		

		#print_r($files);
        $current_user = get_userdata($uid);
        
        check_folder_sp_client_upload();
		if($temp == true){
		$dir = '' . SP_CDM_UPLOADS_DIR . '_temp/';	
		}else{
        $dir = '' . SP_CDM_UPLOADS_DIR . '' . $uid . '/';
		}
       	if (!is_dir(SP_CDM_UPLOADS_DIR)) {
            mkdir(SP_CDM_UPLOADS_DIR, 0777);
        }
	    if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }
        if (get_option('sp_cu_limit_file_types') != "") {
            $filetypes = @explode(",", get_option('sp_cu_limit_file_types'));
            #echo $ext;
            $ext = end(explode('.',  $_REQUEST["name"]));
		
            if (!in_array(strtolower($ext),  $filetypes)) {
                echo 0;
                exit;
            }
        }
      
	  
	    $file_real_name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
		
		$filename 	 = apply_filters('sp_cdm/premium/upload/file_name',$file_real_name,$uid);
		if(sp_cdm_is_featured_disabled('premium', 'file_sanitization') == false){
        $filename    = strtolower($filename);
        $filename    = sanitize_file_name($filename);
		}
		$filename    = remove_accents($filename);
		
		$filename 	 = apply_filters('sp_cdm/premium/upload/file_rename',$filename,$uid);
		
		
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
 		
		$target_path = $dir . $filename;
		$temp_target_path = $dir . $_REQUEST["name"].'.part';
		$filePath = $target_path;
 
			 
		
			$out = @fopen($temp_target_path , $chunk == 0 ? "wb" : "ab");
			if ($out) {
			 
			  $in = @fopen($_FILES['file']['tmp_name'], "rb");
			 
			  if ($in) {
				while ($buff = fread($in, 4096))
				  fwrite($out, $buff);
				  
			  } else
				die('{"OK": 0, "info": "Failed to open input stream."}');
			 
			  @fclose($in);
			  @fclose($out);
			 
			  @unlink($_FILES['file']['tmp_name']);
			} else
			  die('{"OK": 0, "info": "Failed to open output stream."}');
			 
			
			
			if (!$chunks || $chunk == $chunks - 1) {
			 
			  rename($temp_target_path , $filePath);
			
			
			
			
			#convert image to PDF if chosen
			if(get_option('sp_cu_convert_images_to_pdf') == 2 or (get_option('sp_cu_convert_images_to_pdf') == 1 && $_REQUEST['convert_to_pdf'] == 'true' )){
			
			$ext = pathinfo($target_path, PATHINFO_EXTENSION);
			$types = explode(",", strtolower(get_option('sp_cu_convert_images_to_pdf_types', 'jpg,jpeg,png,gif,bmp')));
			if(is_array($types)){
				if(in_array(strtolower($ext),$types)){
					
					$convert_file = $target_path;					
					$new_file_path = str_replace('.'.$ext.'', '.pdf',$convert_file);
					$new_file_name = str_replace('.'.$ext.'', '.pdf',$filename);
					$new_file_real_name = str_replace('.'.$ext.'', '.pdf',$file_real_name);
					
					$image = new Imagick($convert_file);
					$image->setImageFormat('pdf');					
					$image->writeImage($new_file_path);
					unlink($convert_file);
					
					$target_path = $new_file_path;
					$filename = $new_file_name;
					$file_real_name = $new_file_real_name;
					
					}
				}
			}	
			#end pdf conversion
			
			
			
			
			
			
			
       	#generate thumbnail for supported types
        $exts = preg_replace('/^.*\./', '', $file_real_name);
       	if(get_option('sp_cu_user_projects_thumbs_pdf') == 1 && class_exists('imagick')){
	
		$info = new Imagick();
		$formats = $info->queryFormats();
		$disable = get_option('sp_cu_image_magick_path_disable_types');
			if($disable != ''){	
				
				if (strpos(strtoupper($disable), strtoupper($exts)) !== false) {
				$dont_convert = 1;	
				}
			
			}
		
			if(in_array(strtoupper($exts),$formats) && $dont_convert  != 1){			
			cdm_thumbPdf($target_path);
			}
		}
		#end thumbnail conversion
		


		if($_POST['upload_only'] == 1){
			
		echo basename($target_path);	
		die();
		}
		
		
		#allow other plugins to hook in
		do_action('sp_cdm_after_file_upload',$target_path,$uid);
        
		
		#end process and save our files for finish
		$current_options = get_option('current_user_upload_' . $uid . '');
        if ($current_options != "") {
            $current_options = @unserialize($current_options);
            array_push($current_options, '' . $filename . '|' . $file_real_name . '');
            $final_current_options = @serialize($current_options);
            update_option('current_user_upload_' . $uid . '', $final_current_options);
        } else {
            $upload[]              = '' . $filename . '|' .  $file_real_name . '';
            $final_current_options = @serialize($upload);
            add_option('current_user_upload_' . $uid . '', $final_current_options);
        }
    }
    }
    function process_email($id, $email)
    {
        global $wpdb;
        $ids   = implode(",", $id);
        $query = "SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id IN ( " . $ids . ")  order by date desc";
      
	    $r     = $wpdb->get_results($query, ARRAY_A);
      	
		$file_list .= '<ul>';
        for ($i = 0; $i < count($r); $i++) {
            $file_list .= '<li><a '. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'>' . $r[$i]['name'] . '</a></li>';
            $file_list_names .= '<li>' . $r[$i]['file'] . '</li>';
		 	 $file_list_real .= '<li>' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '</li>';
			
			if(function_exists('curl_init')){
			 $file_list_viewfile .= '<li><a href="'.sp_cdm_short_url(sp_cdm_file_link($r[$i]['id'])).'">'. __("View File", "sp-cdm") .'</a></li>';
			  $file_list_viewfile_raw .= ''.sp_cdm_short_url(sp_cdm_file_link($r[$i]['id'])).' 
			  ';
			}else{
				 $file_list_viewfile .= '<li><a href="'.sp_cdm_file_link($r[$i]['id']).'">'. __("View File", "sp-cdm") .'</a></li>';
			  $file_list_viewfile_raw .= ''.sp_cdm_file_link($r[$i]['id']).' 
			  ';
			}
			 $file_list_shortcode .= '
<li>'. sp_cdm_file_link($r[$i]['id']).'</li>';
			
		} 
        $file_list .= '</ul>';
        if ($r[0]['pid'] != "") {
            $r_project = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_project   where id = " . $r[0]['pid'] . "", ARRAY_A);
        }
        if ($r[0]['cid'] != "") {
            $r_cats = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_cats   where id = " . $r[0]['cid'] . "", ARRAY_A);
        }
        if ($r[0]['notes'] == '') {
            $notes = stripslashes(sp_cdm_get_form_fields($r[0]['id']));
        } else {
            $notes = stripslashes($r[0]['notes']);
        }
		
		
        $user_info = get_userdata($r[0]['uid']);
        $message   = nl2br($email);
		$message = apply_filters('sp_cdm_shortcode_email_before',$message,$r ,$r_project,$r_cats);	
        $message   = str_replace('[file]', $file_list, $message);
		if(function_exists('curl_init')){
		$message   = str_replace('[file_shortlink]', $file_list_shortcode, $message);
		$message   = str_replace('[file_directory_shortlink]',sp_cdm_short_url(sp_cdm_folder_link($r[0]['pid'])), $message);
		}
		
		$message   = str_replace('[file_directory]',sp_cdm_folder_link($r[0]['pid']), $message);
		
		
		$message   = str_replace('[file_name]', $file_list_names, $message);
		$message   = str_replace('[file_folder]', $file_list_names, $message);
		$message   = str_replace('[file_real_path]', $file_list_real, $message);
		$message   = str_replace('[file_in_document_area]',$file_list_viewfile, $message);
		$message   = str_replace('[file_in_document_area_raw]',$file_list_viewfile_raw, $message);
        $message   = str_replace('[notes]', $notes, $message);
        $message   = str_replace('[user]', $user_info->user_nicename, $message);
		 $message   = str_replace('[uid]', $user_info->ID, $message);
        $message   = str_replace('[project]', stripslashes($r_project[0]['name']), $message);
        $message   = str_replace('[category]', stripslashes($r_cats[0]['name']), $message);
        $message   = str_replace('[user_profile]', '<a href="' . admin_url('user-edit.php?user_id=' . $r[0]['uid'] . '') . '">' . admin_url('user-edit.php?user_id=' . $r[0]['uid'] . '') . '</a>', $message);
        $message   = str_replace('[client_documents]', '<a href="' . admin_url('admin.php?page=sp-client-document-manager') . '">' . admin_url('admin.php?page=sp-client-document-manager') . '</a>', $message);
        $message =  sp_cdm_replace_custom_fields($message,$r[0]);
	    return apply_filters('spcdm/upload/replace_vars',$message,$r[0]);
    }
    function finish($post, $uid,$publish_draft=false)
    {
        global $wpdb;

		if($publish_draft != false){
		 
        $r     = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = ". $publish_draft."", ARRAY_A);
		
		$uid = $r[0]['uid'];
		$a['uid']        = $uid;
        $a['pid']        = $r[0]['pid'];
        $a['cid']        = $r[0]['cid'];
        $a['tags']       = $r[0]['tags'];
		$a['status']       = 0;        
		$a['date'] = $r[0]['date'];	
		
		
		$update['status'] = 0;
		$where['id'] =  $r[0]['id'];
		$wpdb->update("" . $wpdb->prefix . "sp_cu", $update,$where);
		unset($current_user);
		$current_user = get_userdata($uid);	
		$file_ids[0] = $r[0]['id'];
		$file_id = $r[0]['id'];
		$post = $r[0];
		$post['dlg-upload-name'] = $r[0]['name'];
	
		
		}else{
		
		
        $current_user    = get_userdata($uid);
        $a['uid']        = $uid;
        $a['pid']        = $post['pid'];
        $a['cid']        = $post['cid'];
        $a['tags']       = $post['tags'];
		$a['status']       = $post['status'];
        $a['notes']      = $post['dlg-upload-notes'];
		$a['date'] =  date("Y-m-d G:i:s",current_time( 'timestamp' ));

		#check if its a group
		if($_COOKIE['cdm_group_id'] != ''  ){
		$a['group_id'] = $_COOKIE['cdm_group_id'];
			if(sp_cdm_group_client( $_COOKIE['cdm_group_id']) != false){
				$a['client_id'] = sp_cdm_group_client( $_COOKIE['cdm_group_id']);
			}
				
		}
		#check if client is set
		if($_COOKIE['cdm_client_id'] != ''){
		$a['client_id'] = $_COOKIE['cdm_client_id'];	
		}
		
	
	    $current_options = unserialize(get_option('current_user_upload_' . $uid . ''));

	    if ($post['zip'] == 1) {
            $zip = new Zip(true);
            foreach ($current_options as $key => $value) {
                $files    = explode("|", $value);
               	if($key == 0){
				$first_file = $files[0];	
				}
			   
			    $dir_file = '' . SP_CDM_UPLOADS_DIR . '' . $uid . '/' . $files[0] . '';
                $zip->addFile(file_get_contents($dir_file), $files[1], filectime($dir_file));
            }
            $dir = '' . SP_CDM_UPLOADS_DIR . '' . $uid . '/';
            $zip->finalize(); // as we are not using getZipData or getZipFile, we need to call finalize ourselves.
            $return_file = "" . rand(100000, 100000000000) . "_Archive.zip";
            $zip->setZipFile($dir . $return_file);
            $a['file'] = $return_file;
			if($post['dlg-upload-name'] == ''){
			$post['dlg-upload-name'] =  ''.$first_file.' Archive';
			}
            $a['name'] = $post['dlg-upload-name'];
            foreach($a as $key=>$value){ if(is_null($value)){ unset($a[$key]); } }
			$wpdb->insert("" . $wpdb->prefix . "sp_cu", $a);
			
			
			#sp_cdm_short_link($file_id);
            $file_id      = $wpdb->insert_id;
			do_action('sp_cdm_after_file_insert',$a, $file_id ,$post);
            $file_ids[$i] = $file_id;
            process_sp_cdm_form_vars($post['custom_forms'], $file_id);
			cdm_event_log($a['pid'],$a['uid'],'folder','<strong style="color:green">'.sprintf(__('Uploaded File: %s','sp-cdm'), $a['name']).'</strong>');
        } else {
            $i = 0;
			
            foreach ($current_options as $key => $value) {
                $i++;
                $files     = explode("|", $value);
                $a['file'] = $files[0];
                if ($post['og'] == 1) {
                    $a['name'] = $files[1];
                } else {
                    $a['name'] = $post['dlg-upload-name'];
                }
               foreach($a as $key=>$value){ if(is_null($value) || $value == ''){ unset($a[$key]); } }
			    
			
				$wpdb->insert("" . $wpdb->prefix . "sp_cu", $a);
                 
			    $file_id      = $wpdb->insert_id;
				$file_ids[$i] = $file_id;
				 cdm_event_log($a['pid'],$a['uid'],'folder','<strong style="color:green">'.sprintf(__('Uploaded File: %s','sp-cdm'), $a['name']).'</strong>');		 
				
				if(function_exists('curl_init')){
				sp_cdm_short_link($file_id);
				}
				do_action('sp_cdm_after_file_insert',$a, $file_id ,$post);
              
                process_sp_cdm_form_vars($post['custom_forms'], $file_id);
            }
        }
        delete_option('current_user_upload_' . $uid . '');
        add_user_meta($uid, 'last_project', $a['pid']);
        //email below
        
		}
		
		
	
		$to =  apply_filters('sp_cdm/mail/admin_email',get_option('admin_email'));
       		 //start admin email
           
            if (get_option('sp_cu_additional_admin_emails') != "") {
                $cc_admin = explode(",", get_option('sp_cu_additional_admin_emails'));
                foreach ($cc_admin as $key => $email) {
                    if ($email != "") {
                        $pos = strpos($email, '@');
                        if ($pos === false) {
                            $role_emails = sp_cdm_find_users_by_role($email);
                            foreach ($role_emails as $keys => $role_email) {
                                $headers[] = 'Cc: ' . $role_email . '';
                            }
                        } else {
                             $headers[] = 'Cc: ' . $email . ''; 
                        }
                    }
                }
            }
			
			#print_r($file_ids);
            $message = $this->process_email($file_ids, get_option('sp_cu_admin_email'));
      
              $subject =  sp_cu_process_email($file_id, get_option('sp_cu_admin_email_subject'));
           
		 	$message = apply_filters('spcdm_admin_email_message',$message,$post, $uid);
			$to = apply_filters('spcdm_user_admin_to',$to,$post, $uid);
			$subject = apply_filters('spcdm_admin_email_subject',$subject,$post, $uid);
			$attachments = apply_filters('spcdm_admin_email_attachments',$attachments,$post, $uid);
			#$headers = apply_filters('spcdm_admin_email_headers',$headers,$post, $uid);
			$headers = apply_filters('sp_cdm/mail/headers',$headers,wp_get_current_user(),$to,$subject,$message);
			
			
			
			
			 if (get_option('sp_cu_admin_email') != "" && $a['status'] != 1) {
			 add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		     wp_mail($to, stripslashes($subject),stripslashes( $message), $headers, $attachments);
			cdm_event_log($a['pid'],$a['uid'],'folder','<strong>'.sprintf(__('Admin Email Notification Sent: %s','sp-cdm'),$to).'</strong>');
			
			 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
           do_action('sp_cdm_email_send','sp_cu_admin_email',$file_ids,$a, $uid,$to, $subject, $message, $headers, $attachments);
      		  }	
		    unset($headers);
            unset($pos);
		 //end admin email
		
	
  if (get_option('sp_cu_admin_user_email') != "" && $post['admin-uploader'] == 1 && $a['status'] != 1) {
      
					$subject =  sp_cu_process_email($file_id, get_option('sp_cu_admin_user_email_subject'));
                    $message = sp_cu_process_email($file_id, get_option('sp_cu_admin_user_email'));
                    $to      = $current_user->user_email;
                   
			$message = apply_filters('spcdm_user_email_message',$message,$post, $uid);
			$to = apply_filters('spcdm_user_email_to',$to,$post, $uid);
			$subject = apply_filters('spcdm_user_email_subject',$subject,$post, $uid);
			$attachments = apply_filters('spcdm_user_email_attachments',$attachments,$post, $uid);
			#$user_headers = apply_filters('spcdm_user_email_headers',$user_headers,$post, $uid);
			
						 add_filter( 'wp_mail_content_type', 'set_html_content_type' );
                    wp_mail($to,stripslashes($subject),stripslashes( $message),apply_filters('sp_cdm/mail/headers',array(), wp_get_current_user(),$to,$subject,$message), $attachments);
					cdm_event_log($a['pid'],$a['uid'],'folder','<strong>'.sprintf(__('Admin to User Email Notification Sent: %s','sp-cdm'),$to).'</strong>');
					 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
					 do_action('sp_cdm_email_send','sp_cu_admin_user_email',$file_id,$a);
				
                }else{
		
	
        //start user email
           $subject =  sp_cu_process_email($file_id, get_option('sp_cu_user_email_subject'));
            $message = $this->process_email($file_ids, get_option('sp_cu_user_email'));
            $to      = $current_user->user_email;
            if (get_option('sp_cu_additional_user_emails') != "") {
                $cc_user = explode(",", get_option('sp_cu_additional_user_emails'));
                foreach ($cc_user as $key => $user_email) {
                    if ($user_email != "") {
                        $pos = strpos($user_email, '@');
                        if ($pos === false) {
                            $role_user_emails = sp_cdm_find_users_by_role($user_email);
                            foreach ($role_user_emails as $keys => $role_user_email) {
                                $user_headers[] = 'Cc: ' . $role_user_email . '';
                            }
                        } else {
                            $user_headers[] = 'Cc: ' . $user_email . '';
                        }
                    }
                }
            }
			$user_headers = apply_filters('sp_cdm/mail/headers',$user_headers, wp_get_current_user(),$to,$subject,$message);
			print_r($user_headers);
			$message = apply_filters('spcdm_user_email_message',$message,$post, $uid);
			$to = apply_filters('spcdm_user_email_to',$to,$post, $uid);
			$subject = apply_filters('spcdm_user_email_subject',$subject,$post, $uid);
			$attachments = apply_filters('spcdm_user_email_attachments',$attachments,$post, $uid);
			 do_action('spcdm_user_email_headers',$post, $uid,$to, $subject, $message, $user_headers, $attachments);
         
		 
		 $send_email = true;
		 
		 if(get_option('sp_cu_groups_notify_enable') == 1){ 
		#  $send_email = false;
		  
		 }
		     
		if (get_option('sp_cu_user_email') != "" &&  $send_email != false && $a['status'] != 1) { 
			
			
			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			wp_mail($to, stripslashes($subject),stripslashes( $message), apply_filters('sp_cdm/mail/headers',$user_headers, apply_filters('sp_cdm/mail/admin_email',get_option('admin_email')),$to,$subject,$message), $attachments);			
			cdm_event_log($a['pid'],$a['uid'],'folder','<strong>'.sprintf(__('User Email Notification Sent: %s','sp-cdm'),$to).'');
			remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
			do_action('sp_cdm_email_send','sp_cu_user_email',$file_ids,$a);
			 
        }
		//end user email
		
	}
		 do_action('spcdm_after_emails',$post, $uid,$to, $subject, $message,apply_filters('sp_cdm/mail/headers',$user_headers, apply_filters('sp_cdm/mail/admin_email',get_option('admin_email')),$to,$subject,$message), $attachments);
        //email above
    }
    function construct($id = NULL)
    {
	//old function
    }
	
	
	
	 function upload_dialog($id = NULL)
    {
        global $wpdb;
        global $current_user;
		
		if($current_user->ID != ''){
        if ($_GET['id'] != '') {
            $uid =$_GET['id'];
        } else {
            $uid = $current_user->ID;
        }
	
        add_action('sp_cdm_premium_scripts', array(
            $this,
            'scripts'
        ));
        $html .= '
<div class="remodal" data-remodal-id="premium-upload"><a data-remodal-action="close" class="remodal-close"></a>



<form  action="' . $_SERVER['REQUEST_URI'] . '" method="post" enctype="multipart/form-data" id="cdm_upload_premium_form" >
<input type="hidden" name="action" value="cdm_premium_finish_file">

<div class="cdm_debug_upload_form"></div>
<input type="hidden" name="pid" value="0" class="cdm_premium_pid_field">
<input type="hidden" name="uid" value="'.$uid .'">
';

if( @$_GET['page'] == 'sp-client-document-manager-fileview') {
$html .='<input type="hidden" name="admin-uploader" value="1">';	
}
        $html .= '<div>

			<p>' . stripslashes(get_option('sp_cu_form_instructions')) . '</p>';
			
			
		$html = apply_filters('sp_cdm_above_premium_form_fields', $html);	
		
		
		$filename_method = get_option('sp_cu_file_name_behavior');
		if($filename_method == 'hidden'){	
		$hide = 'style="display:none"';
			}
		if($filename_method == 'checked' or $filename_method == 'hidden'){		
		
		
		
		
		$checked = 'checked="checked"';
		$html .='<script type="text/javascript">
		
		jQuery(document).ready(function() {
//  jQuery("#cdm_upload_table tr:first").css("display", "none");
   jQuery("#cdm_og").attr("checked","checked");
   
setInterval(function(){cdmPremiumReValidate();},1000);

});

		</script>';
		}
		$html .='<p '.$hide.'>
	
		<input style="width:90%" placeholder="' . __("Nombre de Archivo", "sp-cdm") . '"  type="text" name="dlg-upload-name" class="required_name" id="dlg-upload-name-premium"> ';
		if($filename_method != 'specify'){
		$html .='<br><span style="font-size:12px"><input type="checkbox" name="og" id="cdm_og" value="1" onclick="cdmPremiumReValidate();" '.$checked .'> ' . __("Marque esta casilla para usar el nombre del archivo original", "sp-cdm") . '</span>';
		}
		$html .='</p> ';
	if(sp_cdm_is_featured_disabled('premium', 'disable_categories') == false){
       $html .= '<div style="padding:10px 0px">';
        $html .= sp_cdm_display_categories();
		$html .='</div>';
	}
        $html .= '<p>
		<div id="cdm_pluploader">
  			  <p>Your browser doesn\'t have Flash, Silverlight or HTML5 support.</p>
		 </div>
		 </p>';
	
	if(get_option('sp_cu_force_zip') == 1){
		
	$html .= '<p><em>Uploading multiple files will be zipped into a single archive</em></p><input type="hidden" value="1" name="zip" id="cdm_zip" >';
	}else{
		
	
	if(sp_cdm_is_featured_disabled('premium', 'upload_zip_file') == false){
	$html .='<p  style="padding:5px">
	<label>' . __("Archivos Zip", "sp-cdm") . '</label>
	<input type="checkbox" value="1" name="zip" id="cdm_zip" > <em style="font-size:12px">' . __("Marque esta casilla para comprimir todos los archivos en un solo archivo.", "sp-cdm") . '.</em>
			</p>';
	}
	}
		
		
		if(get_option('sp_cu_convert_images_to_pdf') == 1 ){
			
			$html .='<p  style="padding:5px">
	<label>' . __("Convert Images to PDF?", "sp-cdm") . '</label>
	<input type="checkbox" value="1" name="convert_to_pdf" id="convert_to_pdf" class="convert_to_pdf" data-convert-pdf="0"> <em style="font-size:12px">' . __("Check this box and all images will be converted to pdf files.", "sp-cdm") . '.</em>
			</p>';
			
		}
		
         if (get_option('sp_cu_user_disable_tags') != 1) {
                $html .= '

 <p  style="padding:5px">

   <textarea placeholder="'.__('Search tags, press enter after each tag','sp-cdm').'" id="tags" name="tags" class="cdm-tags" ></textarea>

  </p>';
         
		 }
       if (get_option('sp_cu_user_disable_notes') != 1) {
            $html .= '<p style="padding:5px">

   
	<textarea placeholder="' . __("Notas", "sp-cdm") . '" style="width:90%;height:50px" name="dlg-upload-notes"></textarea>

  </p>

  ';
  

	   }
		$html .= display_sp_cdm_form();
			$spcdm_form_upload_fields = '';
		$spcdm_form_upload_fields .= apply_filters('spcdm_form_upload_fields',$spcdm_form_upload_fields);
		
		$html .= $spcdm_form_upload_fields;
		if(sp_cdm_is_featured_disabled('premium', 'disable_drafts') == false){
		  $html .='';
                }
        $html .= '

		
			<div style="padding-top:15px">
					<a  class="plupload_button plupload_start btn btn-primary">' . __("Subir Archivos","sp-cdm").'</a>
</div>
			
				

						<div class="sp_change_indicator" ></div>	
			<!--<div class="cdm_debug"></div>-->
						';
        $html .= '

</div>';
        $timestamp = time();
        $html .= '



	</form>

		

	

	';
        $html .= "<script type=\"text/javascript\">

		
// Initialize the widget when the DOM is ready
jQuery(function() {
		
		//alert(".$uid.");
		
		if(jQuery.fn.button.noConflict) {
jQuery.fn.btn = jQuery.fn.button.noConflict();
}
		
		
   var uploader = jQuery('#cdm_pluploader').plupload({
        // General settings
        runtimes : 'html5,silverlight,browserplus,gears,html4',
       multipart_params : {
        'action' : 'cdm_premium_add_file',
		'convert_to_pdf': jQuery('.convert_to_pdf').attr('data-convert-pdf'),
		'form':jQuery('#cdm_upload_premium_form').serialize(),
        'uid' : '".$uid."'
    },
	   
	    url : sp_vars.ajax_url,
 		
		";
		if(get_option('sp_cu_limit_file_types') != ''){
		$html .="filters : [
            {title : '".__("File Types", "sp-cdm")."', extensions : '".get_option('sp_cu_limit_file_types')."'}
        ],";	
		}
		if(get_option('sp_cu_limit_file_size') != '' && is_numeric(get_option('sp_cu_limit_file_size'))){
		$html .=" max_file_size : '".get_option('sp_cu_limit_file_size')."mb',";	
		}
		if(get_option('sp_cu_limit_files') == 1){ 
		$html .= "   multi_selection:false,
					 max_file_count: 1,  ";
		
		}
		
		$html .="

        // Rename files by clicking on their titles
        rename: true,
          chunk_size: '2000kb',
    max_retries: 3,
        // Sort files
        sortable: true,
 
        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,
 
        // Views to activate
        views: {
            list: true,
          
            active: 'list'
        },
 
        // Flash settings
        flash_swf_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.swf')."',
     
        // Silverlight settings
        silverlight_xap_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.xap')."',
		
		// Post init events, bound after the internal events
        init : {
            PostInit: function() {
                // Called after initialization is finished and internal event handlers bound
                console.log('[PostInit]');
               
            },
 
         
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                console.log( file);
            },
 			 FilesAdded: function(up, files) {
  sp_cdm_check_categories();          
cdmPremiumValidate();
             
            },
  
            FilesRemoved: function(up, files) {
            sp_cdm_check_categories();
			 cdmPremiumValidate();
            },
          
 
            UploadComplete: function(up, files) {
               
               		 jQuery.ajax({

						   type: 'POST',   

						

						   url: sp_vars.ajax_url  ,

						   data:  jQuery('#cdm_upload_premium_form').serialize(),

							   success: function(msg){

							   jQuery('.cdm_debug').append(msg);

						";
							 if(@$_GET['page'] == 'sp-client-document-manager-fileview'){
							 
							$html .="
							
							 jQuery.cookie('pid', jQuery('.cdm_premium_pid_field').val(), { expires: 7  , path:'/'});
							 cdmCloseModal('premium-upload');
							//window.location = 'admin.php?page=sp-client-document-manager-fileview&id=".@$_GET['id']."&pid=' + jQuery('.cdm_premium_pid_field').val();
							cdm_ajax_search();";	
							 
							 }else{
							 $html .="
							  jQuery.cookie('pid', jQuery('.cdm_premium_pid_field').val(), { expires: 7 , path:'/' });
							  
				 			 cdmCloseModal('premium-upload');
							cdm_ajax_search();  
						//location.reload();";
						
						
						
						
						
						
							
							 }
					
				$html .= "
							

							   }

						 });

            },
   			
            Destroy: function(up) {
                // Called when uploader is destroyed
                console.log('[Destroy] ');
            },
  
            Error: function(up, args) {
                // Called when error occurs
                if(args.code === plupload.INIT_ERROR)
            {
                alert('Please install or activate flash player');
            }
            
			   
			   
			   console.log(args);
            }
        }
		
		
	
		
    });
		
jQuery(document).on('opened', '.remodal', function () {
  		
		var uploader = jQuery('#cdm_pluploader').plupload('getUploader');
        uploader.splice();
        uploader.refresh();
	
		
});



	setInterval(function(){cdmPremiumValidate()},1000);
	jQuery( '.required_name' ).bind( 'change blur keyup keydown select click', function() {
   cdmPremiumValidate();
		});	
	jQuery( '#cdm_og' ).bind( 'click', function() {
   cdmPremiumValidate();
		});	
		jQuery( '.required' ).bind( 'change blur keyup keydown select click', function() {
   cdmPremiumValidate();
		});	
});
	
		
		function cdmPremiumValidate(){
				
			
				
				var validated = true;
				var msg1 = '';
				var msg2 = '';
			jQuery('#cdm_upload_premium_form .required').each(function() {
				if(jQuery(this).val() == ''){
				validated = false;
				}
			});	
		
			//jQuery('.cdm_debug_upload_form').text(numItems);
				if(validated == false){
					
					msg1 = 'Please fill out all required fields<br>';
				}
			
				if(jQuery('.required_name').val() == ''){
							if (jQuery('#cdm_og').is(':checked')) {
								
							}else{
							validated = true;	
							}
							
				}
				
				if(jQuery('.can-upload-cat').val() == 0){
				validated = false;	
				}
					var queue_count = 	jQuery('#cdm_pluploader').plupload('getUploader').total.queued;
					
					if(queue_count == false){
							validated = false;	
					}
					
				";
				
				 if(get_option('sp_cu_require_category') == 1){
					$html .= "
					
						if(jQuery('.sp-category-select').val() == ''){
						validated = false;	
						jQuery('.sp-category-select-error').html('<span style=margin-left:20px;color:red>".__("Please select a category","sp-cdm")."</span>');
						}else{
							jQuery('.sp-category-select-error').html('');
						}
					"; 
				 }
				$html .="	
						
				//console.log(validated);
				//jQuery('.debug').html(validated  );
				if(validated == false){
					console.log('disabled');
				jQuery( '.plupload_start').button( 'disable' );		
				}else{
					//console.log('enabled');
				jQuery( '.plupload_start').button( 'enable' );			
				}
			
		}
		function cdmPremiumReValidate(){

			

		if (jQuery('#cdm_og').prop('checked')) {

			if (jQuery('#cdm_zip').is(':checked')) {

			alert('".__("Must choose a name if zipping files","sp-cdm")."');

			 jQuery('#dlg-upload-name-premium').attr('disabled', false);

			 jQuery('#dlg-upload-name-premium').val('');

			  jQuery('#cdm_og').attr('checked', false);

			}else{

    jQuery('#dlg-upload-name-premium').attr('disabled', true);

	jQuery('#dlg-upload-name-premium').val('".__("Usando el nombre del archivo","sp-cdm")."');

			}

} else {

    jQuery('#dlg-upload-name-premium').attr('disabled', false);
	if('Using file Name' == jQuery('#dlg-upload-name-premium').val()){
	jQuery('#dlg-upload-name-premium').val('');
	}

} 



	

		}

		
		

	</script></div>";
      echo $html;
		}
    }
	
}
?>