<?php
		add_action('sp_cdm_settings_email', 'sp_cdm_setings_email_revision');
		add_filter('cdm/viewfile/top_navigation', 'sp_cdm_revision_button',10,2);
		
		add_filter('sp_cdm_view_file_content',  'sp_cdm_revision_content',10,2);
	function sp_cdm_setings_email_revision(){
		
		global $wpdb;
		
		echo '  
<h2>Revision Notification</h2>
<em>This email is dispatched when a revision is made on a file</em>
<table class="wp-list-table widefat fixed posts" cellspacing="0">
     

    <tr>

    <td width="300"><strong>Revision Email</strong><br><em>This is the email that is dispatched to user.</em><br><br>Template Tags:<br><br>

	

	[file] = Link to File<br>
	[file_name] = Actual File Name<br>
	
	[file_real_path] = Real Path URL to the file<br>
	[file_in_document_area] = Link to the file in document area<br>
	[notes] = Notes or extra fields<br>
	[group_notes] = Group Notes<br>
	[user] = users name<br>
	
	[uid] = User ID<br>
	[project] = project<br>

	[category] = category (status)<br>

	[user_profile] = Link to user profile<br>

	[client_documents] = Link to the client document manager</td>

    <td>Subject: <input style="width:100%" type="text" name="sp_cu_revision_subject" value="' . get_option('sp_cu_revision_subject') . '"> 
	CC: <input style="width:100%" type="text" name="sp_cu_revision_cc" value="' . stripslashes(get_option('sp_cu_revision_cc')) . '" ><br>Body:<br>
	';
	echo wp_editor(  stripslashes(get_option('sp_cu_revision_email')) , 'sp_cu_revision_email' );
	echo ' </td>

  </tr>';
  
    do_action('sp_cu_email_extra', 'sp_cu_revision_email');
  echo'</table>';
	
		
		
		
		
	}

	function sp_cdm_revision_add($file_id){
			global $wpdb ;
	global $user_ID;
	global $current_user;
		
		$r_data = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id = '".$_POST['parent']."' ", ARRAY_A);
		
	$data = $_POST;
	$post = $_POST;
	$files = $_FILES;
	$uid = $current_user->ID;
	
	#print_r($_POST);print_r($_GET);
	if($_GET['page'] == 'sp-client-document-manager-fileview'){
	$a['uid'] = $file_id;	
	}else{
	$a['uid'] = $current_user->ID;	
	}
	
	
	$a['name'] = $r_data[0]['name'];
	$a['pid'] = $r_data[0]['pid'];
	$a['cid'] = $r_data[0]['cid'];
	$a['notes'] = $data['dlg-upload-notes'];
	$a['parent'] = $_POST['parent'];
	cdm_upate_modified_date($a['parent']);
	$post =array_merge($post,$a);
	$post['file_owner'] = cdm_get_file_owner($_POST['parent']);
	if($files['dlg-upload-file']['name'] != ""){
	
	
	
	$a['file'] = sp_uploadFile($files,1);
    foreach($a as $key=>$value){ if(is_null($value)){ unset($a[$key]); } }
    $wpdb->insert(  "".$wpdb->prefix."sp_cu", $a );
	$file_id = $_POST['parent'];
	cdm_event_log($file_id,$current_user->ID,'file',''.__('Revision Added: '.$a['notes'].'','sp-cdm').'');
	   
	
	$to =  apply_filters('sp_cdm/mail/admin_email',get_option('admin_email'));
	
	$message  = "".__("Client uploaded a new revision","sp-cdm")."<br><br> ".__("Click here view the files:","sp-cdm")." " . get_bloginfo('wpurl') . "/wp-admin/user-edit.php?user_id=".$user_ID."#downloads";
	$subject = ''.__("New file revision upload from client","sp-cdm").'';
	 add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	 wp_mail($to, stripslashes($subject),stripslashes( $message),apply_filters('sp_cdm/mail/headers',$headers,$current_user,$to,$subject,$message), $attachments);
	 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	
	 if (get_option('sp_cu_revision_email') != "") {
            $subject = sp_cu_process_email($file_id,get_option('sp_cu_revision_subject'));
            $message =  sp_cu_process_email($file_id, get_option('sp_cu_revision_email')); 
            $to  = $post['file_owner']->user_email;
            if (get_option('sp_cu_revision_cc') != "") {
                $cc_user = explode(",", get_option('sp_cu_revision_cc'));
                foreach ($cc_user as $key => $user_email) {
                    if ($user_email != "") {
                        $pos = strpos($user_email, '@');
                        if ($pos === false) {
                            $role_user_emails = sp_cdm_find_users_by_role($user_email);
                            foreach ($role_user_emails as $keys => $role_user_email) {
                                $user_headers[] = 'Cc: ' . $role_user_email . '';
                            }
                        } else {
                            $user_headers[] = 'Cc: ' . $user_email . '';
                        }
                    }
                }
            }
		
			$message = apply_filters('spcdm_user_email_message',$message,$post, $uid);
				
			$to = apply_filters('spcdm_user_email_to',$to,$post, $uid);
			$subject = apply_filters('spcdm_user_email_subject',$subject,$post, $uid);
			$attachments = apply_filters('spcdm_user_email_attachments',$attachments,$post, $uid);
			do_action('spcdm_user_email_headers',$post, $uid,$to, $subject, $message, $user_headers, $attachments);
           
		   add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		    wp_mail($to, stripslashes($subject),stripslashes( $message), apply_filters('sp_cdm/mail/headers',$user_headers,$current_user,$to,$subject,$message), $attachments);
			cdm_event_log($file_id,$current_user->ID,'file',''.__('Revision Email sent to: '.$to.'.','sp-cdm').'');
			
			 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		  do_action('sp_cdm_email_send','sp_cu_revision_email',$file_id,$post, $uid,$to, $subject, $message, $headers, $attachments);
		 
        }

		$post['pid'] = $r_data[0]['pid'];
		print_r($post);
	
		 do_action('spcdm_after_emails',$post, $uid,$to, $subject, $message, $user_headers, $attachments);
		echo  '<p style="color:red">'.__("File uploaded!!","sp-cdm").'</p>';
		cdm_event_log($file_id,$current_user->ID,'file',''.__('Revision added.','sp-cdm').'');
}else{
	
	echo  '<p style="color:red">'.__("Please upload a file!","sp-cdm").'</p>';
	
}
	}
//functions

function sp_cdm_revision_content($html,$r){
	
			$show = false;
			
			if( get_option('sp_cu_user_disable_revisions') != 1){
			$show = true;	
			}
			if(current_user_can('manage_options')){
			$show = true;		
			}
			if(current_user_can('sp_cdm_enable_revisions')){
			$show = true;		
			}	
			if( $show == true){
			if($r[0]['form_id'] == '' or $r[0]['form_id'] == 0){
			 $html .= '<div id="cdm-file-revisions"><div id="cdm_comments"><h4>' . __("Revision History", "sp-cdm") . '</h4>

' . sp_cdm_file_history($r[0]['id']) . '</div></div>';
			}
		}
	
	return $html;
}
function sp_cdm_revision_button($html,$r){
	$show = false;
			
			if( get_option('sp_cu_user_disable_revisions') != 1){
			$show = true;	
			}
			if(current_user_can('manage_options')){
			$show = true;		
			}
			if(current_user_can('sp_cdm_enable_revisions')){
			$show = true;		
			}	
			if( $show == true){

	$disable_features = get_option('sp_cdm_disable_features');
	
	
	if($r[0]['form_id'] == '' or $r[0]['form_id'] == 0){
	
			global $wpdb,$user_ID , $current_user;
	
	$file = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d order by date desc", $r[0]['id']));
	$file_id =  $file[0]->id;	
if($_POST['add-revision'] != ""){
	
echo sp_cdm_revision_add($file_id );
	
}		
		
	

		$html .= '	<a href="javascript:sp_cu_dialog(\'.revision_'.$file_id.'\',550,300)"  title="Add Revision" ><span class="dashicons 
dashicons-plus cdm-dashicons"></span> '.__("Add Revision","sp-cdm").'</a>
		
		<div style="display:none">
	
		<script type="text/javascript">
	
		 function showRequest(){
			jQuery("#add-revision").attr("disabled",true);
		 }
		 function showResponse(){
		//	jQuery("#output1").html();
		cdmRefreshFile('.$file_id.')
			
			jQuery("#revision_'.$file_id.'").dialog("close");
			jQuery(".output1_'.$file_id.'").html(\'<p style="color:red">Revision Added</p>\');
			
			
			 jQuery.cookie("pid", jQuery(".cdm_premium_pid_field").val(), { expires: 7, path:"/" });
			// location.reload();
		 }
		  var options = { 
        target:        \'#view_file_refresh\',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
        
    }; 
 
    jQuery("#history_form_'.$file_id.'").ajaxForm(options); 
		
		</script>
		
		
		<div class="revision_'.$file_id.'">
		<div class="output1_'.$file_id.'">		</div>
		<form id="history_form_'.$file_id.'" action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
	<input type="hidden" name="parent" value="'.$file_id.'">		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="150"><strong>'.__("File","sp-cdm").'</strong></td>
    <td><input type="file" name="dlg-upload-file[]"></td>
  </tr>
  <tr>
    <td><strong>'.__("Notes:","sp-cdm").'</strong></td>
    <td><textarea name="dlg-upload-notes" style:width:300px;height:50px"></textarea></td>
  </tr>';
  

  $spcdm_form_upload_fields .= apply_filters('spcdm_form_upload_fields',$spcdm_form_upload_fields);
  $html .= $spcdm_form_upload_fields;
  
     $html .='<tr>
    <td></td>
    <td><span class="revision-uploading"><input type="submit" name="add-revision" id="add-revision" value="'.__("Add Revision","sp-cdm").'"></span></td>
  </tr>
</table>
</form>
		</div>
		</div>
		';	
		
	}
	
	}
		return $html;
	
}

function sp_cdm_file_history_exists($id){
	global $wpdb,$user_ID;	
	
	$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where parent = '".$id."'  order by date asc", ARRAY_A);
	$r_cur = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id = '".$id."'  ", ARRAY_A);
	
	if(count( $r ) > 0){
		return true;
	}else{
			return false;
	}
	
}

function sp_cd_file_user_by_id($id){
	
$user_info = get_userdata($id);
    return $user_info->user_login;
}
function sp_cdm_file_history($id){
global $wpdb,$user_ID;	
	
		$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where parent = '".$id."'  order by date asc", ARRAY_A);
	$r_cur = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id = '".$id."'  ", ARRAY_A);
	
		if(count( $r ) > 0){

		
		$content .='<div class="sp_su_project">
			<a '. cdm_revision_file_link(base64_encode($r_cur[0]['id'].'|'.$r_cur[0]['date'].'|'.$r_cur[0]['file']),get_option('sp_cu_js_redirect'),NULL,NULL,true).'><strong>'.stripslashes($r_cur[0]['file']).'</strong></a>  <em>(Original added on '.date('F jS Y h:i A', strtotime($r_cur[0]['date'])).') by '.sp_cd_file_user_by_id($r_cur[0]['uid']).' #'.$r_cur[0]['id'].'</em></div>';
		$t= 2;
		for($i=0; $i<count( $r ); $i++){
			$t++;
			
			$rev = $i + 1;
			$content .='<div class="sp_su_project">
			<a '. cdm_revision_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'><strong>'.stripslashes($r[$i]['file']).'</strong></a> - '.$r[$i]['notes'].' <em>(rev:'. $rev.' on '.date('F jS Y h:i A', strtotime($r[$i]['date'])).') by '.sp_cd_file_user_by_id($r[$i]['uid']).'</em></div>';
			
		}

		}else{
		$content = ''.__("No other files in history","sp-cdm").'';	
		}
		return $content;
}


?>