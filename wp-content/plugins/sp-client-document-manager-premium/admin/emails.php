<?php
	
	
	$sp_cdm_premium_emails = new sp_cdm_premium_emails;
	
	
	add_action('sp_cdm_settings_email', array($sp_cdm_premium_emails,'add_zip_email'));
	

	
	class sp_cdm_premium_emails{
		
		
	function __construct(){
		
		
	}
	
	function add_zip_email(){
		
		
			global $wpdb;
		
		echo '  
<h2>Add Files To Zip</h2>
<em>Add a file to a zip archive notification email</em>
<table class="wp-list-table widefat fixed posts" cellspacing="0">
     

    <tr>

    <td width="300"><strong>Add Files</strong><br><br><br>Template Tags:<br><br>

	
	[file_modifications] = Files Added<br>
	[files_modification_notes] = Upload Notes<br>
	[file] = Link to File<br>
	[file_name] = Actual File Name<br>
	
	[file_real_path] = Real Path URL to the file<br>
	[file_in_document_area] = Link to the file in document area<br>
	[notes] = Notes or extra fields<br>
	[group_notes] = Group Notes<br>
	[user] = users name<br>
	
	[uid] = User ID<br>
	[project] = project<br>

	[category] = category (status)<br>

	[user_profile] = Link to user profile<br>
	
	[client_documents] = Link to the client document manager</td>

    <td>Subject: <input style="width:100%" type="text" name="sp_cu_add_zip_subject" value="' . get_option('sp_cu_add_zip_subject') . '"> 
	CC: <input style="width:100%" type="text" name="sp_cu_add_zip_cc" value="' . stripslashes(get_option('sp_cu_add_zip_cc')) . '" ><br>Body:<br>
	';
	echo wp_editor(  stripslashes(get_option('sp_cu_add_zip_email')) , 'sp_cu_add_zip_email' );
	echo ' </td>

  </tr>';
  
    do_action('sp_cu_email_extra', 'sp_cu_add_zip_email');
  echo'</table>';
	
		
		
	}
		
	}