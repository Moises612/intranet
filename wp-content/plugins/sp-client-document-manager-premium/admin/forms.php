<?php

class cdmForms {
	
	function title(){
		
		return '<h2>'.__("Form Manager","sp-cdm").'</h2>'.sp_client_upload_nav_menu().'
		
		 <div style="margin:10px">
									 <a href="admin.php?page=sp-client-document-manager-forms&function=add" class="button">'.__("Add Form Field","sp-cdm").'</a>
		 </div>
		';
		
		
		
		
	}
	
function add(){
	global $wpdb;
	
	echo  '<form action="admin.php?page=sp-client-document-manager-forms" method="post"> ';
	if($_GET['id'] != ""){
	$r = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_forms where id = ".$_GET['id']."", ARRAY_A);	
	echo '<input type="hidden" name="id" value="'.$r[0]['id'].'">';
	$type = '<option value="'.$r[0]['type'].'" selected>'.$r[0]['type'].'</option>';
	$required = '<option value="'.$r[0]['required'].'" selected>'.$r[0]['required'].'</option>';
	}
	
	echo  '	
	<table class="wp-list-table widefat fixed posts" cellspacing="0">
  <tr>
    <td style="width:100px">'.__("Name","sp-cdm").':</td>
    <td><input type="text" name="form-name" value="'.stripslashes($r[0]['name']).'"></td>
  </tr>

    <tr>
    <td style="width:100px">'.__("Required","sp-cdm").'?</td>
    <td><select name="required"> '.$required.'<option value="Yes">Yes</option><option value="No">No</option></select></td>
  </tr>
    <tr>
    <td style="width:100px">'.__("Type","sp-cdm").':</td>
    <td><select name="type">'.	$type .'
	<option value="textbox">Text box</option>
	    <option value="select">Select Box</option>
		 <option value="textarea">Text Area</option>
		 </select>
	</td>
  </tr>
  <tr>
    <td>'.__("Defaults:","sp-cdm").'<br><em>Comma seperated for multiple options when using selectboxes</em></td>
    <td><textarea name="defaults" style="width:100%;height:150px">'.stripslashes($r[0]['defaults']).'</textarea>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="save-form" value="'.__("Save","sp-cdm").'"></td>
  </tr>
  
</table>
</form>
';
	
}
	
	
	function view(){
		
		global $wpdb;
		
		
		
		echo $this->title();
		
		if($_POST['save-form'] != ""){
			
			$insert['defaults'] = $_POST['defaults'];
			$insert['required'] = $_POST['required'];
			
			$insert['type'] = $_POST['type'];
			$insert['name'] = $_POST['form-name'];
			if($_POST['id'] == ""){
			 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
			$wpdb->insert("".$wpdb->prefix."sp_cu_forms",$insert);
				
			}else{
			$where['id'] = $_POST['id'];	
			$wpdb->update("".$wpdb->prefix."sp_cu_forms",$insert,$where);	
			}
			
		}
		
		if($_GET['function'] == "delete"){
			$wpdb->query("DELETE FROM ".$wpdb->prefix."sp_cu_forms WHERE id = ".$_GET['id']."	");			
		}
		
		if($_POST['save-sort'] != ""){
		
		
		foreach( $_POST['sorts'] as $key => $value){
		
		    $insert['sort'] = $value;
		    $where['id'] = $key;	
			$wpdb->update("".$wpdb->prefix."sp_cu_forms",$insert,$where);	
		
		}
		
		
		}
		if(($_GET['function'] == 'add' or $_GET['id'] != "") && $_GET['function'] != 'delete'){
			
		$this->add();
			
		}else{
			
			$r = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_forms order by sort", ARRAY_A);	
			
			 echo '
								
									 
						<form action="admin.php?page=sp-client-document-manager-forms" method="post">			 
									
									 <table class="wp-list-table widefat fixed posts" cellspacing="0">
	<thead>
	<tr>
	<th>'.__("ID","sp-cdm").'</th>
<th style="width:120px"><input type="submit" name="save-sort"  value="'.__("Save Order","sp-cdm").'"></th>
<th>'.__("Name","sp-cdm").'</th>
<th>'.__("Type","sp-cdm").'</th>
<th>'.__("Email Shortcode","sp-cdm").'</th>
<th>'.__("Action","sp-cdm").'</th>
</tr>
	</thead>';
				for($i=0; $i<count(	$r); $i++){
					
					
					echo '<tr>
					<td>'.stripslashes($r[$i]['id']).'</td>
						<td ><input type="text" name="sorts['.$r[$i]['id'].']" value="'.$r[$i]['sort'].'" style="width:40px"></td>
						<td>'.stripslashes($r[$i]['name']).'</td>
						<td>'.stripslashes($r[$i]['type']).'</td>
						<td>[custom_form_field_'.$r[$i]['id'].']</td>				
<td>';
echo '<a href="admin.php?page=sp-client-document-manager-forms&function=edit&id='.$r[$i]['id'].'" style="margin-right:10px" >'.__("Modify","sp-cdm").'</a>';

	
 echo '
<a href="admin.php?page=sp-client-document-manager-forms&function=delete&id='.$r[$i]['id'].'" style="margin-right:15px" >'.__("Delete","sp-cdm").'</a> 
';

echo '</td>
</tr>';
					
				}
				
				echo '</table>';
		
		}
	}
}

?>