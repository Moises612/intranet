<?php

		$sp_cdm_premium_projects = new sp_cdm_premium_projects;
   
	
	
	add_action('sp_cu_admin_menu', array($sp_cdm_premium_projects, 'menu'));
	
	add_action( 'wp_ajax_cdm_admin_change_project_order',  array($sp_cdm_premium_projects, 'change_order'));
	add_action( 'wp_ajax_cdm_admin_change_project_order_load',  array($sp_cdm_premium_projects, 'sortable'));
	
	add_action( 'sp_cdm/admin/projects/list/header',  array($sp_cdm_premium_projects, 'project_header'));
	add_action( 'sp_cdm/admin/projects/list/loop',  array($sp_cdm_premium_projects, 'project_loop'));
	add_action('sp_cdm/admin/projects/list/buttons',  array($sp_cdm_premium_projects, 'project_hide_button'));
	add_action('admin_init',  array($sp_cdm_premium_projects, 'project_button_save')); 
	add_action('init',  array($sp_cdm_premium_projects, 'upgrade')); 
	add_action('sp_cdm/admin/projects/list/query/search',  array($sp_cdm_premium_projects, 'project_query'));
	class sp_cdm_premium_projects{
		
			function upgrade(){
				global $wpdb;
				
				 if ($wpdb->query("SHOW COLUMNS FROM `" . $wpdb->prefix . "sp_cu_project` LIKE 'hide'") != 1){
				 $wpdb->query("ALTER TABLE `".$wpdb->prefix ."sp_cu_project` ADD `hide` INT( 1 ) NOT NULL DEFAULT '0'");				 
				 }
				
				
			}
			function get_hidden_status($project_id){
				global $wpdb;
				$r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project where id = %d", $project_id), ARRAY_A);	
					
				return $r[0]['hide'];				
			}
			function project_query($query){
				global $current_user;
				if(get_user_meta($current_user->ID, '_show_hidden_folders', true) == 1){
			$query .= ' AND hide = 0 ';
				}else{
			$query .= ' AND hide = 1 ';		
				}
				
			return $query;	
			}
			function project_button_save(){
					global $current_user,$wpdb;
					
					
					
				if($_GET['hide_folders'] == 1){
					update_user_meta($current_user->ID, '_show_hidden_folders',0);
					wp_redirect('admin.php?page=sp-client-document-manager-projects');
				}
				if($_GET['show_folders'] == 1){
					update_user_meta($current_user->ID, '_show_hidden_folders',1);
					wp_redirect('admin.php?page=sp-client-document-manager-projects');
				}
				if($_GET['hidden_projects'] != ''){
					
				$where['id'] =$_GET['hide_project'];
				if($this->get_hidden_status($_GET['hide_project']) == 0){
				$update['hide'] = 1;
				}else{
				$update['hide'] = 0;	
				}
				$wpdb->update($wpdb->prefix . "sp_cu_project", $update,$where);
					
					wp_redirect('admin.php?page=sp-client-document-manager-projects');
				}
				
			}
			function project_hide_button(){
					global $wpdb,$current_user;
					
					
					
					if(get_user_meta($current_user->ID, '_show_hidden_folders', true) == 1){
					echo ' <a href="?hide_folders=1" class="button">Show Hidden Folders</a> ';	
					}else{
					echo ' <a href="?show_folders=1" class="button">Show Active Folders</a> ';	
					}
				
				
			}
			function project_loop($r){
			
				if($this->get_hidden_status($r['projectID']) == 0){			
				echo '<td style="font-weight:bold;background-color:#EFEFEF"><a href="?hide_project='.$r['projectID'].'&hidden_projects=1">Hide</a></td>';	
				}else{
				echo '<td style="font-weight:bold;background-color:#EFEFEF"><a href="?hide_project='.$r['projectID'].'&hidden_projects=1">Unhide</a></td>';		
				}
			}
			function project_header(){
				
			echo '<th>Hide</th>';	
			}
			
			
			
			function menu(){
					add_submenu_page('sp-client-document-manager',__(sprintf('Sort %s', sp_cdm_folder_name(1)),'sp-cdm'),__(sprintf('Sort %s', sp_cdm_folder_name(1)),'sp-cdm'), 'sp_cdm_projects', 'sp-client-document-manager-projects-sort', array(
				   	$this,
					'view_sortable'
				));
				
				
			}
			
			function change_order(){
				
				global $wpdb;
			if(current_user_can('manage_options') or current_user_can('sp_cdm')){
		
				
				
				$id = $_POST['order_id']; 	
				$direction = $_POST['direction']; 
				$order_current = $_POST['order_current']; 
				$r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project where id = %d", $id), ARRAY_A);
				$r_project  = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project where parent = %d",  $r[0]['parent']), ARRAY_A);
			
				if($direction == 'up'){
						
						if($order_current >= 0){
							
							$new_order = $order_current - 1;
							
						if($new_order < 0){
				$new_order = 0;	
				}
				$update['default_order'] = $new_order;
				$where['id'] = $id;
				$wpdb->update($wpdb->prefix . "sp_cu_project", $update,$where);
								
						}
					
				}else{
					
				
					
							$new_order = $order_current + 1;
							
						
				if($new_order < 0){
				$new_order = 0;	
				}
				$update['default_order'] = $new_order;
				$where['id'] = $id;
				$wpdb->update($wpdb->prefix . "sp_cu_project", $update,$where);
					
					
				}
				
				
				
			}			
				
			die();	
			}
				function sort_children($id){
			global $wpdb;
			  $r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project where parent = %d  order by default_order", $id), ARRAY_A);
			  if($r != false){
				  
				  echo '<ul>';
				  for ($i = 0; $i < count($r); $i++) {
				   echo '<li><a href="#" data-id="'.$r[$i]['id'].'" data-order="'.$r[$i]['default_order'].'" data-direction="up" class="cdm-project-order"><span class="dashicons dashicons-arrow-up-alt"></span></a> 
				 <a href="#" data-id="'.$r[$i]['id'].'" data-order="'.$r[$i]['default_order'].'" data-direction="down" class="cdm-project-order"><span class="dashicons dashicons-arrow-down-alt"></span></a>  <strong>'.$r[$i]['name'].' </strong><span>Order:'.$r[$i]['default_order'].'</span>';
				   echo $this->sort_children($r[$i]['id']);
				   echo '</li>';  
				  }
				   echo '</ul>';
				  
			  }
			
		}
		function view_sortable(){
			 global $wpdb;
			 echo '<h2>' . sp_cdm_folder_name(1) . '</h2>' . sp_client_upload_nav_menu() . '';
			 
			 echo '<div class="sortable-wrapper">';
			 $this-> sortable();
			 echo '</div>';
			
		}
		function sortable(){
			
			      global $wpdb;
			  
			
			  $r = $wpdb->get_results("SELECT " . $wpdb->prefix . "sp_cu_project.name as projectName,

									" . $wpdb->prefix . "sp_cu_project.uid,
									" . $wpdb->prefix . "sp_cu_project.default_order,
									" . $wpdb->prefix . "sp_cu_project.parent,
									" . $wpdb->prefix . "sp_cu_project.id AS projectID,
									" . $wpdb->base_prefix . "users.ID,
									" . $wpdb->base_prefix . "users.user_nicename								
									
									FROM " . $wpdb->prefix . "sp_cu_project
									LEFT JOIN " . $wpdb->base_prefix . "users ON " . $wpdb->prefix . "sp_cu_project.uid = " . $wpdb->base_prefix . "users.ID
										
									 WHERE " . $wpdb->prefix . "sp_cu_project.parent = 0 
									 	
									 order by default_order", ARRAY_A);
              
			  
			  echo '<div class="sortable_view"><ul >';
			   for ($i = 0; $i < count($r); $i++) {
				   
				   
				 echo '<li><a href="#" data-id="'.$r[$i]['projectID'].'" data-order="'.$r[$i]['default_order'].'" data-direction="up" class="cdm-project-order"><span class="dashicons dashicons-arrow-up-alt"></span></a> 
				 <a href="#" data-id="'.$r[$i]['projectID'].'" data-order="'.$r[$i]['default_order'].'" data-direction="down" class="cdm-project-order"><span class="dashicons dashicons-arrow-down-alt"></span></a> <strong>'.$r[$i]['projectName'].' </strong> <span>Order:'.$r[$i]['default_order'].'</span>';
				$this->sort_children($r[$i]['projectID']);
				 echo '</li>';  
			   }
			   
			 echo '</ul></div>';
			  
	
		if ( is_admin() && defined( 'DOING_AJAX' ) && DOING_AJAX ){
				 die(); 
				}
	
		}
		
	
		
	}



function sp_client_upload_projects_add(){
	
global $wpdb;
	

	
	

echo '
<form action="admin.php?page=sp-client-document-manager-projects" method="post">';

if($_GET['id'] != ""){
$r = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_project where id = '".$_GET['id']."'  ", ARRAY_A);	

echo '<input type="hidden" name="id" value="'.$r[0]['id'].'">';
}


$users = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users order by display_name  ", ARRAY_A);	

$userselect .='<select name="uid">';
if($_GET['id'] != ""){
	$user = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."users  where id = ".$r[0]['uid']."  ", ARRAY_A);	

$userselect .= '<option selected value="'.$r[0]['uid'].'">'.stripslashes($user[0]['display_name']).'</option>';	
}
for($i=0; $i<count($users ); $i++){
	$userselect .= '<option value="'.$users[$i]['ID'].'">'.$users[$i]['display_name'].'</option>';
}
$userselect .= '</select>';




echo '<h2>'.sp_cdm_folder_name(1) .'</h2>'.sp_client_upload_nav_menu().'';
echo '
	 <table class="wp-list-table widefat fixed posts" cellspacing="0">
  <tr>
    <td>'.__("Name:","sp-cdm").'</td>
    <td><input type="text" name="project-name" value="'.stripslashes($r[0]['name']).'"></td>
  </tr>
  <tr>
    <td>'.__("User:","sp-cdm").'</td>
    <td>'.$userselect.'</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="save-project" value="'.__("Save","sp-cdm").'"></td>
  </tr>
</table>
</form>


';	
	
}


function sp_client_upload_projects(){
	
	
global $wpdb;


	
if($_POST['save-project'] != ""){
	
	
			$insert['name'] = $_POST['project-name'];
			$insert['uid'] = $_POST['uid'];

		
		if($_POST['id'] != ""){
		$where['id'] =$_POST['id'] ;
		
	    $wpdb->update(  "".$wpdb->prefix . "sp_cu_project", $insert , $where );	
		}else{
			
			
 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
		$wpdb->insert( "".$wpdb->prefix . "sp_cu_project",$insert );
		}
	
	
}
	



if($_GET['function'] == 'add' or $_GET['function'] == 'edit'){
	
	sp_client_upload_projects_add();
	
}elseif($_GET['function'] == 'delete'){
	
	$wpdb->query("DELETE FROM ".$wpdb->prefix ."sp_cu_project WHERE id = ".$_GET['id']."	");		
echo '<script type="text/javascript">
<!--
window.location = "admin.php?page=sp-client-document-manager-projects"
//-->
</script>';

	
}else{
	
	

	$r = $wpdb->get_results("SELECT ".$wpdb->prefix."sp_cu_project.name as projectName,
									".$wpdb->prefix."sp_cu_project.uid,
									".$wpdb->prefix."sp_cu_project.id AS projectID,
									".$wpdb->prefix."users.ID,
									".$wpdb->prefix."users.user_nicename
										
	
									 FROM ".$wpdb->prefix."sp_cu_project
									 LEFT JOIN ".$wpdb->prefix."users ON ".$wpdb->prefix."sp_cu_project.uid = ".$wpdb->prefix."users.ID
									 order by ".$wpdb->prefix."sp_cu_project.name", ARRAY_A);	
								



echo '<h2>'.sp_cdm_folder_name(1) .'</h2>'.sp_client_upload_nav_menu().'';	 
									 
									 echo '
								
									 
									 
									 <div style="margin:10px">
									 <a href="admin.php?page=sp-client-document-manager-projects&function=add" class="button">'.__("Add","sp-cdm").' '.sp_cdm_folder_name() .'</a>
									 </div>
									 <table class="wp-list-table widefat fixed posts" cellspacing="0">
	<thead>
	<tr>
<th>'.__("ID","sp-cdm").'</th>
<th>'.__("Name","sp-cdm").'</th>
<th>'.__("User","sp-cdm").'</th>
<th>'.__("Action","sp-cdm").'</th>
</tr>
	</thead>';
				for($i=0; $i<count(	$r); $i++){
				
				$vendor_info[$i] = unserialize($vendors[$i]['option_value']);	
					
				echo '	<tr>
<td>'.$r[$i]['projectID'].'</td>				
<td>'.stripslashes($r[$i]['projectName']).'</td>
<td>'.$r[$i]['user_nicename'].'</td>
<td>
<a href="javascript:cdm_download_project('.$r[$i]['id'] .',\''.wp_create_nonce( 'my-action_'.$r[$i]['id'] ).'\')" >'.__("Download Archive","sp-cdm").'</a>  

 <a href="admin.php?page=sp-client-document-manager-projects&function=delete&id='.$r[$i]['projectID'].'" style="margin-right:15px" >'.__("Delete","sp-cdm").'</a> 
<a href="admin.php?page=sp-client-document-manager-projects&function=edit&id='.$r[$i]['projectID'].'" >'.__("Modify","sp-cdm").'</a></td>
</tr>';	
					
				}
				echo '</table>';
}
}



?>