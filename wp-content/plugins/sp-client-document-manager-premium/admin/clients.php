<?php
$sp_cdm_clients_admin = new sp_cdm_clients_admin;

add_action('sp_cu_admin_menu', array(
    $sp_cdm_clients_admin,
    'menu'
));
add_action('admin_init', array(
    $sp_cdm_clients_admin,
    'permissions'
));
add_action('admin_init', array(
    $sp_cdm_clients_admin,
    'install'
));
add_filter('sp_client_upload_top_menu', array(
    $sp_cdm_clients_admin,
    'topmenu'
));


add_action('wp_ajax_cdm_ajax_add_client', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_add_client'
));
add_action('wp_ajax_nopriv_cdm_ajax_add_client', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_add_client'
));

add_action('wp_ajax_cdm_ajax_delete_client', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_delete_client'
));
add_action('wp_ajax_nopriv_cdm_ajax_delete_client', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_delete_client'
));




add_action('wp_ajax_cdm_ajax_add_client_user', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_add_client_user'
));
add_action('wp_ajax_nopriv_cdm_ajax_add_client_user', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_add_client_user'
));

add_action('wp_ajax_cdm_ajax_delete_client_user', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_delete_client_user'
));
add_action('wp_ajax_nopriv_cdm_ajax_delete_client_user', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_delete_client_user'
));

add_action('wp_ajax_cdm_ajax_load_clients', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_load_clients'
));
add_action('wp_ajax_nopriv_cdm_ajax_load_clients', array(
    $sp_cdm_clients_admin,
    'cdm_ajax_load_clients'
));


class sp_cdm_clients_admin
  {
    
    
    function cdm_ajax_delete_client()
      {
        
        global $wpdb;
        $message['error'] = 'Something Went Wrong';
        if ($_POST['action'] == "cdm_ajax_delete_client")
          {
            
            $wpdb->query($wpdb->prepare("DELETE FROM " . $wpdb->prefix . "sp_cu_clients WHERE id = %d", $_POST['id']));
            $wpdb->query($wpdb->prepare("DELETE FROM " . $wpdb->prefix . "sp_cu_clients_assign WHERE gid = %d", $_POST['id']));
            $message['id']    = $_POST['id'];
            $message['error'] = '';
            echo json_encode($message);
            
          }
        
        die(0);
        
      }
    function cdm_ajax_delete_client_user()
      {
        
        global $wpdb;
        $message['error'] = 'Something Went Wrong';
        if ($_POST['action'] == "cdm_ajax_delete_client_user")
          {
            $r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM " . $wpdb->prefix . "sp_cu_clients_assign where id = %d  ", $_POST['id']), ARRAY_A);
            $wpdb->query($wpdb->prepare("DELETE FROM " . $wpdb->prefix . "sp_cu_clients_assign WHERE id = %d", $_POST['id']));
            $message['id']    = $r[0]['gid'];
            $message['error'] = '';
            echo json_encode($message);
            
          }
        
        
        die(0);
      }
    
    function cdm_ajax_add_client_user()
      {
        
        global $wpdb;
        
        $message['error'] = 'Something went wrong';
        
        
        if ($_POST['action'] == "cdm_ajax_add_client_user")
          {
            
            $r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM " . $wpdb->prefix . "sp_cu_clients_assign where gid = %d AND uid = %d ", $_POST['gid'], $_POST['author']), ARRAY_A);
            
            
            if ($r != false)
              {
                
                $message['error'] = 'User already added to client';
                
              }
            else
              {
                $insert['uid'] = $_POST['author'];
                $insert['gid'] = $_POST['gid'];
                
                foreach ($insert as $key => $value)
                  {
                    if (is_null($value))
                      {
                        unset($insert[$key]);
                      }
                  }
                $wpdb->insert("" . $wpdb->prefix . "sp_cu_clients_assign", $insert);
                
                $user             = get_userdata($insert['uid']);
                $message['error'] = '';
                $message['gid']   = $insert['gid'];
                $message['uid']   = $insert['uid'];
              }
            echo json_encode($message);
          }
        
        die(0);
      }
    
    function cdm_ajax_add_client()
      {
        
        global $wpdb;
        
        
        $message['error'] = 'Something went wrong';
        
        
        if ($_POST['action'] == "cdm_ajax_add_client")
          {
            
            
            $insert['name']  = $_POST['clients-name'];
            $insert['email'] = $_POST['clients-email'];
            $insert['phone'] = $_POST['clients-phone'];
            $insert['notes'] = $_POST['clients-notes'];
            
            
            if ($_POST['id'] != "")
              {
                $where['id'] = $_POST['id'];
                
                $wpdb->update("" . $wpdb->prefix . "sp_cu_clients", $insert, $where);
                $message['message'] = 'Updated Client';
                $message['id']      = $where['id'];
                $message['error']   = '';
              }
            else
              {
                
                
                foreach ($insert as $key => $value)
                  {
                    if (is_null($value))
                      {
                        unset($insert[$key]);
                      }
                  }
                $wpdb->insert("" . $wpdb->prefix . "sp_cu_clients", $insert);
                $message['message'] = 'Added Client';
                $message['id']      = $wpdb->insert_id;
                $message['error']   = '';
              }
            
            
            echo json_encode($message);
          }
        
        die(0);
      }
    
    function install()
      {
        global $wpdb;
        
        if (get_option('' . $wpdb->prefix . 'sp_cu_clients') == '' or $_GET['force_upgrade'] == 1)
          {
            
            
            $sql['' . $wpdb->prefix . 'sp_cu_clients'] = 'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'sp_cu_clients` (
			   `id` int(11) NOT NULL AUTO_INCREMENT,
			   `name` varchar(255) NOT NULL,
			   `phone` varchar(255) NOT NULL,
			   `email` varchar(255) NOT NULL,
			   `notes` text NOT NULL,
			   PRIMARY KEY (`id`)
			) ;';
            
            
            
            $sql['' . $wpdb->prefix . 'sp_cu_clients_assign'] = 'CREATE TABLE IF NOT EXISTS `' . $wpdb->prefix . 'sp_cu_clients_assign` (
			    `id` int(11) NOT NULL AUTO_INCREMENT,
				  `gid` int(11) NOT NULL,
				  `uid` int(11) NOT NULL,				 
				  PRIMARY KEY (`id`)
			) ;';
            
            
            
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            foreach ($sql as $table => $install)
              {
                dbDelta($install);
                
                
                update_option('' . $wpdb->prefix . 'sp_cu_clients', 1);
              }
            
          }
        
      }
    function permissions()
      {
        global $current_user;
        if ($current_user != '')
          {
            
            
            if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_clients'))
              {
                @require_once(ABSPATH . 'wp-includes/pluggable.php');
                $role = get_role('administrator');
                $role->add_cap('sp_cdm_clients');
                
                
              }
            
          }
      }
    function menu()
      {
        
        add_submenu_page('sp-client-document-manager', 'Clients', 'Clients', 'sp_cdm_clients', 'sp-client-document-manager-clients', array(
            $this,
            'view'
        ));
        
      }
    
    function topmenu($extra_top_menus)
      {
        $extra_top_menus .= '<li><a href="admin.php?page=sp-client-document-manager-clients" >' . __("Clients", "sp-cdm") . '</a></li>	';
        
        
        return $extra_top_menus;
        
      }
    
    
    
    function add()
      {
        
        global $wpdb;
        
        
        
        
        
        echo '
<div class="clients-left-column">
<form action="admin.php?page=sp-client-document-manager-clients" method="post" class="sp_cu_client_form">
<input type="hidden" name="action" value="cdm_ajax_add_client">	
';
        
        if ($_GET['id'] != "")
          {
            $r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM " . $wpdb->prefix . "sp_cu_clients where id = %d ", $_GET['id']), ARRAY_A);
            
            echo '<input type="hidden" name="id" value="' . $r[0]['id'] . '">';
            echo '<h3>Editing: ' . $r[0]['name'] . '</h3>';
          }
        else
          {
            echo '<h3>Add New Client</h3>';
          }
        
        
        $users = $wpdb->get_results("SELECT * FROM " . $wpdb->base_prefix . "users order by display_name  ", ARRAY_A);
        
        $userselect .= '<select name="uid" style="width:320px">';
        if ($_GET['id'] != "")
          {
            $user = $wpdb->get_results("SELECT * FROM " . $wpdb->base_prefix . "users  where id = " . $r[0]['uid'] . "  ", ARRAY_A);
            
            $userselect .= '<option selected value="' . $r[0]['uid'] . '">' . stripslashes($user[0]['display_name']) . '</option>';
          }
        for ($i = 0; $i < count($users); $i++)
          {
            $userselect .= '<option value="' . $users[$i]['ID'] . '">' . $users[$i]['display_name'] . '</option>';
          }
        $userselect .= '</select>  ';
        
        
        if ($r[0]['locked'] == 1)
          {
            $checked = ' checked="checked"';
            
          }
        echo '
	 <table class="wp-list-table widefat fixed posts" cellspacing="0">
  <tr>
    <th width="120">' . __("Contact Name", "sp-cdm") . ':</th>
    <td><input type="text" name="clients-name" value="' . stripslashes($r[0]['name']) . '"></td>
  </tr>
 <tr>
    <th width="120">' . __("Contact Email", "sp-cdm") . ':</th>
    <td><input type="text" name="clients-email" value="' . stripslashes($r[0]['email']) . '"></td>
  </tr>
 <tr>
    <th width="120">' . __("Clontact Phone", "sp-cdm") . ':</th>
    <td><input type="text" name="clients-phone" value="' . stripslashes($r[0]['phone']) . '"></td>
  </tr>
<tr>
    <th width="120">' . __("Client Notes", "sp-cdm") . ':</th>
    <td><textarea type="text" name="clients-notes">' . stripslashes($r[0]['notes']) . '</textarea></td>
  </tr>';
        
        do_action('sp_cdm_client_form');
        echo '<tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="save-clients" value="' . __("Save", "sp-cdm") . '"></td>
  </tr>
</table>
</form>


';
        
        
        
        if ($_GET['id'] != "")
          {
            
            
            
            
            
            if ($_GET['admin'] != '')
              {
                
                $update['admin'] = $_GET['change'];
                $where['id']     = $_GET['admin'];
                $wpdb->update("" . $wpdb->prefix . "sp_cu_clients_assign", $update, $where);
                
                $r_update = $wpdb->get_results($wpdb->prepare("SELECT *
										
	
									 FROM " . $wpdb->prefix . "sp_cu_clients_assign
									 where id = %d order by admin desc", $_GET['admin']), ARRAY_A);
                
                $user = get_userdata($r_update[0]['uid']);
                echo '  <div class="updated">
        <p>' . __('Updated ' . $user->display_name . '!', 'sp-cdm') . '</p>
    </div>';
              }
            $r_users = $wpdb->get_results($wpdb->prepare("SELECT *
										
	
									 FROM " . $wpdb->prefix . "sp_cu_clients_assign
									 where gid = %d ", $_GET['id']), ARRAY_A);
            
            
            do_action('sp_cdm_client_above_associated_users');
            echo '<h3>' . __("Associated Users", "sp-cdm") . '</h3>';
            
            echo '
								
									 
									 
									 <div style="margin:10px">
									<a href="#add-existing-user" class="cdm-add-existing-client button">' . __('Add Existing User', 'sp-cdm') . '</a>
								
								<div class="remodal" data-remodal-id="add-existing-user"><a data-remodal-action="close" class="remodal-close"></a>
										
									<form class="cdm_ajax_add_client_user">
									<input type="hidden" name="action" value="cdm_ajax_add_client_user">
									<input type="hidden" name="gid" value="' . $_GET['id'] . '">		
									';
            echo '<table class="wp-list-table widefat fixed posts" cellspacing="0"><tr><td><label>Choose a user</label></td><td>';
            wp_dropdown_users(array(
                'name' => 'author'
            ));
            echo '</td></tr><tr><td></td><td><input type="submit" name="add-user" value="Add User To Group"></td></tr></table>
									</form>
									</div>
									 </div>
									 
									
									 <table class="wp-list-table widefat fixed posts" cellspacing="0">
	<thead>
	<tr>
<th>' . __("ID", "sp-cdm") . '</th>

<th>' . __("User", "sp-cdm") . '</th>
<th>' . __("Email", "sp-cdm") . '</th>
<th>' . __("Action", "sp-cdm") . '</th>
</tr>
	</thead>';
            for ($i = 0; $i < count($r_users); $i++)
              {
                
                
                unset($r_user);
                $r_user = $wpdb->get_results($wpdb->prepare("SELECT *
										
	
									 FROM " . $wpdb->base_prefix . "users
									 where ID = %d", $r_users[$i]['uid']), ARRAY_A);
                
                
                if ($r_users[$i]['admin'] == 1)
                  {
                    $checked = 'checked="checked"';
                    $rel     = '0';
                  }
                else
                  {
                    $rel     = '1';
                    $checked = '';
                  }
                
                echo '	<tr>
<td>' . $r_user[0]['ID'] . '</td>		
				
<td>' . stripslashes($r_user[0]['display_name']) . '</td>
<td><a href="mailto:' . stripslashes($r_user[0]['user_email']) . '">' . stripslashes($r_user[0]['user_email']) . '</a></td>
<td>

 <a href="#" class="cdm_ajax_delete_client_user" data-id="' . $r_users[$i]['id'] . '" style="margin-right:15px" >' . __("Delete", "sp-cdm") . '</a> 
</td>
</tr>';
                
              }
            echo '</table></div>
				
				
				<div class="clients-right-column">
				<h3>Client Files</h3>';
            
            $files = sp_cdm_get_files_by_client($_GET['id']);
            
            for ($i = 0; $i < count($files); $i++)
              {
                $user = get_userdata($files[$i]['uid']);
                if ($files[$i]['group_id'] != 0)
                  {
                    $group = ' (' . sp_cdm_advanced_get_group_name($files[$i]['group_id']) . ') ';
                  }
                echo '<div class="sp-cdm-client-file"><a href="javascript:cdmViewFile(' . $files[$i]['id'] . ')"><strong>' . $files[$i]['name'] . '</strong> added by <strong>' . $user->display_name . ' ' . $group . '</strong> on <strong>' . date_i18n('' . get_option('date_format') . '  g:i A', strtotime($files[$i]['date'])) . '</strong> </a></div>';
                
                
              }
            
            
            echo '</div><div style="clear:both">';
            
            
          }
        
      }
    function cdm_ajax_load_clients()
      {
        global $wpdb;
        $r = $wpdb->get_results("SELECT *
										
	
									 FROM " . $wpdb->prefix . "sp_cu_clients
									
									 order by name", ARRAY_A);
        
        
        
        
        
        
        
        echo '
								
									 
									 
									
									 <table class="wp-list-table widefat fixed posts" cellspacing="0">

	<thead>
	<tr>
<th>' . __("ID", "sp-cdm") . '</th>
<th>' . __("Name", "sp-cdm") . '</th>
<th>' . __("Users", "sp-cdm") . '</th>
<th>' . __("Action", "sp-cdm") . '</th>
</tr>
	</thead>';
        for ($i = 0; $i < count($r); $i++)
          {
            
            
            unset($r_users);
            $r_users = $wpdb->get_results("SELECT *
										
	
									 FROM " . $wpdb->prefix . "sp_cu_clients_assign
									 where gid = " . $r[$i]['id'] . "", ARRAY_A);
            
            
            echo '	<tr>
<td>' . $r[$i]['id'] . '</td>				
<td>' . stripslashes($r[$i]['name']) . '</td>
<td>' . count($r_users) . '</td>
<td>

 <a href="#" data-id="' . $r[$i]['id'] . '" class="cdm_ajax_delete_client client_page button" style="margin-right:15px" >' . __("Delete", "sp-cdm") . '</a> 
<a href="admin.php?page=sp-client-document-manager-clients&function=edit&id=' . $r[$i]['id'] . '" class="button">' . __("Modify", "sp-cdm") . '</a></td>
</tr>';
            
          }
        echo '</table>';
        
        die(0);
      }
    function view()
      {
        global $wpdb;
        echo '<h2>' . __("Clients", "sp-cdm") . '</h2>' . sp_client_upload_nav_menu() . '';
        
        
        
        
        
        
        
        
        
        if ($_GET['function'] == 'add' or $_GET['function'] == 'edit')
          {
            
            $this->add();
            
            
            
            
          }
        else
          {
            
            echo '
			
			<script type="text/javascript">
			
			jQuery( document ).ready(function() {
			cdm_ajax_load_clients("#sp_cdm_clients_list");
			});
			</script>
			 <div style="margin:10px">
									 <a href="admin.php?page=sp-client-document-manager-clients&function=add" class="button">' . __("Add A Client", "sp-cdm") . '</a>
									 </div>
			<div id="sp_cdm_clients_list"></div>';
            
            
            
          }
      }
    
    
  }

?>