<?php 

if(!class_exists('cdmPremiumSettings')){
class cdmPremiumSettings{
	
		function __construct(){
			
	
		}
		
		function depreciated_settings(){
			
			if(get_option('sp_cu_user_projects_thumbs') == 1){
				
				update_option('sp_cu_user_projects_thumbs', 0);
				update_option('sp_cdm_file_list_template','thumbnails');
			}
			
		}
		function license_tab(){
			if(cdm_multisite() == 0){
			echo ' <li><a href="#cdm-tab-license">' . __('Licenses', 'sp-cdm') . '</a></li>';
			}
		}
		function license_content(){
			if(cdm_multisite() == 0){
			echo '<div id="cdm-tab-license"><table class="wp-list-table widefat fixed posts" cellspacing="0"><h2>SP Document Manager Licenses</h2><p>Enter your licenses to receive updates for Smarty Products. <a href="http://smartypantsplugins.com/purchase-history-2/">Click here to view your licenses</a>. <a href="http://support.smartypantsplugins.com/">Click here if you need support.</a>';
			do_action('wp_cdm_premium_licenses');
			echo '</table></div>';
			}
			
		}
		function emails(){
		
		if($_POST['save_options'] != ''){		
		update_option('sp_cu_public_link_email_subject',$_POST['sp_cu_public_link_email_subject'] );
		update_option('sp_cu_public_link_email_cc',$_POST['sp_cu_public_link_email_cc'] );
		update_option('sp_cu_public_link_email',$_POST['sp_cu_public_link_email'] );
		}
		
			
			echo '<h2>Share Public Email Link</h2>
<table class="wp-list-table widefat fixed posts" cellspacing="0">



    <tr>

    <td width="300"><strong>Public Email Link</strong><br><em>This email shares a link to a public file.</em><br><br>Template Tags:<br><br>

	

	[file] = Link to File<br>
	[file_name] = File Name<br>
	[expiration] = Link to File<br>
	
	[email] = Email<br>
	[name] = Name<br>
	[message] = Message<br></td>

    <td>Subject: <input style="width:100%" type="text" name="sp_cu_public_link_email_subject" value="' . get_option('sp_cu_public_link_email_subject') . '"><br>
	CC: <input style="width:100%" type="text" name="sp_cu_public_link_email_cc" value="' . get_option('sp_cu_public_link_email_cc') . '"><br>
	Body:<br>
	';
	echo wp_editor(  stripslashes(get_option('sp_cu_public_link_email')) , 'sp_cu_public_link_email' );
	echo '
	
	 </td>

  </tr>';
  
  echo do_action('sp_cu_email_extra', 'sp_cu_public_link_email');
  
  echo'

 </table>';
			
		}
		function disable_features($disable_features){
		if($_POST['save_options'] != ''){		
	if($_POST['sp_cu_user_disable_revisions'] == "1"){update_option('sp_cu_user_disable_revisions','1' ); }else{update_option('sp_cu_user_disable_revisions','0' );	}	
	if($_POST['sp_cu_user_disable_tags'] == "1"){update_option('sp_cu_user_disable_tags','1' ); }else{update_option('sp_cu_user_disable_tags','0' );	}	
	if($_POST['sp_cu_user_disable_notes'] == "1"){update_option('sp_cu_user_disable_notes','1' ); }else{update_option('sp_cu_user_disable_notes','0' );	}	
		}
		
	if(get_option('sp_cu_user_disable_revisions') == 1){ $sp_cu_user_disable_revisions = ' checked="checked" ';	}else{ $sp_cu_user_disable_revisions = '  '; }
	if(get_option('sp_cu_user_disable_tags') == 1){ $sp_cu_user_disable_tags = ' checked="checked" ';	}else{ $sp_cu_user_disable_tags = '  '; }	
		if(get_option('sp_cu_user_disable_notes') == 1){ $sp_cu_user_disable_notes = ' checked="checked" ';	}else{ $sp_cu_user_disable_notes = '  '; }
		
			
	
			
			echo '
  <tr><td colspan="2"><h3>Premium Addon</h3>
  <p>Some of the features below can be enabled by using a plugin like <a class="thickbox open-plugin-details-modal" href="http://wordpress2.dlgresults.com/wp-admin/plugin-install.php?tab=plugin-information&plugin=user-role-editor&TB_iframe=true&width=772&height=935">User Role Editor</a>. If there is a capability available it will be listed next to the feature</p>
  </tr> 
    <tr>
    <td width="300">File Revisions <br><i>sp_cdm_enable_revisions</i></td>
    <td><input type="checkbox" name="sp_cu_user_disable_revisions"   value="1" '. $sp_cu_user_disable_revisions.'> </td>
  </tr>
   <tr>
    <td width="300">File Tags </td>
    <td><input type="checkbox" name="sp_cu_user_disable_tags"   value="1" '. $sp_cu_user_disable_tags.'> </td>
  </tr>
     <tr>
    <td width="300">File Notes</td>
    <td><input type="checkbox" name="sp_cu_user_disable_notes"   value="1" '. $sp_cu_user_disable_notes.'> </td>
  </tr>
  <tr>
    <td width="300">Categories</td>
    <td><input type="checkbox" name="sp_cu_user_disable_"   value="1" '. $sp_cu_user_disable_notes.'> </td>
  </tr>
 <tr>
 <td >Disable Categories</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][disable_categories]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'disable_categories'). '></td>
 </tr>
 
  <tr>
 <td >Disable Drafts</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][disable_drafts]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'disable_drafts'). '></td>
 </tr>
  <tr>
 <td >Disable File Sanitization</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_sanitization]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_sanitization'). '></td>
 </tr>
  <tr>
 <td >Disable Public File Link</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][public_file_link]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'public_file_link'). '></td>
 </tr>
  <tr>
 <td >Disable Private File Link</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][private_file_link]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'private_file_link'). '></td>
 </tr>
    <tr>
 <td >Disable Edit Tab</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][edit_file_link]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'edit_file_link'). '></td>
 </tr>
   <tr>
 <td >Disable Outgoing Share File Link</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][share_file_link]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'share_file_link'). '></td>
 </tr>
    <tr>
 <td >Disable Share Tab</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][share_file_tab]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'share_file_tab'). '></td>
 </tr>
 <tr>
 <td >Uploader: Disable Zip file</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][upload_zip_file]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'upload_zip_file'). '></td>
 </tr>
  <tr>
 <td >View file: edit category</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][view_file_edit_category]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'view_file_edit_category'). '></td>
 </tr>
  <tr>
 <td >File list: File Name</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_name]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_name'). '></td>
 </tr>
 <tr>
 <td >File list: Date</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_date]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_date'). '></td>
 </tr>
  <tr>
 <td >File list: File size</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_file_size]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_file_size'). '></td>
 </tr>
   <tr>
 <td >File list: Category</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_category]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_category'). '></td>
 </tr>
    <tr>
 <td >File list: Newest Files</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_newest_files]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_newest_files'). '></td>
 </tr>
 <tr>
 <td >File list: Top Downloads</td><td>
 <input type="checkbox" name="sp_cdm_disable_features[premium][file_list_top_downloads]"   value="1" ' . sp_client_upload_settings_checkbox($disable_features, 'premium', 'file_list_top_downloads'). '></td>
 </tr>
<tr>
<tr>

  ';
  sp_client_upload_settings_field('File List: Right Click Context Menu','context_menu','premium',$disable_features);
  sp_client_upload_settings_field('File List: Image','file_image','premium',$disable_features);
  sp_client_upload_settings_field('File List: File meta info','file_meta_info','premium',$disable_features);
  sp_client_upload_settings_field('File List: Toolbox','file_toolbox','premium',$disable_features);
  
  sp_client_upload_settings_field('File List: Toolbox Preview','toolbox_preview','premium',$disable_features);
  sp_client_upload_settings_field('File List: Toolbox Edit','toolbox_edit','premium',$disable_features);
  sp_client_upload_settings_field('File List: Toolbox Share','toolbox_share','premium',$disable_features);
  sp_client_upload_settings_field('File List: Toolbox Download','toolbox_download','premium',$disable_features);
  sp_client_upload_settings_field('File List: Toolbox Delete','toolbox_delete','premium',$disable_features);
  
  
  
		}
		function view(){
			
			
		global $wpdb;
		
		if($_POST['save_options'] != ''){
		

		
			

	
	if($_POST['sp_cu_enable_tags'] == "1"){update_option('sp_cu_enable_tags','1' ); }else{update_option('sp_cu_enable_tags','0' );	}	
	if($_POST['sp_cu_user_projects_thumbs'] == "1"){update_option('sp_cu_user_projects_thumbs','1' ); }else{update_option('sp_cu_user_projects_thumbs','0' );	}
	if($_POST['sp_cu_user_projects_thumbs_pdf'] == "1"){update_option('sp_cu_user_projects_thumbs_pdf','1' ); }else{update_option('sp_cu_user_projects_thumbs_pdf','0' );	}
	if($_POST['sp_cu_user_disable_revisions'] == "1"){update_option('sp_cu_user_disable_revisions','1' ); }else{update_option('sp_cu_user_disable_revisions','0' );	}
	if($_POST['sp_cu_free_uploader'] == "1"){update_option('sp_cu_free_uploader','1' ); }else{update_option('sp_cu_free_uploader','0' );	}
	if($_POST['sp_cu_release_the_kraken'] == "1"){update_option('sp_cu_release_the_kraken','1' ); }else{update_option('sp_cu_release_the_kraken','0' );	}
	if($_POST['sp_cu_release_the_kraken_permissions'] == "1"){update_option('sp_cu_release_the_kraken_permissions','1' ); }else{update_option('sp_cu_release_the_kraken_permissions','0' );	}
	if($_POST['sp_cu_file_direct_access'] == "1"){update_option('sp_cu_file_direct_access','1' ); }else{update_option('sp_cu_file_direct_access','0' );	}
	if($_POST['sp_cu_user_create_folder'] == "1"){update_option('sp_cu_user_create_folder','1' ); }else{update_option('sp_cu_user_create_folder','0' );	}
	if($_POST['sp_cu_free_file_list'] == "1"){update_option('sp_cu_free_file_list','1' ); }else{update_option('sp_cu_free_file_list','0' );	}
	if($_POST['sp_cu_require_category'] == "1"){update_option('sp_cu_require_category','1' ); }else{update_option('sp_cu_require_category','0' );	}
	if($_POST['sp_cu_user_delete_folders'] == "1"){update_option('sp_cu_user_delete_folders','1' ); }else{update_option('sp_cu_user_delete_folders','0' );	}
	if($_POST['sp_cu_user_create_folder_name'] == "1"){update_option('sp_cu_user_create_folder_name','1' ); }else{update_option('sp_cu_user_create_folder_name','0' );	}
	if($_POST['sp_cu_limit_files'] == "1"){update_option('sp_cu_limit_files','1' ); }else{update_option('sp_cu_limit_files','0' );	}
	if($_POST['sp_cu_force_zip'] == "1"){update_option('sp_cu_force_zip','1' ); }else{update_option('sp_cu_force_zip','0' );	}
	
	
	update_option('sp_cu_brand',$_POST['sp_cu_brand']);	
	
	
	
	update_option('sp_cu_convert_images_to_pdf',$_POST['sp_cu_convert_images_to_pdf']);
	update_option('sp_cu_convert_images_to_pdf_types',$_POST['sp_cu_convert_images_to_pdf_types']);
	update_option('sp_cu_folder_button',$_POST['sp_cu_folder_button']);	
	update_option('sp_cu_folder_back_button',$_POST['sp_cu_folder_back_button']);	
	
	update_option('sp_cu_user_create_folder_template',$_POST['sp_cu_user_create_folder_template']);	
	update_option('sp_cu_file_name_behavior',$_POST['sp_cu_file_name_behavior']);	
	update_option('sp_cu_default_ordering',$_POST['sp_cu_default_ordering']);	
		update_option('sp_cu_default_ordering_type',$_POST['sp_cu_default_ordering_type']);
	update_option('sp_cu_jqueryui_theme',$_POST['sp_cu_jqueryui_theme']);	
	update_option('sp_cu_cat_text',$_POST['sp_cu_cat_text']);			
	update_option('sp_cu_file_delete',$_POST['sp_cu_file_delete']);		
		update_option('sp_cdm_premium_license',$_POST['sp_cdm_premium_license']);				
				update_option('sp_cu_image_magick_path',$_POST['sp_cu_image_magick_path']);	
				
				update_option('sp_cu_project_ordering_method',$_POST['sp_cu_project_ordering_method']);	
				
			
				
						
	}
	
		
	
		
		if(get_option('sp_cu_enable_tags') == 1){ $sp_cu_enable_tags = ' checked="checked" ';	}else{ $sp_cu_enable_tags = '  '; }
		if(get_option('sp_cu_user_projects_thumbs') == 1){ $sp_cu_user_projects_thumbs = ' checked="checked" ';	}else{ $sp_cu_user_projects_thumbs = '  '; }
		if(get_option('sp_cu_user_projects_thumbs_pdf') == 1){ $sp_cu_user_projects_thumbs_pdf = ' checked="checked" ';	}else{ $sp_cu_user_projects_thumbs_pdf = '  '; }
		if(get_option('sp_cu_user_disable_revisions') == 1){ $sp_cu_user_disable_revisions = ' checked="checked" ';	}else{ $sp_cu_user_disable_revisions = '  '; }
		if(get_option('sp_cu_free_uploader') == 1){ $sp_cu_free_uploader = ' checked="checked" ';	}else{ $sp_cu_free_uploader = '  '; }
		if(get_option('sp_cu_release_the_kraken') == 1){ $sp_cu_release_the_kraken = ' checked="checked" ';	}else{ $sp_cu_release_the_kraken = '  '; }
		if(get_option('sp_cu_release_the_kraken_permissions') == 1){ $sp_cu_release_the_kraken_permissions = ' checked="checked" ';	}else{ $sp_cu_release_the_kraken_permissions = '  '; }
		if(get_option('sp_cu_file_direct_access') == 1){ $sp_cu_file_direct_access = ' checked="checked" ';	}else{ $sp_cu_file_direct_access = '  '; }
		if(get_option('sp_cu_user_create_folder') == 1){ $sp_cu_user_create_folder = ' checked="checked" ';	}else{ $sp_cu_user_create_folder = '  '; }
		if(get_option('sp_cu_free_file_list') == 1){ $sp_cu_free_file_list = ' checked="checked" ';	}else{ $sp_cu_free_file_list = '  '; }
		if(get_option('sp_cu_require_category') == 1){ $sp_cu_require_category = ' checked="checked" ';	}else{ $sp_cu_require_category = '  '; }
		if(get_option('sp_cu_user_delete_folders') == 1){ $sp_cu_user_delete_folders = ' checked="checked" ';	}else{ $sp_cu_user_delete_folders = '  '; }
		if(get_option('sp_cu_user_create_folder_name') == 1){ $sp_cu_user_create_folder_name = ' checked="checked" ';	}else{ $sp_cu_user_create_folder_name = '  '; }
		if(get_option('sp_cu_limit_files') == 1){ $sp_cu_limit_files = ' checked="checked" ';	}else{ $sp_cu_limit_files = '  '; }
		
		if(get_option('sp_cu_force_zip') == 1){ $sp_cu_force_zip = ' checked="checked" ';	}else{ $sp_cu_force_zip = '  '; }
		
		
			$sp_cu_folder_back_button = get_option('sp_cu_folder_back_button');
			if($sp_cu_folder_back_button!= ''){
				$sp_cu_folder_back_button = '<img src="'.$sp_cu_folder_back_button.'" >';	
					
				}
		$sp_cu_folder_button = get_option('sp_cu_folder_button');
			if($sp_cu_folder_button != ''){
				$sp_cu_folder_button = '<img src="'.$sp_cu_folder_button.'" >';	
					
				}
		
		if(class_exists('Imagick')){
			 $Imagick = new Imagick;
		      $imagick_version =  $Imagick->getVersion();
    
    $imagick_version_number = $imagick_version['versionNumber'];
    $imagick_version_string = $imagick_version['versionString'];
	$imagemagick = '<span style="color:green">ImageMagick Installed! Version: '. $imagick_version_number.'</span>';

	
		}else{
		$imagemagick = '<span style="color:red">ImageMagick Not installed on your server</span>';
		}
	echo '   <h2>Premium Settings</h2>
		
		 <table class="wp-list-table widefat fixed posts" cellspacing="0">
		 
	';
	
	
	
  echo '<tr>
    <td width="300"><strong>File list template: </strong><br><em>Choose a template for your file list</em></td>
    <td>';
	
	$templates = cdm_premium_file_list::file_list_templates();
	$default = get_option('sp_cdm_file_list_template', 'responsive');
	
	
	echo '<select name="sp_cdm_file_list_template">';
	foreach($templates as $template_key=>$template_name){
			if($default  == $template_key){
				$selected= 'selected="selected"';
			}else{
				$selected = '';
				}
			echo '<option value="'.$template_key.'" '.$selected .'>'.$template_name.'</option>';
	}
	echo '</select>';
	echo '</td>
	</tr>';
  
  
  
  
   echo '<tr>
    <td width="300"><strong>File name Behavior </strong><br><em>This will bypass the file name behavior</em></td>
    <td><select name="sp_cu_file_name_behavior">';
	
	if(get_option('sp_cu_file_name_behavior') != ''){
		echo '<option value="'.get_option('sp_cu_file_name_behavior').'" selected>'.get_option('sp_cu_file_name_behavior').'</option>';
	}
		
		echo '<option value="">Default: Unchecked and allow user to write a filename</option>
		<option value="checked">Checked: The Automatic filename is checked by default</option>
		<option value="hidden">Hidden: The filename field is hidden by default</option>
		<option value="specify">Specify: The user must specify a filename and default name is not used at all</option>
		</select>
		</td>
  </tr>
		 <tr>
    <td width="300"><strong>Jquery UI Theme</strong><br><em>You can change the theme here or remove the theme if you use your own custom theme. The default theme is smoothness. <a href="http://jqueryui.com/themeroller/" target="_blank">Click here to view all themes</a>.</em></td>
    <td><select name="sp_cu_jqueryui_theme">
			<option value="'.get_option('sp_cu_jqueryui_theme').'" selected="selected">'.get_option('sp_cu_jqueryui_theme').'</option>
			<option value="none">none</option>
			<option value="smoothness">Smoothness</option>
		 </select></td>
  </tr>
		  <tr>
    <td width="300"><strong>Remove Branding</strong><br><em>Change the branding to your own</em></td>
    <td><input type="text" name="sp_cu_brand"   value="'.get_option('sp_cu_brand', 'Smarty Pants Client Document Manager').'" style="width:100%"> </td>
  </tr>
	
   <tr>
    <td width="300"><strong>Disable Folder Deleting</strong><br><em>Remove the ability for users to delete and edit folders</em></td>
    <td><input type="checkbox" name="sp_cu_user_delete_folders"   value="1" '. $sp_cu_user_delete_folders.'> </td>
  </tr>
    
    <tr>
    <td width="300"><strong>Create thumbnails for file types with image magick?</strong><br><em>You must have Image Magick Installed on your server. You can also set the imageMagick path. The default path is "/usr/local/bin/convert" but this varies from server to server, so if you have imageMagick in a different location then please enter it here. If you do not know your imageMagick path you can find it using the phpinfo() command to display your php configuration or just ask your webhost.
	
	<br> '. $imagemagick.' </em></td>
    <td><input type="checkbox" name="sp_cu_user_projects_thumbs_pdf"   value="1" '. $sp_cu_user_projects_thumbs_pdf.'><br><br><strong>Custom Path:</strong> <input type="text" name="sp_cu_image_magick_path"  value="'.get_option('sp_cu_image_magick_path').'" >';
	
	if(class_exists('imagick')){
		$info = new Imagick();
		$formats = $info->queryFormats();
	
		if(count($formats) > 0){
		echo '<div style="width:100%;font-size:9px !important;"><p><strong>Supported Formats</strong><br>';
		foreach($formats as $key => $value){
			echo ''.$value.', ';
		}
		echo '</div>';	
		}
	}
	
	if(get_option('sp_cu_convert_images_to_pdf') == 0 or get_option('sp_cu_convert_images_to_pdf') == ''){
	$pdf_one = 'selected="selected"';
	}elseif(get_option('sp_cu_convert_images_to_pdf') == 1){
	$pdf_two = 'selected="selected"';
	}elseif(get_option('sp_cu_convert_images_to_pdf') == 2){	
	$pdf_three = 'selected="selected"';
	}
	echo ' <strong>Disable File Types: </strong><input type="text" name="sp_cu_image_magick_path_disable_types"  value="'.get_option('sp_cu_image_magick_path_disable_types').'" ><br>
<em>Sometimes some file types require 3rd party software installed on your server, please consult your server or disable the extention here. Seperate extentions with a comma</em></td>
  </tr>
   
 
  </tr>
 <tr>
    <td width="300"><strong>Convert Images to PDF</strong><br><em>Choose settings to convert images to pdf '.$imagemagick.'(requires imagemagick)</em></td>
    <td><select name="sp_cu_convert_images_to_pdf">
			<option value="0" '.$pdf_one.'>Do not convert any images to PDF</option>
			<option value="1" '.$pdf_two.'>Allow the user to choose to convert images to PDF</option>
			<option value="2" '.$pdf_three.'>Convert all images to PDF automatically</option>
		</select> </td>
  </tr>
  <tr>
    <td width="300"><strong>Image types to convert</strong><br><em>Seperate the image type with a comma</em></td>
    <td><input type="text" name="sp_cu_convert_images_to_pdf_types"  value="'.get_option('sp_cu_convert_images_to_pdf_types', 'jpg,jpeg,png,gif,bmp').'"  size="80"> </td>
  </tr>
  
    <tr>
    <td width="300"><strong>Direct Link to file</strong><br><em>This will bypass the file view popup</em></td>
    <td><input type="checkbox" name="sp_cu_file_direct_access"   value="1" '. $sp_cu_file_direct_access.'> </td>
  </tr>
  
     <tr>
    <td width="300"><strong>Use The Free Version Uploader</strong><br><em>If you do not want to use the premium uploader enable this option. </em></td>
    <td><input type="checkbox" name="sp_cu_free_uploader"   value="1" '. $sp_cu_free_uploader.'> </td>
  </tr>
       <tr>
    <td width="300"><strong>Use The Free Version File List</strong><br><em>If you do not want to use the premium responsive file list mode check this box . </em></td>
    <td><input type="checkbox" name="sp_cu_free_file_list"   value="1" '. $sp_cu_free_file_list.'> </td>
  </tr>
         <tr>
    <td width="300"><strong>Require Category to be chosen</strong><br><em>Require the the user has to choose a category to upload a file. </em></td>
    <td><input type="checkbox" name="sp_cu_require_category"   value="1" '. $sp_cu_require_category.'> </td>
  </tr>
  <tr>
    <td width="300"><strong>Categories Text</strong><br><em>This is the text you want to call categories, for example you may want to use it as a status.</em></td>
    <td><input type="text" name="sp_cu_cat_text"  value="'.get_option('sp_cu_cat_text').'"  size="80"> </td>
  </tr>
    <tr>
    <td width="300"><strong>Default File Category</strong><br><em>Defeault Category of file when uploaded.</em></td>
    <td>'.sp_cdm_categories_dropdown_admin('sp_cu_cat_default',get_option('sp_cu_cat_default'),'None').'</td>
  </tr>
   <tr>
    <td width="300"><strong>File Deletion Period</strong><br><em>How many days should a file exist before its deleted from the system? Leave blank if you do not wish to use this function.</em></td>
    <td><input type="text" name="sp_cu_file_delete"  value="'.get_option('sp_cu_file_delete').'"  size="80"> </td>
  </tr>    <tr>
    <td width="300"><strong>Limit File Types</strong><br><em>Limit to specific file types below, comma seperate each file type.</em></td>
    <td><input type="text" name="sp_cu_limit_file_types"  value="'.get_option('sp_cu_limit_file_types').'"  size="80"> </td>
  </tr>
   <tr>
    <td width="300"><strong>Limit 1 file at a time</strong><br><em>Check this to allow uploading 1 file at a time</em></td>
    <td><input type="checkbox" name="sp_cu_limit_files"   value="1" '. $sp_cu_limit_files.'> </td>
  </tr>
    <tr>
    <td width="300"><strong>Force zip multiple files</strong><br><em>Check this if you want multiple files force zipped</em></td>
    <td><input type="checkbox" name="sp_cu_force_zip"   value="1" '. $sp_cu_force_zip.'> </td>
  </tr>
   <tr>
    <td width="300"><strong>Force Stream File Types</strong><br><em>Stream the following file types, the file will be linked and stream in the browser. comma seperate each file type.</em></td>
    <td><input type="text" name="sp_cu_stream_file_types"  value="'.get_option('sp_cu_stream_file_types').'"  size="80"> </td>
  </tr>
   <tr>
    <td width="300"><strong>Show All Files to All Users! </strong><br><em>If you want all users to see all files then enable this.  </em></td>
    <td><input type="checkbox" name="sp_cu_release_the_kraken"   value="1" '. $sp_cu_release_the_kraken.'> </td>
  </tr>
     <tr>
    <td width="300"><strong>Allow users to upload to any folder </strong><br><em>If this option is enabled users will have free reign.  </em></td>
    <td><input type="checkbox" name="sp_cu_release_the_kraken_permissions"   value="1" '. $sp_cu_release_the_kraken_permissions.'> </td>
  </tr>
   <tr>
    <td width="300"><strong>Upload File Size Limit</strong><br><em>If you want to limit the file size limit please fill in that info here. Example: 100   (this would be 100 Megabytes) .</em></td>
    <td><input type="text" name="sp_cu_limit_file_size"  value="'.get_option('sp_cu_limit_file_size').'"  style="width:60px">MB </td>
  </tr><tr>
      <td width="300"><strong>Default Sorting</strong><br><em>Default ordering of files.</em></td>
    <td><select name="sp_cu_default_ordering" >
		<option value="'.get_option('sp_cu_default_ordering').'" selected="selected">'.get_option('sp_cu_default_ordering').'</option>
		<option value="name">Name</option>
		<option value="file">File Name</option>
		<option value="date">Date</option>
		<option value="uid">User ID</option>
		<option value="cid">Category</option>
		<option value="">Use System Default</option>
		</select>
	 </td>
  </tr>
  <tr>
      <td width="300"><strong>Sorting Type</strong><br><em>Ascending or descending?.</em></td>
    <td><select name="sp_cu_default_ordering_type" >
		<option value="'.get_option('sp_cu_default_ordering_type').'" selected="selected">'.get_option('sp_cu_default_ordering_type').'</option>
		<option value="asc">asc</option>
		<option value="desc">desc</option>
		
		</select>
	 </td>
  </tr>
  
    <tr>
      <td width="300"><strong>Sorting Type</strong><br><em>Order folders by name or custom order?</em></td>
    <td><select name="sp_cu_project_ordering_method" >
		<option value="'.get_option('sp_cu_project_ordering_method', 'Name').'" selected="selected">'.get_option('sp_cu_project_ordering_method', 'Name').'</option>
		<option value="Name">Name</option>
		<option value="Custom Order">Custom Order</option>
		
		</select>
	 </td>
  </tr>
    <tr>
    <td width="300"><strong>Use a template when a new user is created</strong><br><em>When a new user is added the files in the selected folder will be automatically added to their file area. If you choose to create a folder based off their username please check the indicating box as well. Doing so all the files and folders will be created inside a sub folder. </em></td>
    <td><input type="checkbox" name="sp_cu_user_create_folder"   value="1" '. $sp_cu_user_create_folder.'>
			 '. sp_cdm_folder_dropdown_parents('sp_cu_user_create_folder_template',get_option('sp_cu_user_create_folder_template')).' Create in a sub folder based off the username? <input type="checkbox" name="sp_cu_user_create_folder_name" value="1"  '. $sp_cu_user_create_folder_name.'> 
	
	 </td>
  </tr>
  
  <tr>
    <td>'.__("Folder Image","sp-wpfh").'</td>
     <td >

 <div class="wpfh-metabox-table">
	<input id="_unique_name" name="sp_cu_folder_button" type="hidden"  class="image-value"  value="'.get_option('sp_cu_folder_button').'" />
	<input id="_unique_name_button" class="button" name="_unique_name_button" type="text" value="Upload" />
	
	<div class="preview-image" style="margin:10px">'.$sp_cu_folder_button.'</div>
</div>



</td>
  </tr>
  <tr>
    <td>'.__("Folder Back Button Image","sp-wpfh").'</td>
     <td >

 <div class="wpfh-metabox-table">
	<input id="_sp_cu_folder_back" name="sp_cu_folder_back_button" class="image-value" type="hidden" value="'.get_option('sp_cu_folder_back_button').'" />
	<input id="_sp_cu_folder_back_button" class="button" name="_sp_cu_folder_back_button" type="text" value="Upload" />
	
	<div class="preview-image" style="margin:10px">'.$sp_cu_folder_back_button.'</div>
</div>



</td>
  </tr>
  
    <td>&nbsp;</td>
    <td><input type="submit" name="save_options" value="Save Options"></td>
  </tr></table>';
  
  
			
		}
		
		
		function network_tab(){
			if ( is_multisite() ) { 
				add_menu_page( __("CDM Licenses",'sp-cdm'), __("CDM Licenses",'sp-cdm'), 'sp_cdm', 'cdm-document-licenses', array($this,'network_tab_content'));	
			}
		}
		function network_tab_content(){
			if ( is_multisite() ) { 
				echo '<div id="cdm-tab-license"><table class="wp-list-table widefat fixed posts" cellspacing="0"><h2>SP Document Manager Licenses</h2><p>Enter your licenses to receive updates for Smarty Products. <a href="http://smartypantsplugins.com/purchase-history-2/">Click here to view your licenses</a>. <a href="http://support.smartypantsplugins.com/">Click here if you need support.</a>';
			do_action('wp_cdm_premium_licenses');
			echo '</table></div>';
			}
		}
	
}



$cdmPremiumSettings = new cdmPremiumSettings;
add_action('network_admin_menu', array($cdmPremiumSettings,'network_tab'));
add_action('cdm_premium_settings',array($cdmPremiumSettings,'view'));
add_action('init',array($cdmPremiumSettings,'depreciated_settings'));
add_action('sp_cdm_disable_features',array($cdmPremiumSettings,'disable_features'));

add_action('sp_cdm_settings_email',array($cdmPremiumSettings,'emails'));

add_action('sp_cdm_settings_add_tab',array($cdmPremiumSettings,'license_tab'));
add_action('sp_cdm_settings_add_tab_content',array($cdmPremiumSettings,'license_content'));



}