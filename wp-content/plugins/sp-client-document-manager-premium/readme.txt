=== Plugin Name ===
Contributors: smartypants
Donate link: http://smartypantsplugins.com/donate/
Tags: client upload,project management, document manager, file uploader, customer file manager, customer files, document version system
Requires at least: 2.0.2
Tested up to: 3.9
Stable tag: 3.2.6

Premium base for SP Client Document Manager

== Description ==

Premium base for SP Client Document Manager


== Installation ==

* Upload the plugin to your plugins folder or use the built in wordpress uploader


== Frequently Asked Questions ==

= How come I'm getting a 404 error? =

This could be one of two reasons, either you did not install theme my login or you're running wordpress in a directory in which you can go to settings and set the directory for wordpress.


== Changelog ==

= 2.0.0 =
* Created first plugin version of premium
	