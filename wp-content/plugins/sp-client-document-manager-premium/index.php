<?php

/*
Plugin Name: SP Client Document Manager Premium
Plugin URI: http://www.smartypantsplugins.com
Description: Premium base file for SP Client Document Manager
Author: Anthony Brown
Author URI: http://www.smartypantsplugins.com
Version: 3.5.6.4
*/



global $sp_client_upload_premium;

$sp_client_upload_premium = "3.5.6.4";
define('EDD_CDM_VERSION', $sp_client_upload_premium);
define('EDD_CDM_ITEM_NAME', 'SP Client Document Manager Premium');
define('EDD_CDM_LICENSE_KEY', trim(get_option('sp_cdm_premium_license')));
define('EDD_CDM_STORE_URL', 'http://www.smartypantsplugins.com');
define('EDD_CDM_DEBUG', true);
define('SP_CDM_PREMIUM_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('SP_CDM_PREMIUM_PLUGIN_URL', plugin_dir_url(__FILE__));
define('SP_CDM_PREMIUM_TEMPLATE_DIR', plugin_dir_path(__FILE__) . 'templates/');
define('SP_CDM_PREMIUM_TEMPLATE_URL', plugin_dir_url(__FILE__) . 'templates/');
include_once '' . dirname(__FILE__) . '/smarty.php';
include_once '' . dirname(__FILE__) . '/functions.php';
include_once '' . dirname(__FILE__) . '/user/file-list.php';
include_once '' . dirname(__FILE__) . '/classes/ajax.php';
include_once '' . dirname(__FILE__) . '/classes/uploader.php';
include_once '' . dirname(__FILE__) . '/admin/emails.php';
include_once '' . dirname(__FILE__) . '/admin/projects.php';
include_once '' . dirname(__FILE__) . '/admin/clients.php';
include_once '' . dirname(__FILE__) . '/admin/categories.php';
include_once '' . dirname(__FILE__) . '/admin/history.php';
include_once '' . dirname(__FILE__) . '/admin/forms.php';
include_once '' . dirname(__FILE__) . '/admin/groups.php';
include_once '' . dirname(__FILE__) . '/admin/options.php';
include_once '' . dirname(__FILE__) . '/user/shortcode.php';
include_once '' . dirname(__FILE__) . '/user/context.php';
include_once '' . dirname(__FILE__) . '/user/groups.php';
include_once '' . dirname(__FILE__) . '/user/folders.php';
include_once '' . dirname(__FILE__) . '/user/clients.php';
include_once '' . dirname(__FILE__) . '/user/clients.widget.php';
include_once '' . dirname(__FILE__) . '/user/download.php';
include_once '' . dirname(__FILE__) . '/user/viewfile.php';
include_once '' . dirname(__FILE__) . '/user/preview.php';
include_once '' . dirname(__FILE__) . '/classes/rest.api.php';
include_once '' . dirname(__FILE__) . '/user/woocommerce.php';
include_once '' . dirname(__FILE__) . '/ajax.php';
if (!class_exists('TGM_Plugin_Activation'))
  {
    
    require_once '' . dirname(__FILE__) . '/classes/download-plugins.php';
    
  }
if (!class_exists('EDD_SL_Plugin_Updater'))
  {
    // load our custom updater if it doesn't already exist
    include_once(dirname(__FILE__) . '/EDD_SL_Plugin_Updater.php');
  }
add_action('init', 'cdm_test_mail');
function cdm_test_mail()
  {
    
    if ($_GET['cdm_test_mail'] == 1)
      {
        add_filter('wp_mail_content_type', 'set_html_content_type');
        $mail = wp_mail('anthony@dlgresults.com', 'This is a test mail', 'just a test');
        remove_filter('wp_mail_content_type', 'set_html_content_type');
        
        if ($mail == true)
          {
            echo 'Mail sent';
          }
        else
          {
            echo 'Mail not sent';
            
          }
        exit;
        
      }
    
  }
function cdm_get_premium_content_url()
  {
    
    $url = plugin_dir_url(__FILE__);
    if (MULTISITE == true)
      {
        
        
        if (SUBDOMAIN_INSTALL == false)
          {
            $bloginfo = get_blog_details(get_current_blog_id());
            
            $url = str_replace($bloginfo->path, '/', $url);
          }
      }
    
    return $url;
    
  }
$premium_add_file_link = "javascript:sp_cu_dialog_premium()";

$sp_premium_license = new sp_premium_license;

add_action('sp_cdm_errors', array(
    $sp_premium_license,
    'check_license'
));
add_action('wp_cdm_premium_licenses', array(
    $sp_premium_license,
    'license_form'
));


add_action('wp_ajax_funeralpress_activate_' . $sp_premium_license->namesake . '', array(
    $sp_premium_license,
    'activate_license'
));
add_action('wp_ajax_nopriv_funeralpress_activate_' . $sp_premium_license->namesake . '', array(
    $sp_premium_license,
    'activate_license'
));

add_action('wp_ajax_funeralpress_deactivate_' . $sp_premium_license->namesake . '', array(
    $sp_premium_license,
    'deactivate_license'
));
add_action('wp_ajax_nopriv_funeralpress_deactivate_' . $sp_premium_license->namesake . '', array(
    $sp_premium_license,
    'deactivate_license'
));

class sp_premium_license
  {
    
    function __construct()
      {
        
        $this->namesake            = 'sp_premium';
        $this->version             = "3.5.6.4";
        $this->name                = 'SP Client Document Manager Premium';
        $this->item_name           = $this->name;
        $this->license_option_name = 'sp_cdm_premium_license';
        $this->license_key         = trim(get_option($this->license_option_name));
        $this->store_url           = 'http://www.smartypantsplugins.com';
        
        
        
        if (!class_exists('EDD_SL_Plugin_Updater'))
          {
            // load our custom updater if it doesn't already exist
            include(dirname(__FILE__) . '/EDD_SL_Plugin_Updater.php');
            
          }
        // setup the updater
        $edd_updater = new EDD_SL_Plugin_Updater($this->store_url, '' . dirname(__FILE__) . '/index.php', array(
            'version' => $this->version, // current version number
            'license' => $this->license_key, // license key (used get_option above to retrieve from DB)
            'item_name' => $this->item_name, // name of this plugin
            'author' => 'Anthony Brown' // author of this plugin
        ));
        
        
        
        
        
      }
    
    
    
    
    
    
    
    
    
    function license_form()
      {
        
        
        
        
        
        
        $status = $status = get_option('edd_' . $this->namesake . '_license_data');
        
        if ($status != false)
          {
            
            
            if (time() > strtotime($status->expires))
              {
                // data to send in our API request
                $api_params = array(
                    'edd_action' => 'check_license',
                    'license' => trim(get_option($this->license_option_name)),
                    'item_name' => urlencode($this->item_name) // the name of our product in EDD
                );
                
                
                $response = wp_remote_get(add_query_arg($api_params, $this->store_url), array(
                    'timeout' => 15,
                    'body' => $api_params,
                    'sslverify' => false
                ));
                
                $license_data = json_decode(wp_remote_retrieve_body($response));
                
                update_option('edd_' . $this->namesake . '_license_data', $license_data);
                update_option('edd_' . $this->namesake . '_license_active', $license_data->license);
                $status = get_option('edd_' . $this->namesake . '_license_data');
                
              }
            
            
            if ($status->license == 'valid')
              {
                $license_data = '<a href="#" class="sp-license-' . $this->namesake . '-deactivate button" >Deactivate</a> <span style="color:green">Valid License. Expires on: ' . date("F j, Y", strtotime($status->expires)) . '</span> ';
              }
            else
              {
                if ($status->error != '')
                  {
                    $license_data = '<a href="#" class="sp-license-' . $this->namesake . '-activate button" >Activate</a> <span style="color:red">Invalid License. ' . $status->error . '</span> ';
                  }
                else
                  {
                    $license_data = '<a href="#" class="sp-license-' . $this->namesake . '-activate button" >Activate</a> <span style="color:red">Invalid License. Expires on: ' . date("F j, Y", strtotime($status->expires)) . '</span> ';
                  }
              }
            
          }
        else
          {
            $license_data = '<a href="#" class="sp-license-' . $this->namesake . '-activate button" >Activate</a> ';
          }
        
        echo ' <tr>

    <td width="300"><strong>' . $this->name . ' ' . __('License', 'sp-wpfh') . '</strong><br><em>' . __('Your serial key for', 'sp-wpfh') . ' ' . $this->name . ' </em></td>

    <td>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {

			$(".sp-license-' . $this->namesake . '-activate").on("click", function(){
			jQuery.post(ajaxurl, {"action": "funeralpress_activate_' . $this->namesake . '", "license":$(".' . $this->license_option_name . '").val()}, function(response) {console.log(response);
			location.reload();
			});
			
			});
			
			$(".sp-license-' . $this->namesake . '-deactivate").on("click", function(){
			jQuery.post(ajaxurl, {"action": "funeralpress_deactivate_' . $this->namesake . '"}, function(response) {console.log(response);
				location.reload();
				});
		
			});			
		
	});
	</script>
	
	<input style="width:400px" type="text" name="' . $this->license_option_name . '" class="' . $this->license_option_name . '"  value="' . get_option($this->license_option_name) . '"  ><span></span> ' . $license_data . ' </td>

  </tr>';
        
      }
    function check_license()
      {
        
        if ($_GET['' . $this->license_option_name . ''] == 'ignore')
          {
            update_option('' . $this->license_option_name . '_ignore', '1');
          }
        $ignored = 0;
        if (get_option('' . $this->license_option_name . '_ignore') != false)
          {
            $ignored = get_option('' . $this->license_option_name . '_ignore');
          }
        $ignored = apply_filters('sp_cdm_check_license_flag', $ignored);
        
        $license_status = get_option('edd_' . $this->namesake . '_license_data');
        
        if (($license_status == '' or $license_status->license == 'invalid') && $ignored != 1)
          {
            
            echo '<div class="updated">
        <p>It looks like you are not receiveing automatic updates for <strong> ' . $this->name . '</strong>.  Please enter your license key for automatic updates. Please visit the settings page and check your license settings <a href="admin.php?page=sp-client-document-manager-settings" class="button">Go to settings</a> <a style="margin-left:20px" class="button" href="admin.php?page=sp-client-document-manager-settings&' . $this->license_option_name . '=ignore" >I don\'t need automatic updates</a> </p>
		
    
	</div>';
            
          }
      }
    
    
    # License stuff just ignore
    function activate_license()
      {
        global $edd_options;
        
        
        update_option($this->license_option_name, $_POST['license']);
        
        // data to send in our API request
        $api_params = array(
            'edd_action' => 'activate_license',
            'license' => trim(get_option($this->license_option_name)),
            'item_name' => urlencode($this->item_name) // the name of our product in EDD
        );
        
        // Call the custom API.
        $response = wp_remote_get(add_query_arg($api_params, $this->store_url), array(
            'timeout' => 15,
            'body' => $api_params,
            'sslverify' => false
        ));
        
        // make sure the response came back okay
        if (is_wp_error($response))
            return false;
        
        // decode the license data
        $license_data = json_decode(wp_remote_retrieve_body($response));
        echo json_encode($license_data);
        update_option('edd_' . $this->namesake . '_license_data', $license_data);
        update_option('edd_' . $this->namesake . '_license_active', $license_data->license);
        
        die();
      }
    
    
    function deactivate_license()
      {
        global $edd_options;
        
        // data to send in our API request
        $api_params = array(
            'edd_action' => 'deactivate_license',
            'license' => trim(get_option($this->license_option_name)),
            'item_name' => urlencode($this->item_name) // the name of our product in EDD
        );
        
        
        // Call the custom API.
        $response = wp_remote_get(add_query_arg($api_params, $this->store_url), array(
            'timeout' => 15,
            'body' => $api_params,
            'sslverify' => false
        ));
        
        // decode the license data
        $license_data = json_decode(wp_remote_retrieve_body($response));
        echo json_encode($license_data);
        delete_option('edd_' . $this->namesake . '_license_data');
        delete_option('edd_' . $this->namesake . '_license_data');
        delete_option($this->license_option_name);
        die();
      }
    
  }





function sp_cdm_check_admin_caps_premium()
  {
    global $current_user;
    
    if ($current_user != '')
      {
        
        
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm'))
          {
            @require_once(ABSPATH . 'wp-includes/pluggable.php');
            $role = get_role('administrator');
            $role->add_cap('sp_cdm');
            $role->add_cap('sp_cdm_vendors');
            $role->add_cap('sp_cdm_settings');
            $role->add_cap('sp_cdm_projects');
            $role->add_cap('sp_cdm_uploader');
            
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_help'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_help');
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_forms'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_forms');
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_groups'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_groups');
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_categories'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_categories');
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_top_menu'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_top_menu');
          }
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_show_folders_as_nav'))
          {
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_show_folders_as_nav');
          }
        
        
        
        
        if (user_can($current_user->ID, 'manage_options') && !current_user_can('sp_cdm_enable_revisions'))
          {
            @require_once(ABSPATH . 'wp-includes/pluggable.php');
            $role = get_role('administrator');
            $role->add_cap('sp_cdm_enable_revisions');
          }
        
      }
  }



function sp_cdm_premium_styles()
  {
    
    
    wp_enqueue_style('cdm-premium-style', plugins_url('css/style.css', __FILE__), array(), EDD_CDM_VERSION);
    wp_enqueue_style('cdm-premium-tags', plugins_url('css/jquery.tags.css', __FILE__), array(), EDD_CDM_VERSION);
    do_action('sp_cdm_premium_styles');
    
    
  }

add_action('sp_cdm/enqueue_scripts', 'sp_cdm_premium_styles');
add_action('sp_cdm/enqueue_scripts', 'sp_cdm_premium_styles');
//add_action('cdm_add_hidden_html',array($cdmPremiumUploader,'construct'));


function sp_cdm_premium_scripts()
  {
    
    
    
    
    wp_enqueue_script('hoverIntent');
    
    
    wp_enqueue_script('jquery-print', plugins_url('js/jquery.print.js', __FILE__), array(
        'jquery',
        'jquery-ui-core',
        'jquery-ui-widget'
    ));
    wp_enqueue_script('juqeruy-premium-tags', plugins_url('js/jquery.tags.js', __FILE__), array(
        'jquery',
        'jquery-ui-core',
        'jquery-ui-widget',
        'jquery-ui-autocomplete'
    ));
    
    wp_enqueue_script('jquery-bootstrap-conflict', plugins_url('js/bootstrap-button-conflict.js', __FILE__), array(
        'jquery',
        'jquery-ui-core',
        'jquery-ui-button'
    ));
    do_action('sp_cdm_premium_scripts');
    
  }
add_action('sp_cdm/enqueue_scripts', 'sp_cdm_premium_scripts');


add_action('admin_init', 'sp_cdm_check_admin_caps_premium');
add_action('sp_cu_admin_menu', 'sp_client_upload_menu_premium');

if (isset($_GET['update_tables']))
  {
    if ($_GET['update_tables'] == 1)
      {
        
        sp_client_upload_install_premium();
        echo '<script type="text/javascript">alert("Tables Updated");</script>';
      }
  }
include_once '' . dirname(__FILE__) . '/app.php';

sp_client_upload_install_premium();






add_action('admin_menu', 'cdm_premium_check_community_page');
function cdm_premium_check_community_page_content()
  {
    
    
  }
add_filter('sp_cdm_check_license_flag', 'cdm_multisite');

function cdm_multisite($site = 0)
  {
    if (is_multisite() && !is_main_site(get_current_blog_id()))
      {
        $site = 1;
      }
    
    
    return $site;
  }
function cdm_premium_check_community_page()
  {
    if (!function_exists('sp_cdm_language_init'))
      {
        remove_menu_page('sp-client-document-manager');
        #add_menu_page( 'sp_cu', 'Client Documents',  'manage_options', 'sp-client-document-manager', 'cdm_premium_check_community_page_content');	
      }
  }

function cdm_premium_check_community()
  {
    if (!function_exists('sp_cdm_language_init'))
      {
        
        echo $sp_client_upload;
        
        
        
        
        $plugins = array(
            
            // This is an example of how to include a plugin pre-packaged with a theme.
            array(
                'name' => 'SP Project & Document Manager',
                'slug' => 'sp-client-document-manager',
                'version' => '2.7.3',
                'required' => true // If false, the plugin is only 'recommended' instead of required.
                
                
                
                
                
            )
            
            
            
        );
        
        $theme_text_domain = 'sp-cdm';
        /**
         * Array of configuration settings. Amend each line as needed.
         * If you want the default strings to be available under your own theme domain,
         * leave the strings uncommented.
         * Some of the strings are added into a sprintf, so see the comments at the
         * end of each line for what each argument will be.
         */
        $config            = array(
            'default_path' => '', // Default absolute path to pre-packaged plugins.
            
            'parent_menu_slug' => 'plugins.php',
            'parent_url_slug' => 'plugins.php', // Default parent URL slug
            'menu' => 'cdm-install-plugins', // Menu slug.<br>
            'has_notices' => true, // Show admin notices or not.
            'dismissable' => false, // If false, a user cannot dismiss the nag message.
            'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => true, // Automatically activate plugins after installation or not.
            'message' => '', // Message to output right before the plugins table.
            'strings' => array(
                'page_title' => __('Install Required Plugins', $theme_text_domain),
                'menu_title' => __('Install Plugins', $theme_text_domain),
                'installing' => __('Installing Plugin: %s', $theme_text_domain), // %1$s = plugin name
                'oops' => __('Something went wrong with the plugin API.', $theme_text_domain),
                'notice_can_install_required' => _n_noop('SP Client Document Manager Premium requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.'), // %1$s = plugin name(s)
                'notice_can_install_recommended' => _n_noop('SP Client Document Manager recommends the following plugin: %1$s. But is not required.', 'SP Client Document Manager recommends the following plugins: %1$s. But is not required.'), // %1$s = plugin name(s)
                'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.'), // %1$s = plugin name(s)
                'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.'), // %1$s = plugin name(s)
                'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.'), // %1$s = plugin name(s)
                'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.'), // %1$s = plugin name(s)
                'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with  SP Client Document Manager Premium %1$s', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.'), // %1$s = plugin name(s)
                'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.'), // %1$s = plugin name(s)
                'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins'),
                'activate_link' => _n_noop('Activate installed plugin', 'Activate installed plugins'),
                'return' => __('Return to Required Plugins Installer', $theme_text_domain),
                'plugin_activated' => __('Plugin activated successfully.', $theme_text_domain),
                'complete' => __('All plugins installed and activated successfully. %s', $theme_text_domain), // %1$s = dashboard link
                'nag_type' => 'error' // Determines admin notice type - can only be 'updated' or 'error'
            )
        );
        
        smarty_tgmpa($plugins, $config);
        
        
        
        
        
      }
    
    
    
  }
add_action('tgmpa_register', 'cdm_premium_check_community');
