<?php

$spcdm_premium_ajax = new spdm_premium_ajax;


new CDM_Premium_Uplad_Ajax;

class CDM_Premium_Uplad_Ajax{
	
	function __construct(){
	add_action( 'wp_ajax_cdm_premium_add_file', array($this, 'add_file'));
	add_action( 'wp_ajax_nopriv_cdm_premium_add_file',array($this, 'add_file'));		
	
	add_action( 'wp_ajax_cdm_premium_finish_file', array($this, 'finish_file'));
	add_action( 'wp_ajax_nopriv_cdm_premium_finish_file',array($this, 'finish_file'));	
	
	add_action( 'wp_ajax_cdm_premium_responsive_view', array($this, 'responsive_view'));
	add_action( 'wp_ajax_nopriv_cdm_premium_responsive_view',array($this, 'responsive_view'));	
	
	add_action( 'wp_ajax_cdm_premium_change_status', array($this, 'change_status'));
	add_action( 'wp_ajax_nopriv_cdm_premium_change_status',array($this, 'change_status'));	
	
	add_action( 'wp_ajax_cdm_premium_add_folder', array($this, 'add_folder'));
	add_action( 'wp_ajax_nopriv_cdm_premium_add_folder',array($this, 'add_folder'));
	
	add_action( 'wp_ajax_cdm_premium_view_file', array($this, 'view_file'));
	add_action( 'wp_ajax_nopriv_cdm_premium_view_file',array($this, 'view_file'));	
	
	add_action( 'wp_ajax_cdm_premium_public_file_list', array($this, 'public_file_list'));
	add_action( 'wp_ajax_nopriv_cdm_premium_public_file_list',array($this, 'public_file_list'));
	
	add_action( 'wp_ajax_cdm_premium_add_file_zip', array($this, 'add_file_zip'));
	add_action( 'wp_ajax_nopriv_cdm_premium_add_file_zip',array($this, 'add_file_zip'));
	
	}
	

	function public_file_list(){
		
		global $spcdm_premium_ajax;
		$spcdm_premium_ajax-> public_file_list($_GET, $_REQUEST);		
		die();
		
	}
	
	
	function view_file(){
	global $spcdm_premium_ajax;
	$spcdm_premium_ajax->view_public_view($_GET);		
	die();	
	}
	
	function add_folder(){
		global $wpdb;
		
		
		$current_user = wp_get_current_user();
		$insert['name'] = $_REQUEST['project-name'];
		$insert['parent'] = $_REQUEST['parent'];
		$insert['uid'] = $current_user->ID;
	
	 	foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
		$wpdb->insert("" . $wpdb->prefix . "sp_cu_project", $insert);	
		
		
	exit;	
	}
	function change_status(){
	global $spcdm_premium_ajax;
	$spcdm_premium_ajax->change_status($_POST);		
	die();
	}
	
	
	function responsive_view(){
	
		
		if ( !is_user_logged_in() ) 
		exit;

		global $cdm_premium_file_list;
		
		$cdm_premium_file_list->view();
	
		die();
	}
	function add_file(){
		global $cdmPremiumUploader;
	
 	    $cdmPremiumUploader->upload($_FILES,$_POST['uid']);	
			
		die();	
	}
	
		function add_file_zip(){
		global $cdmPremiumUploader;
	
 	    $cdmPremiumUploader->upload($_FILES,$_POST['uid'],true,true);
			
		die();	
	}
	function finish_file(){
		global $cdmPremiumUploader;
		
 	   $cdmPremiumUploader->finish($_POST,$_POST['uid']);	
		die();	
	}
	
}
