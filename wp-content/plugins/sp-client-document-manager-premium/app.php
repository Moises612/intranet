<?php

add_action('init','sp_client_force_upgrade');

function sp_client_force_upgrade(){
	if(get_option('sp_client_force_upgrade') != 1){
		update_option('sp_client_force_upgrade',1);
		wp_redirect(admin_url( 'admin.php?page=sp-client-document-manager-settings&force_upgrade=1&force_upgrades=1' ));
		exit;
	}
	
}
add_action('init','sp_client_upload_upgrader');
function sp_client_upload_upgrader(){
	global $wpdb;
   global  $sp_client_upload_premium;
  
   $tables = array();
  
  
    $check_cu = $wpdb->get_results("SHOW COLUMNS FROM `".$wpdb->prefix . "sp_cu`", ARRAY_A);

  
  $columns = array();
  foreach($check_cu as $column){
	  
		$columns[] = $column['Field'];
		
		
  }
  
  	if(!in_array('last_modified',$columns)){
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'sp_cu` ADD `last_modified` TIMESTAMP  NOT NULL AFTER `date`;'); 	
	}
	if(!in_array('last_viewed',$columns)){
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'sp_cu` ADD `last_viewed` TIMESTAMP  NOT NULL AFTER `date`;'); 	
	}
	if(!in_array('last_downloaded',$columns)){
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'sp_cu` ADD `last_downloaded` TIMESTAMP  NOT NULL AFTER `date`;'); 	
	}
	
	
   do_action('spcdm/database/table/sp_cu',$columns);
   if(get_option('sp_cu_installed_cat_updates') != 1){
   

	 
	  $tables[] = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix ."sp_cu_cats` (
					 `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(255) NOT NULL,
					  `cat_limit` INT( 11 ) NOT NULL  DEFAULT '0',
					  `cat_limit_type` INT( 11 ) NOT NULL  DEFAULT '0'	,				  
					  PRIMARY KEY (`id`)
						);";
	   
	   
   }

   
   if(count($tables)>0){
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
	foreach($tables as $table){
		dbDelta($table );		
	}
	   
 
   #;
   
  
   
   
  $check_files = $wpdb->get_results("SHOW COLUMNS FROM `".$wpdb->prefix . "sp_cu_cats", ARRAY_A);
  

	$wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu_cats` ADD `cat_limit` INT( 11 ) NOT NULL  DEFAULT "0";'); 
	$wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu_cats` ADD `cat_limit_type` INT( 11 ) NOT NULL  DEFAULT "0";'); 
	$wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu_project` ADD `default_order` INT( 11 ) NOT NULL  DEFAULT "0";'); 	
	
  
   
   update_option('sp_cu_installed_cat_updates', 1);
   }
   
   
}
	
add_action('init','sp_cdm_premium_upgrades_installer');
function sp_cdm_premium_upgrades_installer(){
	
	   global $wpdb;
	   $upgrade_count += 0;
	
  $table_name = "".$wpdb->prefix . "sp_cu_meta";
		 if($wpdb->get_var("show tables like '$table_name'") != $table_name){			
			$sql_meta = "CREATE TABLE IF NOT EXISTS `".$table_name."` (
			  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			  `fid` bigint(20) unsigned NOT NULL DEFAULT '0',
			  `pid` bigint(20) unsigned NOT NULL DEFAULT '0',
			  `uid` bigint(20) unsigned NOT NULL DEFAULT '0',
			  `meta_key` varchar(255) DEFAULT NULL,
			  `meta_value` longtext,
			  PRIMARY KEY (`id`)
			);" ;
			
			$upgrade_count +=1;
			
			  
		 }	
	
	apply_filters('sp_cdm_premium_upgrades',$upgrade_count);
	
	if($_GET['database_upgrade'] == 1 or  $upgrade_count >0){
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta($sql_meta );
	

	
	$updated = 1;
	}
	if($updated != 1){
	if($upgrade_count > 0){

	}
	}
	
}	

function sp_client_upload_install_premium() {
   global $wpdb;
   global  $sp_client_upload_premium;
   #echo $sp_client_upload_premium ;
   $force_uprade = isset($_POST['force_upgrade']) ? $_POST['force_upgrade'] : null;
   if($sp_client_upload_premium != get_option('sp_client_upload_premium') or $force_uprade ==1){
   
   
   
   
   $sql1 = '
CREATE TABLE  `'.$wpdb->prefix.'sp_cu_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `template` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `defaults` text NOT NULL,  
  `sort` int(11) NOT NULL DEFAULT "0",
  `required` varchar(11) NOT NULL DEFAULT "No",
  PRIMARY KEY (`id`)
) ;';

$sql2 = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'sp_cu_form_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `file_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ';
   
   	$sql3 = '
CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'sp_cu_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);';

$sql4 = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'sp_cu_groups_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;';

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql1);
  dbDelta($sql2);
  dbDelta($sql3);
  dbDelta($sql4);

	$wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu` ADD `cid` INT( 11 ) NOT NULL;');
   $wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu` ADD `tags` text NOT NULL;');
   	$wpdb->query("ALTER TABLE `".$wpdb->prefix ."sp_cu_project` ADD `parent` INT( 11 ) NOT NULL DEFAULT '0'");		
	$wpdb->query("ALTER TABLE `".$wpdb->prefix ."sp_cu_groups` ADD `locked` INT( 1 ) NOT NULL DEFAULT '0'");	
   
     
	   $wpdb->query('ALTER TABLE `'.$wpdb->prefix . 'sp_cu` ADD `status` INT( 11 ) NOT NULL  DEFAULT "0";');
	   
   	update_option('sp_client_upload_premium',$sp_client_upload_premium);
   }
}
register_activation_hook(''.dirname(__FILE__).'/index.php','sp_client_upload_install_premium');

function sp_cdm_check_file_deletion(){
	
global $wpdb;
	
	if(	strtotime(get_option("sp_file_check")) < date("Y-m-d")){						 
		 
		  $check_files = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."sp_cu", ARRAY_A);
	
	
	$filecheck_time = get_option('sp_cu_file_delete');								 
	for($i=0; $i<count($check_files); $i++){
		$today = time();
		$date = strtotime($check_files[$i]['date']);
		
		$howlong = ($today - $date);
		$howlong  = $howlong / 60;
		$howlong = $howlong / 60;
		$howlong = $howlong / 24;
	
		if($howlong > $filecheck_time){
		unlink(''.SP_CDM_UPLOADS_DIR.''.$check_files[$i]['uid'].'/'.$check_files[$i]['file'].'');
	    $wpdb->query("DELETE FROM ".$wpdb->prefix."sp_cu WHERE id = ".$check_files[$i]['id']."");
		}
		
	}
	
	update_option('sp_file_check',date("Y-m-d"));
	}
}




if(get_option('sp_cu_file_delete') != ""){
sp_cdm_check_file_deletion();
}






define("CU_PREMIUM", 1);




function sp_client_upload_menu_premium() {

	

			if(class_exists('cdmForms')){
		$forms = new cdmForms;
		$groups = new cdmGroups;
		}
		 add_submenu_page( 'sp-client-document-manager-none', 'Welcome', 'Welcome', 'sp_cdm_settings', 'sp-client-document-manager-welcome',  'sp_cdm_premium_welcome');
		  add_submenu_page( 'sp-client-document-manager', 'Forms', 'Forms', 'sp_cdm_forms', 'sp-client-document-manager-forms',   array(  $forms ,'view'));
		    add_submenu_page( 'sp-client-document-manager', 'Share Spaces', 'Share Spaces', 'sp_cdm_groups', 'sp-client-document-manager-groups',   array(  $groups ,'view'));
		 add_submenu_page( 'sp-client-document-manager', 'Categories', 'Categories', 'sp_cdm_categories', 'sp-client-document-manager-categories', 'sp_client_upload_cat_view');
		 add_submenu_page( 'sp-client-document-manager-disabled', 'Welcome!', 'Welcome!', 'sp_cdm', 'sp-client-document-manager-disabled',  'cdm_gotowelcome');
		 
		 add_submenu_page( 'sp-client-user-files', 'Your Files', 'Your Files', 'cdm_your_files', 'sp-client-document-manager-disabled',  'cdm_userfiles');
		 

}



	
	
	
	function cdm_gotowelcome(){
	
		echo '<script type="text/javascript">
<!--
window.location = "admin.php?page=sp-client-document-manager-welcome"
//-->
</script>';	
}
function check($plugin){
		
				
		if ( ! function_exists( 'get_plugins' ) ) {
	require_once ABSPATH . 'wp-admin/includes/plugin.php';
}


		$all_plugins = get_plugins();
$plugin_exists = 0;
	foreach($all_plugins as $key=>$plugins){
			
		if($plugins['Name'] == $plugin){
		$plugin_exists = 1;
		}
		
	}
	if($plugin_exists == 1){
	return true;	
	}else{
	return false;
	}
		
	}	
function sp_cdm_premium_welcome(){
	
echo  '<div class="wrap about-wrap">
<h1>Welcome to '.EDD_CDM_ITEM_NAME.' '.EDD_CDM_VERSION.'</h1>
<div class="about-text"> Thank you for updating! '.EDD_CDM_ITEM_NAME.' '.EDD_CDM_VERSION.' is our safest, fastest, most flexible version ever. </div>

<h2 class="nav-tab-wrapper"> 
<a class="nav-tab nav-tab-active" href="#"> What&#8217;s New </a>
<a class="nav-tab" href="http://smartypantsplugins.com/purchases/" target="_blank">Get Your License</a>
<a class="nav-tab" href="admin.php?page=sp-client-document-manager-settings">Go to the settings page</a>
<a class="nav-tab" href="admin.php?page=sp-client-document-manager-help">Shortcode Instructions</a>

 </h2>
<div class="changelog">
  <h3>Check Out Our New Available Addons for SP Client Docoument Manager</h3>';
  

	

		$url = wp_remote_get( 'http://www.smartypantsplugins.com/addons.php' );
	
		$addons = json_decode($url['body']);
	
		foreach($addons as $addon){
		
		if(sp_cdm_smarty_features::check($addon->name) == true){
		$installed = 'updated';	
		$purchase = '<div style="color:green;font-size:1.2em;font-weight:bold;">Installed, Thank you for your support!</div>';	
		}else{
		$installed = 'error';
		$purchase = '<div> <a style="color:red;font-size:1.2em;font-weight:bold;" href="'.$addon->url.'" class="button">Not Installed! Click here to purchase</a></div>';		
		}
		echo '
 <div style="border:1px solid #CCC;border-radius:5px;margin:10px;padding:5px;background-color:#EFEFEF"><h2><strong>'.$addon->name.'</strong> </h2>
		<em>Starting at $'.$addon->price.' </em>
		<p>'.$addon->description.'</p>
		<p>'.$purchase .'</p>
		</div>';
  
		}

 
  echo '</div>
  <div class="return-to-dashboard"> <a href="admin.php?page=sp-client-document-manager-settings">Go to the '.EDD_CDM_ITEM_NAME.' Settings page</a> </div>
</div>
';


	
}

