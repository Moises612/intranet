<?php
/*
Template: Public Responsive Mode

*/



global $data,$wpdb;
$file_list = $data;

$pid = $file_list['pid'];

if($data['atts']['download_icon'] == ''){
	$data['atts']['download_icon'] = 'dashicons-download';
}

if($data['atts']['view_icon'] == ''){
	$data['atts']['view_icon'] = 'dashicons-admin-page';
}
?>

		 
	<div class="cdm-public-file-list">
<ul>
<?php
  $r = $file_list['files'];
			 for ($i = 0; $i < count($r); $i++) {	#start file loop	
			if ($data['atts']['date'] == 1) {
                $inc_date = '<em style="font-size:10px"><br>' . date("F Y g:i A", strtotime($r[$i]['date'])) . '</em>';
            } else {
                $inc_date = '';
            }
			$download_link = '<a  href="'.admin_url( 'admin-ajax.php?cdm-download-file-id='.base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']).'' ).'" class="cdm-download-file-link" data-id="'.base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']).'" >';			
			$view_link = '<a  href="javascript:cdmViewFile('.$r[$i]['id'].');" class="cdm-download-file-link" data-id="'.base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']).'" >';			
			$preview_link = '<a href="#" class="doc-preview-item-standalone" data-id="'.$r[$i]['id'].'">';
			
			
			?>
            <li>
            
			    <?php
				$action = 'image_link_action';	
				if($data['atts'][$action] == 'download'  ){ 
				echo $download_links;
				}elseif( $data['atts'][$action] == 'view'  ){ 
				echo $view_link;
				}elseif( $data['atts'][$action] == 'preview'  ){ 
				echo $preview_link;
				}else{
				echo $download_link;	
				}				
				?>
                
                
            <?php do_action('sp_cdm/file_list/image', $r[$i], 32); ?>
            </a>
             <span>
			   <?php
				$action = 'title_link_action';	
				if($data['atts'][$action] == 'download'  ){ 
				echo $download_links;
				}elseif( $data['atts'][$action] == 'view'  ){ 
				echo $view_link;
				}elseif( $data['atts'][$action] == 'preview'  ){ 
				echo $preview_link;
				}else{
				echo $download_link;	
				}				
				?>
			 <?php echo  stripslashes($r[$i]['name']); ?> <?php echo $inc_date ?>
             </a>
             </span><div class="cdm-project-download-link">
            <?php if($data['atts']['disable_download'] != 1){ ?>
            <a  href="<?php echo admin_url( 'admin-ajax.php?cdm-download-file-id='.base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']).'' ); ?>" class="cdm-download-file-link" data-id="<?php echo base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']); ?>" ><span class="dashicons <?php echo $data['atts']['download_icon']; ?>"></span></a>
            <?php } ?>
            <?php if($data['atts']['disable_view'] != 1){ ?>
             <a  href="javascript:cdmViewFile(<?php echo $r[$i]['id']; ?>);" class="cdm-download-file-link" data-id="<?php echo base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']); ?>" ><span class="dashicons <?php echo $data['atts']['view_icon']; ?>"></span></a>
            <?php } ?>
            </div><div style="clear:both"></div> 
            <div style="display:none">
            <div class="remodal" data-remodal-id="upload-preview-<?php echo $r[$i]['id']; ?>"> <a data-remodal-action="close" class="remodal-close"></a>
           <?php echo cdm_get_image($r[$i],500); ?>
            </div>
            
            </div>
           <div style="clear:both"></div> 
            </li>
            
            <?php 
			
			  }
			  ?>
			
			
			

</ul>


	</div>
    
    
    
    