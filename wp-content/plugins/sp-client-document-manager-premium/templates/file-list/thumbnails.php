<?php
/*
Template: Windows GUI

*/

//pid


global $data,$wpdb;
$file_list = $data;

$pid = $file_list['pid'];

?>

		 
	<div id="dlg_cdm_thumbnails">
       
       


		<div class="sp_isotope_filter">
        <?php
	 	 do_action('spcdm/file_list/above');
		 do_action('spdm_file_responsive_top',$pid);	
		 ?>
        
			
			<?php
			 #Start output the back button
			 if($pid != 0){ ?>
       <div class="dlg_cdm_thumbnail_folder">
				<a href="javascript:sp_cdm_load_project(<?php echo $file_list['current_folder']['parent']; ?>)"><img src="<?php echo $file_list['back_image']; ?>">
				<div class="dlg_cdm_thumb_title">
				 <?php echo __("Go Back", "sp-cdm"); ?>
				</div>
				</a>
	 	</div>
          
        
            <?php 
			}#END output the back button  ?>
        
        	
            
            <?php
			#Start output the projects
			 $r_projects = $file_list['projects'];
			 if (count($r_projects) > 0) {
					for ($i = 0; $i < count($r_projects); $i++) {
							if (($r_projects[$i]['project_name'] != "" && in_array( $r_projects[$i]['pid'],$file_list['current_user_projects']) && cdm_is_folder_hidden($r_projects[$i]['pid']) ==false ) or get_option('sp_cu_release_the_kraken') == 1) {
						 
						 if( cdm_contains_viewable($r_projects[$i]['pid']) ==1 or get_option('sp_cu_release_the_kraken') == 1 ){
						 ?>	
                         
                         	<div class="dlg_cdm_thumbnail_folder">

				<a href="javascript:sp_cdm_load_project(<?php echo $r_projects[$i]['pid']; ?>)"><img src="<?php echo $file_list['folder_image']; ?>">

				<div class="dlg_cdm_thumb_title">

				<?php echo  stripslashes($r_projects[$i]['project_name']); ?>

				</div>

				</a>
				
				<?php do_action('spdm_file_thumbs_column_before_folder', $r_projects[$i]['pid']) ?>

				</div>

            
                        <?php	
						}
						}
					}
			 }#END output the projects
			 
			 ?>
             
			  <?php
			#Start output the files
			  $r = $file_list['files'];
			  for ($i = 0; $i < count($r); $i++) {	#start file loop	
			
			
				 $ext   = preg_replace('/^.*\./', '', $r[$i]['file']);
            $r_cat = $wpdb->get_results("SELECT name  FROM " . $wpdb->prefix . "sp_cu_cats   where id = '" . $r[$i]['cid'] . "' ", ARRAY_A);
            if ($r_cat[0]['name'] == '') {
                $cat = stripslashes($r_cat[0]['name']);
            } else {
                $cat = '';
            }
            if ($file_list['search'] != "" && sp_cdm_get_project_name($r[$i]['pid']) != false) {
                $project_name = ' <em>('.sp_cdm_folder_name() .': ' . sp_cdm_get_project_name($r[$i]['pid']) . ')</em> ';
            } else {
                $project_name = '';
            }
           
		   if(get_option('sp_cu_file_direct_access') == 1){
			$file_link = 	'window.open(\''. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'\'); void(0)'; ;
			}else{
			$file_link =  'cdmViewFile(' . $r[$i]['id'] . ')';	
			}
			
			
		
			if((@in_array( $r[$i]['pid'],$file_list['current_user_projects']) && cdm_folder_permissions($r[$i]['pid']) == 1)or   $r[$i]['pid'] == 0 or get_option('sp_cu_release_the_kraken') == 1 ){
		    
		    
			?>
				
                
               <div class="dlg_cdm_thumbnail_folder">

			<div class="dlg_cdm_thumbnail_image">

				<a href="javascript:<?php echo $file_link; ?>" >
                
                <div class="cdm_img_container">
                <?php echo do_action('sp_cdm/file_list/image', $r[$i], 64); ?>
                </div>

				<div class="dlg_cdm_thumb_title">

				<?php echo  apply_filters('spcdm/files/responsive/file_name','' . stripslashes($r[$i]['name']) . ' ' . $project_name . '', $r); ?>

				</div>

				</a>

				
				<?php do_action('spdm_file_thumbs_column_before_file', $r[$i]['id']); ?>
				
				</div>
                </div>
                
                
	
	<?php
	
		}
			
			
			  }#end file loop
			 ?> <div style="clear:both"></div>
             </div>
             <div style="clear:both"></div>
        </div>



    
    