<?php
/*
Template: Responsive Mode

*/

//pid


global $data,$wpdb;
$file_list = $data;

$pid = $file_list['pid'];
#echo'<pre>';
##print_r($file_list);
#echo '</pre>';
?>

		 
	<div id="dlg_cdm_file_list">
        <div class="sp-cdm-r-head">
        <?php do_action('spdm_file_list_column_before_sort'); ?>
        </div>


		<div class="sp_responsive_view_list">
        <?php
	 	 do_action('spcdm/file_list/above');
		 do_action('spdm_file_responsive_top',$pid);	
		 ?>
        
			
			<?php
			 #Start output the back button
			 if($pid != 0){ ?>
            <div class="sp-cdm-r-folder" onclick="sp_cdm_load_project(<?php echo $file_list['current_folder']['parent']; ?>)">
            <?php do_action('spdm_file_list_column_before_folder_back'); ?>
            <a href="javascript:sp_cdm_load_project(<?php echo $file_list['current_folder']['parent']; ?>)"><img src="<?php echo $file_list['back_image']; ?>" width="42"> &laquo; <?php echo __("Go Back", "sp-cdm"); ?></a>
            </div>
            <?php 
			}#END output the back button  ?>
        
        	
            
            <?php
			#Start output the projects
			 $r_projects = $file_list['projects'];
			# print_r($r_projects);
			 if (count($r_projects) > 0) {
					for ($i = 0; $i < count($r_projects); $i++) {
					
						if (($r_projects[$i]['project_name'] != ""   && cdm_is_folder_hidden($r_projects[$i]['pid']) ==false ) or get_option('sp_cu_release_the_kraken') == 1 ) {
						 
						 if( cdm_contains_viewable($r_projects[$i]['pid']) ==1 or get_option('sp_cu_release_the_kraken') == 1 ){
						if(cdm_folder_permissions($r_projects[$i]['pid']) == 1 or get_option('sp_cu_release_the_kraken') == 1 ){
						 ?>	
						<div class="sp-cdm-r-folder"  >
                            <div class="sp-cdm-r-folder-image">
                            <?php do_action('spdm_file_list_column_before_folder', $r_projects[$i]['pid']); ?>
                            <a href="javascript:sp_cdm_load_project(<?php echo $r_projects[$i]['pid']; ?>)"><img src="<?php echo $file_list['folder_image']; ?>" width="42"></a>
                            </div>
                            
                            <div class="sp-cdm-r-folder-title" onclick="sp_cdm_load_project(<?php echo $r_projects[$i]['pid']; ?>)">
                            <strong><?php echo  stripslashes($r_projects[$i]['project_name']); ?></strong>
                            </div>
                            
                        <div style="clear:both"></div>    
                        </div>
                        <?php	
						 }
						 }
						}
					}
			 }#END output the projects
			
			 ?>
             
			  <?php
			#Start output the files
			  $r = $file_list['files'];
			  
			 
			  for ($i = 0; $i < count($r); $i++) {	#start file loop	
			
			
				 $ext   = preg_replace('/^.*\./', '', $r[$i]['file']);
            $r_cat = $wpdb->get_results("SELECT name  FROM " . $wpdb->prefix . "sp_cu_cats   where id = '" . $r[$i]['cid'] . "' ", ARRAY_A);
            if ($r_cat[0]['name'] == '') {
                $cat = stripslashes($r_cat[0]['name']);
            } else {
                $cat = '';
            }
            if ($file_list['search'] != "" && sp_cdm_get_project_name($r[$i]['pid']) != false) {
                $project_name = ' <em>('.sp_cdm_folder_name() .': ' . sp_cdm_get_project_name($r[$i]['pid']) . ')</em> ';
            } else {
                $project_name = '';
            }
           
		   if(get_option('sp_cu_file_direct_access') == 1){
			$file_link = 	'window.open(\''. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'\'); void(0)'; ;
			}else{
			$file_link =  'cdmViewFile(' . $r[$i]['id'] . ')';	
			}
			
			
		
			if((@in_array( $r[$i]['pid'],$file_list['current_user_projects']) && cdm_folder_permissions($r[$i]['pid']) == 1)or   $r[$i]['pid'] == 0 or get_option('sp_cu_release_the_kraken') == 1 ){
		    
				
			echo '<div class="sp-cdm-r-file" >';
			
			
			if( sp_cdm_is_featured_disabled('premium', 'file_image') == false){
			echo '<div class="sp-cdm-r-file-image">';
			
			do_action('spdm_file_list_column_before_file',$r[$i]['id'] );
			$file_link = apply_filters('spcdm/file_list/link', $file_link, $r[$i]);
			echo' <a href="javascript:'.$file_link.'">';
		do_action('sp_cdm/file_list/image', $r[$i], 32);
			echo'</a> 
			</div>';
			}else{
			echo '<div class="sp-cdm-r-file-image">';
			do_action('spdm_file_list_column_before_file',$r[$i]['id'] );			
			echo'</div>';	
			}
			
			echo'<div class="sp-cdm-r-file-file"  >
			<div onclick="'.$file_link.'" class="sp-cdm-r-file-file-inside">
			';
			
			
			do_action('spcdm/files/responsive/before',$r[$i]['id']);
			if( sp_cdm_is_featured_disabled('premium', 'file_list_name') == false){
			echo apply_filters('spcdm/files/responsive/file_name','<strong>' . stripslashes($r[$i]['name']) . ' ' . $project_name . '', $r);
			}
			if( sp_cdm_is_featured_disabled('premium', 'file_meta_info') == false){
				echo '</strong><br>';
				if( sp_cdm_is_featured_disabled('premium', 'file_list_date') == false){
					echo apply_filters('spcdm/files/responsive/file_date','<span class="sp-cdm-r-file-date"> '.__('Modified on','sp-cdm').' ' . date("F jS Y g:i A", strtotime(cdm_get_modified_date($r[$i]['id']))) . '</span> ', $r); 
				}
				if( sp_cdm_is_featured_disabled('premium', 'file_list_file_size') == false){
					if($r[$i]['url'] == ''){					
						
					echo apply_filters('spcdm/files/responsive/file_size','<span class="sp-cdm-r-file-size">'.cdm_file_size(''.SP_CDM_UPLOADS_DIR . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '').'</span> ', $r); 
					}
				}
				if( sp_cdm_is_featured_disabled('premium', 'file_list_category') == false){
					echo apply_filters('spcdm/files/responsive/file_cat','<span class="sp-cdm-r-file-cat">'.cdm_get_category($r[$i]['cid']).'</span> ', $r).''; 
				}
			}
			echo '</div>';
			
			if( sp_cdm_is_featured_disabled('premium', 'file_toolbox') == false){
				
					
					echo '<div class="sp-cdm-r-file-toolbox">';
					do_action('spcdm/files/responsive/file_toolbox',$r[$i]['id']);
					
					if( sp_cdm_is_featured_disabled('premium', 'toolbox_preview') == false && $r[$i]['url'] == ''){
						if(cdm_folder_permissions($r[$i]['pid']) or $file_list['user_id'] == $r[$i]['uid']){
						
						echo apply_filters('spcdm/files/responsive/file_toolbox/preview_link','<a href="#" class="doc-preview-item-standalone" data-id="'.$r[$i]['id'].'">'.__('Preview', 'sp-cdm').'</a>',$r[$i]); 
						}
					}
					
					if( sp_cdm_is_featured_disabled('premium', 'toolbox_edit') == false){
						
						if( sp_cdm_is_featured_disabled('premium', 'edit_file_link') == false){
			
							if(cdm_file_permissions($r[$i]['pid']) or $file_list['user_id'] == $r[$i]['uid']){
								
						if( (get_option('sp_cu_user_delete_folders') == 1 && current_user_can('manage_options')) or (get_option('sp_cu_user_delete_folders') != 1) ){		
								
					echo apply_filters('spcdm/files/responsive/file_toolbox/edit_link','<a href="#" class="cdm-r-toolbox-link" tab-id="#cdm-edit" data-id="'.$r[$i]['id'].'">'.__('Edit', 'sp-cdm').'</a>',$r[$i]); 
						}
							}
						}
					}
					if( sp_cdm_is_featured_disabled('premium', 'toolbox_share') == false){
						if( sp_cdm_is_featured_disabled('premium', 'share_file_tab') == false){
					
					echo apply_filters('spcdm/files/responsive/file_toolbox/share_link','<a href="#" class="cdm-r-toolbox-link" tab-id="#cdm-share-tab" data-id="'.$r[$i]['id'].'">'.__('Share', 'sp-cdm').'</a>',$r[$i]); 
					
						}
					}
					if( sp_cdm_is_featured_disabled('premium', 'toolbox_download') == false){
						if(cdm_folder_permissions($r[$i]['pid']) or $file_list['user_id'] == $r[$i]['uid']){
						$ext = substr(strrchr($r[$i]['file'], '.'), 1);
						$stream_file_types = get_option('sp_cu_stream_file_types');
							if($stream_file_types != ''){
							$file_types = explode(",",$stream_file_types);	
							}
						
						if($r[$i]['url'] != ''){
						echo apply_filters('spcdm/files/responsive/file_toolbox/download_link','<a href="'.$r[$i]['url'].'" target="_blank">'.__('View','sp-cdm').'</a>',$r[$i]);
						}else{
						echo apply_filters('spcdm/files/responsive/file_toolbox/download_link','<a '. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect'),$ext,$file_types).'>'.__('Download','sp-cdm').'</a>',$r[$i]);	
						}
					}
					}
		
					if( sp_cdm_is_featured_disabled('premium', 'toolbox_delete') == false){
						
						if ( cdm_user_can_delete($file_list['user_id']) == true && cdm_delete_permission($r[$i]['pid']) == 1) {
						if( (get_option('sp_cu_user_delete_folders') == 1 && current_user_can('manage_options')) or (get_option('sp_cu_user_delete_folders') != 1) ){	
						echo apply_filters('spcdm/files/responsive/file_toolbox/delete_link','<a href="javascript:sp_cu_confirm_delete(\'' . get_option('sp_cu_delete') . '\',200,\''.$r[$i]['id'].'\');" title="'.__('Delete','sp-cdm').'" class="sp-cdm-delete-file" >'.__('Delete','sp-cdm').'</a>',$r[$i]);
						}
						}
					}
		
					echo '</div>';
			}
			
			echo '</div>
				 
				  
				
<div style="clear:both"></div>
		</div>	

		';
		}
			
			
			  }#end file loop
			 ?>
        </div>



	</div>
    
    
    
    