<?php
global $search;
?>
<div class="sp-cdm-premium-search ">
		<form action="" method="post" class="sp-cdm-search-form">
		<div class="cdm_nav_buttons">
		<input   type="text" name="search" id="search_files"><a href="#" class="sp-cdm-search-button"><i class="fa fa-search" aria-hidden="true"></i> <?php _e("Buscar", "sp-cdm"); ?></a> 
		
        </div>
		<div style="text-align:left" class="cdm_nav_buttons">
		<input type="hidden" name="cdm_enable_asearch" class="cdm_enable_asearch" value="0"> 
       
         <select name="order">
        <option value=""><?php _e('Ordenar por:','sp-cdm'); ?></option>
        <option value="name"><?php _e('Nombre de Archivo','sp-cdm'); ?></option>
        <option value="date"><?php _e('Fecha','sp-cdm'); ?></option>
        </select>
         
        <select name="order_type">
        
        <option value="ASC" selected><?php _e('Ascendente','sp-cdm'); ?></option>
        <option value="DESC"><?php _e('Descendente','sp-cdm'); ?></option>
        </select>
		</div>
		
		<div class="cdm-advanced-search" style="display:none">
	
	
	<div class="cdm-advanced-search-field"><span><input type="checkbox" name="advanced_search[folders]" value="1" checked></span><label><?php _e("Search Folder Names", "sp-cdm"); ?></label></div>
		<div class="cdm-advanced-search-field"><span><input type="checkbox" name="advanced_search[file_files]" value="1" checked></span><label><?php _e("Search File Title", "sp-cdm"); ?></label></div>
		
		
		<div class="cdm-advanced-search-field"><span><input type="checkbox" name="advanced_search[file_name]" value="1" checked></span><label><?php _e("Search File Name", "sp-cdm"); ?></label></div>
		<div class="cdm-advanced-search-field"><span><input type="checkbox" name="advanced_search[file_tags]" value="1" checked></span><label><?php _e("Search File Tags", "sp-cdm"); ?></label></div>
		<?php do_action('sp_cdm/templates/search/fields/checkboxes'); ?>
		<div class="cdm-advanced-search-field-text">
        <select  name="advanced_search[date_type]"><option value="date"><?php _e("Created Date", "sp-cdm"); ?></option>
        <option value="last_modified"><?php _e("Modified Date", "sp-cdm"); ?></option>
        <option value="last_viewed"><?php _e("Viewed Date", "sp-cdm"); ?></option>
        <option value="last_downloaded"><?php _e("Downloaded Date", "sp-cdm"); ?></option>
        </select> 
        <label><?php _e("Start Date", "sp-cdm"); ?><em></em></label><span><input type="text" name="advanced_search[date_start]" id="search_from"></span> <label><?php _e("End Date", "sp-cdm"); ?><em></em></label><span><input type="text" name="advanced_search[date_end]" id="search_to"></span></div>
		
		<div class="cdm-advanced-search-field-text"><label><?php echo sp_cdm_category_name(); ?></label><span><?php echo sp_cdm_categories_dropdown('advanced_search[cid]'); ?></span></div>
		<div class="cdm-advanced-search-field-text"><label><?php _e("File Extensions", "sp-cdm"); ?><em>(<?php _e("Comma Seperated", "sp-cdm"); ?>)</em></label><span><input type="text" name="advanced_search[file_ext]" ></span></div>
		<div style="clear:both"></div>
        <div style="text-align:right;"><span class="cdm_nav_buttons"><a href="#" class="sp-cdm-search-button"><?php _e("Search", "sp-cdm"); ?></a></span></div>
        <div style="clear:both"></div>
		</div></form></div>