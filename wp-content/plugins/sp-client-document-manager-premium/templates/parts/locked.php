<?php
/*
Template: Project View shortcode Locked

*/



global $data,$wpdb;


?>

<div class="cdm-public-file-list">
<p><?php _e('This project requires you to login to view files', 'sp-cdm'); ?></p>
<form action="" class="sp-cdm-project-login">
<input type="hidden" value="<?php _e('Please enter your username and password', 'sp-cdm'); ?>" name="login_error">
<div class="sp-cdm-project-login-message"></div>
<input type="text" name="project_login" placeholder="<?php _e('Username', 'sp-cdm'); ?>"> <input type="password" name="project_password" placeholder="<?php _e('Password', 'sp-cdm'); ?>"> <input type="submit" name="_login" value="<?php _e('Login', 'sp-cdm'); ?>"> 
</form>
</div>