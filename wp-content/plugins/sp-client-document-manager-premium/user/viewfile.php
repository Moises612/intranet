<?php

$sp_cdm_premium_viewfile = new sp_cdm_premium_viewfile;

add_filter('sp_cdm_view_file_tab', array($sp_cdm_premium_viewfile, 'share_menu_tab'),10,2);
add_filter('sp_cdm_view_file_content', array($sp_cdm_premium_viewfile, 'share_menu_content'),10,2);

add_filter('sp_cdm_view_file_tab', array($sp_cdm_premium_viewfile, 'edit_menu_tab'),10,2);
add_filter('sp_cdm_view_file_content', array($sp_cdm_premium_viewfile, 'edit_menu_content'),10,2);


add_action( 'wp_ajax_cdm_ajax_view_file_share_save', array($sp_cdm_premium_viewfile, 'ajax_share_save'));
add_action( 'wp_ajax_nopriv_cdm_ajax_view_file_share_save', array($sp_cdm_premium_viewfile, 'ajax_share_save'));


add_action( 'wp_ajax_cdm_ajax_view_file_share_send_email', array($sp_cdm_premium_viewfile, 'ajax_share_send_email'));
add_action( 'wp_ajax_nopriv_cdm_ajax_view_file_share_send_email', array($sp_cdm_premium_viewfile, 'ajax_share_send_email'));

add_action( 'wp_ajax_cdm_ajax_view_file_edit_save', array($sp_cdm_premium_viewfile, 'ajax_edit_save'));
add_action( 'wp_ajax_nopriv_cdm_ajax_view_file_edit_save', array($sp_cdm_premium_viewfile, 'ajax_edit_save'));


add_action( 'init', array($sp_cdm_premium_viewfile, 'ajax_load_file'));

add_filter('sp_cdm/view_file/after_refresh', array($sp_cdm_premium_viewfile, 'add_file_to_zip_button'),10,2);


add_action( 'wp_ajax_cdm_ajax_view_file_add_file_finish', array($sp_cdm_premium_viewfile, 'cdm_ajax_view_file_add_file_finish'));
add_action( 'wp_ajax_nopriv_cdm_ajax_view_file_add_file_finish', array($sp_cdm_premium_viewfile, 'cdm_ajax_view_file_add_file_finish'));

add_filter('sp_cdm_viewfile', array($sp_cdm_premium_viewfile, 'cdm_ajax_view_file_add_file_to_zip'),10,2);


add_filter('sp_cdm_view_file_first_add_button', array($sp_cdm_premium_viewfile,'sp_cdm_view_file_first_add_button'),10,2);

add_action('sp_download_file_after_query',array($sp_cdm_premium_viewfile,'sp_download_file_after_query'));
add_filter('sp_cdm_file_view_info',array($sp_cdm_premium_viewfile,'sp_cdm_file_view_info'),20,2);
class sp_cdm_premium_viewfile{
	
	function sp_cdm_file_view_info($html,$r){
		
		$html .= '<div class="sp_su_project">
	
	<strong>' . __("Modified on ", "sp-cdm") . ': </strong>' . date("F jS Y g:i A", strtotime(cdm_get_modified_date($r['id']))) . '
	
	</div>';
	$html .= '<div class="sp_su_project">
	
	<strong>' . __("Last downloaded on ", "sp-cdm") . ': </strong>' . cdm_get_download_date($r['id']) . '
	
	</div>';
	

		
	return $html;	
	}
	function sp_download_file_after_query($r){
		cdm_upate_download_date($r[0]['id']);
		
	}
	function sp_cdm_view_file_first_add_button($html,$r){
	global $wpdb;
	
	
	#update viewed and modified dates
	if($r[0]['last_modified'] == '0000-00-00 00:00:00'){
	$update['last_modified'] = $r[0]['date'];		
	}
	$update['last_viewed'] =  current_time( 'mysql' );
	$where['id'] = $r[0]['id'];
	$wpdb->update($wpdb->prefix . "sp_cu",$update,$where);
	
	return $html;	
		
	}
	
	function cdm_ajax_view_file_add_file_finish(){

		global $wpdb,$current_user;
		$fid = $_REQUEST['fid'];
		$file_id = $fid;
		$uid = $_REQUEST['uid'];
		$notes = $_REQUEST['notes'];
	   $tmp_dir = '' . SP_CDM_UPLOADS_DIR . '_temp/';	
	
	
	
	 $r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d",  sanitize_text_field($fid)), ARRAY_A);	
	 print_r($r);
	 $post = $r[0];
	 $post['email_to_group'] = 	1;
		$is_encrypted = cdm_is_encrypted($r[0]['id']);
		if($is_encrypted == true){
	
			$sp_cdm_aes_init = new sp_cdm_aes_hooker;
			$encrypted = 1;
			
			 $sp_cdm_aes_init->decrypt_file($r[0]['id']);
			
		}
	
	
		$file = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'';	
		$file_folder = str_replace('.zip','',$file);
		 if (!is_dir($file_folder)) {
		mkdir($file_folder, 0777);
		 }
	
		$file_system = WP_Filesystem();
		$destination = wp_upload_dir();
		$destination_path = $destination['path'];
		$unzipfile = unzip_file( 	$file, 	$file_folder);
		
	  $current_options = unserialize(get_option('current_user_upload_' . $uid . ''));
echo 1;
	  print_r($current_options);
	
	  $dir = ''.SP_CDM_UPLOADS_DIR.''.$uid.'';	
	      foreach ($current_options as $key => $value) {
			
                $files     = explode("|", $value);   
			    if(file_exists(''.$tmp_dir.'/' .''.$files[0].'')){
					if(file_exists(''.$file_folder.'/'.$files[0].'')){
					$method = 'Replaced ';	
					}else{
					$method = 'Added ';		
					}
				rename(''.$tmp_dir.''.$files[0].'', ''.$file_folder.'/'.$files[0].'');	
				unlink($tmp_dir.$files[0]);
				$modified_files .='<li>'.$method.''.$files[0].'</li>';
				}
		  }
		
	
		$zip = new Zip(true);
		$zip->addDirectoryContent($file_folder ,'', true);
		
		
		    $zip->finalize();          
            $zip->setZipFile($file);
		
		
		
		
		if($is_encrypted == true){
			
			 $sp_cdm_aes_init->encrypt_file($r[0]['id']);
			
		}
		cdm_delete_dir($file_folder);
		delete_option('current_user_upload_' . $uid . '');
		
		
		
		
		 if (get_option('sp_cu_add_zip_email') != "") {
            $subject = sp_cu_process_email($file_id,get_option('sp_cu_add_zip_subject'));
           
		    $message =  sp_cu_process_email($file_id, get_option('sp_cu_add_zip_email')); 
            $message = str_replace('[file_modifications]', '<ul>'.$modified_files.'</ul>',$message);
			$message = str_replace('[files_modification_notes]', $notes,$message);
			
			
		    $to      = $current_user->user_email;
            if (get_option('sp_cu_add_zip_cc') != "") {
                $cc_user = explode(",", get_option('sp_cu_add_zip_cc'));
                foreach ($cc_user as $key => $user_email) {
                    if ($user_email != "") {
                        $pos = strpos($user_email, '@');
                        if ($pos === false) {
                            $role_user_emails = sp_cdm_find_users_by_role($user_email);
                            foreach ($role_user_emails as $keys => $role_user_email) {
                                $user_headers[] = 'Cc: ' . $role_user_email . '';
                            }
                        } else {
                            $user_headers[] = 'Cc: ' . $user_email . '';
                        }
                    }
                }
            }
		
			$message = apply_filters('spcdm_user_email_message',$message,$post, $uid);
				
			$to = apply_filters('spcdm_user_email_to',$to,$post, $uid);
			$subject = apply_filters('spcdm_user_email_subject',$subject,$post, $uid);
			$attachments = apply_filters('spcdm_user_email_attachments',$attachments,$post, $uid);
			do_action('spcdm_user_email_headers',$post, $uid,$to, $subject, $message, $user_headers, $attachments);
           
		   add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		    wp_mail($to, stripslashes($subject),stripslashes( $message), apply_filters('sp_cdm/mail/headers',$user_headers,$current_user,$to,$subject,$message), $attachments);
		 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		  do_action('sp_cdm_email_send','sp_cu_add_zip_email',$file_id,$post, $uid,$to, $subject, $message, $headers, $attachments);
		 
        }
	
		
		 do_action('spcdm_after_emails',$post, $uid,$to, $subject, $message, $user_headers, $attachments);
		cdm_event_log($file_id,$current_user->ID,'file',''.__('Added files to archive <em>'.$notes.'</em>','sp-cdm').'');
	die();	
	}


	function cdm_ajax_view_file_add_file_to_zip($html,$r){
					global $current_user;
		
		$ext        = preg_replace('/^.*\./', '', $r[0]['file']);
	
		if($ext == 'zip'){
		
		$uid = $current_user->ID;
		$fid = $r[0]['id'];
		#set_transient('current_user_upload_add_zip_'.$uid.'',$fid, 12 * HOUR_IN_SECONDS );	
		
		$html .='
		<div class="cdm_pluploader_add_file_dialog" style="display:none">
	
		<h2>Add files to this archive</h2>
		<p>Choose the files you would like to add to the archive below</p>
			<div style="text-align:right;margin-bottom:10px" ><a href="" class="cdm_button cdm_add_zip_to_file">Cancel</a></div>
		
		
		
			<div id="cdm_pluploader_add_file" style="display:none">
  					
					<div id="filelist_zip" class="cdm_upload_area">Your browser doesn\'t have Flash, Silverlight or HTML5 support.</div>
 						<p><textarea name="upload-notes" class="upload-notes" value="" placeholder="Upload Notes" style="width:100%;max-width:500px;height:40px"></textarea>
						<div id="container_zip"> 
					
							<a  class="cdm_button" id="pickfiles_zip">' . __("Choose Files","sp-cdm").'</a>
							<a  class="cdm_button" id="uploadfiles_zip">' . __("Start Upload","sp-cdm").'</a>
						</div>
						 
					
		 </div>
		
		</div>
	
	
		 
		
			
		 
		 ';
		 
		$html .= "<script type=\"text/javascript\">

		// Custom example logic
 
var uploader = new plupload.Uploader({
    runtimes : 'html5,silverlight,browserplus,gears,html4',     
    browse_button : 'pickfiles_zip', // you can pass in id...
    container: document.getElementById('container_zip'), // ... or DOM Element itself
     

         multipart_params : {
        'action' : 'cdm_premium_add_file_zip',
        'uid' : '".$uid."'
    },
	   
	    url : sp_vars.ajax_url,
   ";
   
   if(get_option('sp_cu_limit_file_types') != ''){
		$html .= "filters : [
            {title : '".__("File Types", "sp-cdm")."', extensions : '".get_option('sp_cu_limit_file_types')."'}
        ],";	
		}
		if(get_option('sp_cu_limit_file_size') != '' && is_numeric(get_option('sp_cu_limit_file_size'))){
		$html .= " max_file_size : '".get_option('sp_cu_limit_file_size')."mb',";	
		}
		if(get_option('sp_cu_limit_files') == 1){ 
		$html .= "   multi_selection:false,
					 max_file_count: 1,  ";
		
		}
		$html .= "

        // Rename files by clicking on their titles
        rename: true,
          chunk_size: '2000kb',
    max_retries: 3,
        // Sort files
        sortable: true,
 
        // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
        dragdrop: true,
 
        // Views to activate
        views: {
            list: true,
          
            active: 'list'
        },
 
        // Flash settings
        flash_swf_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.swf')."',
     
        // Silverlight settings
        silverlight_xap_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.xap')."',";
		
     
 $html .="
    // Post init events, bound after the internal events
        init : {
            PostInit: function() {
                // Called after initialization is finished and internal event handlers bound
                console.log('[PostInit]');
				
			document.getElementById('filelist_zip').innerHTML = '';
 
            document.getElementById('uploadfiles_zip').onclick = function() {
                uploader.start();
                return false;
            };
				
               
            },
 
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
                document.getElementById('filelist_zip').innerHTML += '<div id=\"' + file.id + '\">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            });
        },
            FileUploaded: function(up, file, info) {
                // Called when file has finished uploading
                console.log( file);
            },
 			
  
           
 
            UploadComplete: function(up, files) {
               
               		 jQuery.ajax({

						   type: 'POST',   

						

						   url:  cdm_scripts.ajax_url ,

						   data:  {'action': 'cdm_ajax_view_file_add_file_finish', 'uid': '".$uid."', 'fid': '".$fid."','notes':jQuery('.upload-notes').val()},

							   success: function(msg){

							   alert('Files Added To Archive!');
							   cdm_refresh_file_view(".$fid.");

						";
						
					
			$html .= "
							

							   }

						 });

            },
   			
            Destroy: function(up) {
                // Called when uploader is destroyed
                console.log('[Destroy] ');
            },
  UploadProgress: function(up, file) {
            document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + \"%</span>\";
        },
            Error: function(up, args) {
                // Called when error occurs
                if(args.code === plupload.INIT_ERROR)
            {
                alert('Please install or activate flash player');
            }
            
			   
			   
			   console.log(args);
            }
    }
});
 jQuery(function() {
uploader.init();
 });




		

	</script>";
		
		}
		return $html;

	}
	
	
	function add_file_to_zip_button($html,$r){
			global $current_user;
			
		$ext        = preg_replace('/^.*\./', '', $r[0]['file']);
	
		if($ext == 'zip'){
		$html .= '<a href="#" class="cdm_add_zip_to_file" data-fid="' . $r[0]['id'] . '" data-uid="' . $current_user->ID . '"><span class="dashicons dashicons-plus cdm-dashicons"></span> '.__('Add Files To Zip','sp-cdm').'</a></a>';
		}
		return $html;
	}
	function ajax_load_file(){
		if($_GET['cdm_linkout'] != ''){
			
			$file_id = base64_decode($_GET['cdm_linkout']);
			$transient = get_transient('cdm_share_link_'.$file_id.'');
			
			if($transient != ''){
			@session_start();
		
			global $wpdb;
				
			$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d", $file_id), ARRAY_A);	
			
			
			if($_POST['password-auth'] != ''){
				
				if($_POST['password-auth'] == get_option('cdm_share_link_'.$file_id.'_password')){
				$_SESSION['cdm-file-'.$file_id.'-auth'] = 	md5(get_option('cdm_share_link_'.$file_id.'_password'));
				}
			}
			
			if(get_option('cdm_share_link_'.$file_id.'_password')!= ''){	
				if($_SESSION['cdm-file-'.$file_id.'-auth'] != md5(get_option('cdm_share_link_'.$file_id.'_password'))){
				
				
				echo '
				<style type="text/css">
				form{padding:30px;margin:0px auto; text-align:center;font-size:1.2em;}
				form input[type="password"]{padding:10px;margin:10px;font-size:1.3em;}
				form input[type="submit"]{padding:10px;margin:10px;font-size:1.3em;}
				</style>
				<form method="post">
						<p><label>Please enter your password to download this file</label></p><input type="password" name="password-auth">	<br><input type="submit" name="save" value="Download File">
						</form>';
					
				exit;
				}
			}
				
		

			$r_rev_check = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where parent= '".$file_id."'  order by date desc", ARRAY_A);
			
			if(count($r_rev_check) > 0 ){			
			unset($r);
			
			$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= '".$r_rev_check[0]['id']."'  order by date desc", ARRAY_A);
			
			}
			
					
					if($r != false){
						
						if($r[0]['url'] != ''){
							
						wp_redirect($r[0]['url']);	
						die();
						}
						
						
					ob_start();
					
					$file = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'';
					$mime = mime_content_type($file);
					smartReadFile($file ,basename($file ),$mime);
					exit;	
					}
				
			}else{
			header('HTTP/1.0 404 Not Found', true, 404);	
			}
		}
	}
	
	function ajax_share_send_email(){
		
		global $wpdb,$current_user;
		$json['error'] = ''.__('Please fill out all fields','sp-cdm').'';
		
		
		if($_POST['email_name'] != '' ||  $_POST['email_email'] != '' ){
		
			
           
             
			
					 if (get_option('sp_cu_public_link_email') != "") {
						 
					 $r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d", $_POST['fid']), ARRAY_A);	
					  $subject = get_option('sp_cu_public_link_email_subject');
					   $subject = str_replace('[file_name]' ,$r[0]['name'],  $subject);
					  $message = wpautop(get_option('sp_cu_public_link_email'));  
					       	  
					  $message = str_replace('[name]' , $_POST['email_name'], $message);
					  $message = str_replace('[email]' , $_POST['email_email'], $message);
					  $message = str_replace('[message]' , $_POST['email_message'], $message);
					  $message = str_replace('[file]' , '<a href="'.$this->share_url($_POST['fid']).'">'.__('Download','sp-cdm').'</a>', $message);
					  $message = str_replace('[file_name]' ,$r[0]['name'], $message);
					  $seconds_left = get_option('_transient_timeout_cdm_share_link_'.$_POST['fid'].'');
					  if(get_option('cdm_share_link_'.$r[0]['id'].'_password') != ''){
						$message .= '<p style="background-color:#ffc0c0;color:red;padding:5px;margin:5px;font-weight:bold">File Password: '.get_option('cdm_share_link_'.$r[0]['id'].'_password').'</p>';  
					  }
			 
						 $seconds =  $seconds_left - time();
					 
					  $dtF = new DateTime("@0");
    				 $dtT = new DateTime("@$seconds");
    				 $time = $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
					   $message = str_replace('[expiration]' ,''.$time.' before download link expires.', $message);
					  
					 add_filter( 'wp_mail_content_type', 'set_html_content_type' );
					 wp_mail( $_POST['email_email'], stripslashes($subject),stripslashes( $message),apply_filters('sp_cdm/mail/headers',$headers,$current_user,$to,$subject,$message), $attachments);
					 remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
				  $json['error'] = '';
		 		 $json['message'] = ''.__('Email Sent!','sp-cdm').'';
				 $json['post'] = $_POST;
					  }	else{
						  
					$json['error'] = ''.__('Administrator error: Please fill out the email content in admin settings','sp-cdm').'';	  
					  }
			  
		}
			  ob_start();
			  echo json_encode($json);
		die();
	}
	function ajax_share_save(){
	global $current_user,$wpdb;	
		
	
		if ( is_user_logged_in() && $_POST['cdm_set_share_length'] != '' ) {
		
			$transient = get_transient('cdm_share_link_'.$_POST['fid'].'');
			
			if($_POST['cdm_set_share_length'] == 'x'){
			delete_transient(  'cdm_share_link_'.$_POST['fid'].'' );
			delete_option('cdm_share_link_'.$_POST['fid'].'_password');
			die();	
			}
				
		
				$share = $_POST['cdm_set_share_length'] * 60;	
				
				if($transient != false){
					 $seconds_left = get_option('_transient_timeout_cdm_share_link_'.$_POST['fid'].'');
					  $seconds =  $seconds_left - time();	
					
					
				$share = $seconds + $share;	
				}
				
				if($_POST['cdm_set_share_length'] == 0){
				delete_transient(  'cdm_share_link_'.$_POST['fid'].'' );
				set_transient( 'cdm_share_link_'.$_POST['fid'].'',$_POST['fid'],0 );	
				
				}else{
					
					if($seconds_left == false){
					delete_transient(  'cdm_share_link_'.$_POST['fid'].'' );
					delete_option('cdm_share_link_'.$_POST['fid'].'_password');	
					}
				set_transient( 'cdm_share_link_'.$_POST['fid'].'',$_POST['fid'],$share );	
				
				}
				
			
	
		}
		
			update_option('cdm_share_link_'.$_POST['fid'].'_password',$_POST['share-password']);	
		
	die();	
	}
	
		
	function share_button($text,$id){
	
		$share_button = '<select id="cdm_share_length" data-id="'.$id.'" style="padding:5px">
		<option value="">'.$text.'</option> 
		<option value="5">'.sprintf(__('%s Minutes','sp-cdm'),5).'</option>
		<option value="10">'.sprintf(__('%s Minutes','sp-cdm'),10).'</option>
		<option value="30">'.sprintf(__('%s Minutes','sp-cdm'),30).'</option>
		<option value="60">'.sprintf(__('%s Hour','sp-cdm'),1).'</option>
		<option value="480">'.sprintf(__('%s Hours','sp-cdm'),8).'</option>
		<option value="1440">'.sprintf(__('%s Hours','sp-cdm'),24).'</option>
		<option value="2880">'.sprintf(__('%s Hours','sp-cdm'),48).'</option>
		<option value="4320">'.sprintf(__('%s Hours','sp-cdm'),72).'</option>
		<option value="10080">'.sprintf(__('%s Week','sp-cdm'),1).'</option>
		<option value="20160">'.sprintf(__('%s Weeks','sp-cdm'),2).'</option>
		<option value="40320">'.sprintf(__('%s Month','sp-cdm'),1).'</option>
		<option value="120960">'.sprintf(__('%s Months','sp-cdm'),3).'</option>
		<option value="241920">'.sprintf(__('%s Months','sp-cdm'),6).'</option>
		<option value="483840">'.sprintf(__('%s Year','sp-cdm'),1).'</option>
		<option value="0">'.__('Never Expires','sp-cdm').'</option>
		<option value="x">'.__('Remove Public Link','sp-cdm').'</option>
		</select>   <input type="text" placeholder="Optional Password"  id="cdm_share_password" value="'.get_option('cdm_share_link_'.$id.'_password').'">  <button class="cdm_set_share_length">Save</button>';
		
		
		return $share_button;
		
		
	}
	function share_menu_tab($html,$r){
		if( sp_cdm_is_featured_disabled('premium', 'share_file_tab') == false  or current_user_can('manage_options')){
		  $html .= '<li><a href="#cdm-share-tab">'.__("Share","sp-cdm").'</a></li>';
		}
		  return $html;
	}
		function edit_menu_tab($html,$r){
			global $current_user;
		if( sp_cdm_is_featured_disabled('premium', 'edit_file_link') == false or current_user_can('manage_options')){
			
				if(cdm_file_permissions($r[0]['pid']) or $current_user->ID == $r[0]['uid']){
		  $html .= '<li><a href="#cdm-edit">'.__("Edit","sp-cdm").'</a></li>';
				}
		}
		  return $html;
	}
	
	
	function ajax_edit_save(){
	global $current_user,$wpdb;	
	$message['message'] = '';
	$message['error'] = '';
	
		if ( $current_user->ID != '') {
		
				$update['last_modified'] = current_time( 'mysql' );
				$update['name'] = $_POST['file-name'];
				$update['pid'] = $_POST['pid'];
				$update['tags'] = $_POST['tags'];
				$update['notes'] = $_POST['dlg-upload-notes'];
				if($_POST['new-file'] != ''){
				$update['file'] = $_POST['new-file'];	
					
				}
				
				
				$update = apply_filters('cdm/view_file/edit_fields_save',$update);
				
				$where['id'] = $_POST['fid'];
				
				do_action('cdm/view_file/ajax/save',$_POST['fid'],$update);
				
				
				$wpdb->update( $wpdb->prefix . "sp_cu", $update,$where);
				$message['message'] = ''.__('File Saved!','sp-cdm').'';
				$message['post'] = $update;
				$message['where'] = $where;
				cdm_event_log( $_POST['fid'],$current_user->ID,'file',''.__('File Information Edited','sp-cdm').'');
		}else{
		$message['error'] = 'Method not found';	
		}
	
	
	echo json_encode($message);
	die();	
	}
	function edit_menu_content($html,$r){
			global $wpdb,$current_user;
			if( sp_cdm_is_featured_disabled('premium', 'edit_file_link') == false or current_user_can('manage_options')){
		 if(cdm_file_permissions($r[0]['pid']) or $current_user->ID == $r[0]['uid'] or current_user_can('manage_options')){
		
		   $html .= '<div id="cdm-edit">
		   <script type="text/javascript">
		   jQuery( document ).ready(function($) {
	
	 		$(".cdm-tags").tagsInput({width:"auto"});
	 
		   });
		   
		   
		   
		   </script>';
		   
		   
		   $html .= "<script type=\"text/javascript\">
		    
var edit_uploader = new plupload.Uploader({
    runtimes : 'html5,flash,silverlight,html4',
           rename: true,
          chunk_size: '2000kb',
		  multi_selection:false,  //disable multi-selection
    browse_button : 'cdm-replace-file', // you can pass in id...
    container: document.getElementById('cdm-edit-file-upload'), // ... or DOM Element itself
     
   url : sp_vars.ajax_url,
     runtimes : 'html5,silverlight,browserplus,gears,html4',
       multipart_params : {
        'action' : 'cdm_premium_add_file',
		'fid' : '".$r[0]['id']."',
		'pid' : '".$r[0]['pid']."',
		'uid' : '".$r[0]['uid']."',
        'upload_only' : '1'
    },
 
   // Flash settings
        flash_swf_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.swf')."',
     
        // Silverlight settings
        silverlight_xap_url : '". plugins_url('sp-client-document-manager-premium/js/plupload/js/Moxie.xap')."',
     
 
    init: {
        PostInit: function() {
          
        },
 
        FilesAdded: function(up, files) {
			  edit_uploader.start();
           
        },
 
        UploadProgress: function(up, file) {
			
			jQuery('.cdm-replace-file-val').html('<div class=\"cdm-edit-file-list\"><span>Uploading File.... ' + file.percent + '%</span></div>');
         
        },
 		
          FileUploaded: function(up, files, object) {
			  
			  console.log(object);
			 jQuery('.cdm-replace-file-val').html('<div class=\"cdm-edit-file-list cdm-success-message\"><input type=\"hidden\" name=\"new-file\"  value=\"'+ object.response + '\">Uploaded ' + object.response +'! Save Changes to update this file.<a href=\"#\" class=\"cdm-edit-file-delete-new-upload\"><span class=\"dashicons dashicons-no cdm-dashicons\"></span> Delete</a></div>'); 
			  
		  },
		  
		
		
        Error: function(up, err) {
			jQuery('.cdm-replace-file-val').html('<div class=\"cdm-edit-file-list\"><span>' +  err.message + '%</span></div>');
           
        }
    }
});

edit_uploader.init();
		   
		   </script>";
		   
		  $html .='<form id="document_edit_form" data-id="'.$r[0]['id'].'" class="cdm_form">
		   <input type="hidden" name="fid" value="'.$r[0]['id'].'">
		   <input type="hidden" name="action" value="cdm_ajax_view_file_edit_save">
		   ';
		   
		   $html .='<p><strong>'.__('File Display Name','sp-cdm').'</strong><br> <input style="width:100%"  placeholder="' . __("File Name", "sp-cdm") . '"  type="text" name="file-name"  value="'.stripslashes($r[0]['name']).'"></p>';
		   $html .='<p id="cdm-edit-file-upload"><a class="cdm_button" href="#" class="cdm-replace-file" id="cdm-replace-file" data-fid="'.$r[0]['id'].'">'.__('Upload New File','sp-cdm').'</a>
		     <div class="cdm-replace-file-val"></div></p>';
		   
		   
		   
		   $html .='<p><strong>'.__("Project","sp-cdm").'</strong><br> '.cdm_premium_projects_dropdown('pid',$r[0]['pid']).'</p>';
		   
		   
		        if (get_option('sp_cu_user_disable_tags') != 1) {
                $html .= '

 <p >
<strong>'.__('Search tags, press enter after each tag','sp-cdm').'</strong><br>
   <textarea  style="width:100%;height:70px" id="tags" name="tags" class="cdm-tags" >'.stripslashes($r[0]['tags']).'</textarea>

  </p>';
         
		 }
		 
		 
		 
		 
       if (get_option('sp_cu_user_disable_notes') != 1) {
            $html .= '<p >

   <strong>' . __("Notes", "sp-cdm") . '</strong><br>
	<textarea  style="width:100%;height:70px" name="dlg-upload-notes">'.stripslashes($r[0]['notes']).'</textarea>

  </p>

  ';
  

	   }
	   
	   		$html = apply_filters('cdm/view_file/edit_fields', $html,$r);
		   
		   $html .='<p style="text-align:right"><a  class="cdm_button cdm_edit_file_button">' . __("Save File","sp-cdm").'</a></p></form></div>';
			}
			}
			return $html;
			
	}
	function share_url($fid){
			
				
		return ''.get_site_url().'/?cdm_linkout='.base64_encode($fid).'';		
			
}

	function share_menu_content($html,$r){
		global $wpdb;
		
		 if( sp_cdm_is_featured_disabled('premium', 'share_file_tab') == false){
		   $html .= '<div id="cdm-share-tab">';
					if($r[0]['public_shortcode'] == 1){
					$link = sp_cdm_public_file_link($r[0]['id']);	
						
					}else{
					$link = sp_cdm_file_link($r[0]['id']);	
					}
		
		
		
		$html .='<h4 style="margin:20px 0px 20px 0px">'.__('Share File Link','sp-cdm').'</h4>
		

 <p><em>'.__('Creating a share link will allow you to email and send this link to outside sources. Once you share the file you will be given the public link and options to email the file.','sp-cdm').'</em></p>
';
		
		
	
		
		if(get_transient('cdm_share_link_'.$r[0]['id'].'') != false){
			
			 $seconds_left = get_option('_transient_timeout_cdm_share_link_'.$r[0]['id'].'');
						 if($seconds_left != false){
						 
						 $seconds =  $seconds_left - time();
						
						 $dtF = new DateTime("@0");
						 $dtT = new DateTime("@$seconds");
						 $time = $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
						 $left = sprintf(__('File is public for another %s','sp-wpfh'), $time);
						 }else{
							 
						$left = 'Never expires';	 
						 }
		$html .= ' <span style="color:green">'.$left.'</span> 
		<div class="sp_su_project"><p>
		'.$this->share_button('Extend Share',$r[0]['id']).'
		</p></div>
		';	
		$html .='<div class="sp_su_project">

<strong>' . __("Outside Share Link ", "sp-cdm") . ': </strong> <input type="text" style="width:98%" value="'.$this->share_url($r[0]['id']).'"> 

</div>';
			
		$html .='<div class="cdm_share_email_form">
			 	 <p><strong>'.__('Email public link','sp-cdm').'</strong></p>
				 <p><em>'.__('Email the file to the user below, the link will only stay active as long as file stays public.','sp-cdm').'</em></p>
				<p><input style="width:98%" class="cdm_share_email_form_name" type="text" placeholder="Recipients Name"></p>
				<p><input style="width:98%" class="cdm_share_email_form_email" type="text" placeholder="Recipients Email"></p>
				<p><textarea style="width:98%" class="cdm_share_email_form_message"   placeholder="Message"></textarea></p>
				<p><button class="cdm_share_email_form_save" data-id="'.$r[0]['id'].'">'.__('Send Email','sp-cdm').'</button></p>';
				
				
		$html .='</div>';
		
		}else{
		$html .= ' <span style="color:red">'.__('Not Shared','sp-cdm').'</span>
		<div class="sp_su_project"><p>
		'.$this->share_button('Share this file',$r[0]['id']).'
		</p></div>';		
		}
		
	
		
		
		if( sp_cdm_is_featured_disabled('premium', 'private_file_link') == false or sp_cdm_is_featured_disabled('premium', 'public_file_link') == false){	
			
		$html .='	<h4 style="margin:30px 0px 10px 0px">'.__('Share Document Manager Link','sp-cdm').'</h4>';	
		}
		if( sp_cdm_is_featured_disabled('premium', 'private_file_link') == false){
		$html .='<div class="sp_su_project">

<strong>' . __("Private View File Link ", "sp-cdm") . ': </strong> <input type="text" style="width:98%" value="'.sp_cdm_file_link($r[0]['id']).'"> 

</div>';
		}

		if( sp_cdm_is_featured_disabled('premium', 'public_file_link') == false){
			$html .='<div class="sp_su_project">
		
		<strong>' . __("Public View File Link ", "sp-cdm") . ': </strong> <input type="text" style="width:98%" value="'.sp_cdm_public_file_link($r[0]['id']).'"> 
		
		</div>';
		}
		$html .='</div>';
		
}

		return $html;	
	}
	
	
}