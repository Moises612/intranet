<?php
$cdm_premium_preview = new cdm_premium_preview;

add_action('init', array($cdm_premium_preview, 'preview'));
#add_action('init', array($cdm_premium_preview, 'cdm_get_pdf'));
add_filter('cdm/viewfile/top_navigation', array($cdm_premium_preview, 'button'),10,2);


add_action( 'wp_ajax_cdm_ajax_pdf_preview', array($cdm_premium_preview, 'preview_pdf_inline'));
add_action( 'wp_ajax_nopriv_cdm_ajax_pdf_preview', array($cdm_premium_preview, 'preview_pdf_inline'));


add_action( 'wp_ajax_cdm_ajax_pdf_preview_file', array($cdm_premium_preview, 'get_pdf'));
add_action( 'wp_ajax_nopriv_cdm_ajax_pdf_preview_file', array($cdm_premium_preview, 'get_pdf'));


add_action( 'wp_ajax_cdm_ajax_gdocs_url_enable', array($cdm_premium_preview, 'ajax_enable_preview'));
add_action( 'wp_ajax_nopriv_cdm_ajax_gdocs_url_enable',  array($cdm_premium_preview, 'ajax_enable_preview'));

add_action( 'wp_ajax_cdm_ajax_gdocs_preview_container', array($cdm_premium_preview, 'ajax_preview_container'));
add_action( 'wp_ajax_nopriv_cdm_ajax_gdocs_preview_container',  array($cdm_premium_preview, 'ajax_preview_container'));
add_action('wp_footer', array($cdm_premium_preview, 'preview_modal'));
add_action('admin_footer', array($cdm_premium_preview, 'preview_modal'));
class cdm_premium_preview{
		
		
		function smartReadFile($location, $filename, $mimeType='application/octet-stream')
{ if(!file_exists($location))
  { header ("HTTP/1.0 404 Not Found");
    return;
  }

#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);


  $size= filesize($location);
  $time=date('r',filemtime($location));
 
  $fm=fopen($location,'r')  or die("Couldn't get handle");
  


  
  
  if(!$fm)
  { header ("HTTP/1.0 505 Internal server error");
    return;
  }
  
  $begin=0;
  $end=$size;
  
  if(isset($_SERVER['HTTP_RANGE']))
  { if(preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches))
    { $begin=intval($matches[0]);
      if(!empty($matches[1]))
        $end=intval($matches[1]);
    }
  }
 

  if($begin>0||$end<$size)
    header('HTTP/1.0 206 Partial Content');
  else
    header('HTTP/1.0 200 OK');  



 header('Content-Type: '.$mimeType); 
  header("Last-Modified: $time");

// check for IE only headers

if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
    header("Cache-control: private");
    header('Pragma: private');
} else {
    header('Pragma: public');
}
  $cur=$begin;
  fseek($fm,$begin,0);


#  while(!feof($fm)&&$cur<$end&&(connection_status()==0))
#  { print fread($fm,min(1024*16,$end-$cur));
#    $cur+=1024*16;
#  }
  
  
  if ( function_exists( 'apache_get_modules' ) && in_array( 'mod_xsendfile', apache_get_modules() ) ) {
			
			header( "X-Sendfile: $location" );
			exit;
		} elseif ( stristr( getenv( 'SERVER_SOFTWARE' ), 'lighttpd' ) ) {
			
			header( "X-Lighttpd-Sendfile: $location" );
			exit;
		} elseif ( stristr( getenv( 'SERVER_SOFTWARE' ), 'nginx' ) || stristr( getenv( 'SERVER_SOFTWARE' ), 'cherokee' ) ) {
			
			$xsendfile_path = trim( preg_replace( '`^' . str_replace( '\\', '/', getcwd() ) . '`', '',$location ), '/' );
			header( "X-Accel-Redirect: /$xsendfile_path" );
			exit;
	}
  
  
  
  
  
	$chunksize = 1024 * 1024;
	

		if ( false === $fm ) {
			return false;
		}

		while ( ! @feof($fm ) ) {
			echo @fread( $fm, $chunksize );

			if ( ob_get_length() ) {
				ob_flush();
				flush();
			}
		}
 
fclose($handle);
  
  
  exit;
#  ob_end_flush();
}

		
		
		function get_pdf(){
			
				if($_REQUEST['fid'] == '' || !is_numeric($_REQUEST['fid'] )){
					header("HTTP/1.0 404 Not Found");die();	
				die();	
				}
				global $wpdb;
				
				
				if ( (is_user_logged_in() && get_option('sp_cu_user_require_login_download') == 1 ) or (get_option('sp_cu_user_require_login_download') == '' or get_option('sp_cu_user_require_login_download') == 0 )){
				
				
				$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= %d",$_REQUEST['fid']), ARRAY_A);
				
				$file_name = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'';
				$mime = mime_content_type($file_name);
				$this->smartReadFile($file_name,basename($file_name),$mime);
				
				}else{
				header("HTTP/1.0 404 Not Found");die();		
				}
		
			exit;				
			
			
		}
		function preview_pdf_inline(){
		
		if($_REQUEST['fid'] == ''){
		die();	
		}
		global $wpdb;
		$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= %d",$_REQUEST['fid']), ARRAY_A);
		
		echo '
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=no">
<style type="text/css">

#two {
	margin: 50px auto;
	width: 100%;
}

</style>
</head>

<body>			
<div id="two" class="pdf-pro-plugin" data-mode="normal" data-pdf-url="' .admin_url('admin-ajax.php?action=cdm_ajax_pdf_preview_file&fid='.$_REQUEST['fid'].'').'"></div>

<link href="'.SP_CDM_PREMIUM_PLUGIN_URL.'js/pdfjs/pdf-viewer.css" rel="stylesheet" type="text/css" />

<script src="'.SP_CDM_PREMIUM_PLUGIN_URL.'js/pdfjs/pdfjs/pdf.js"></script>
<script src="'.SP_CDM_PREMIUM_PLUGIN_URL.'js/pdfjs/pdf-viewer.min.js"></script>
</body>
</html>';

die();
			
			
		}
		
		function preview_modal(){
			
			echo '<div style="display:none">
				<div class="remodal gdocs-preview" data-id="" data-remodal-id="gdocs_preview"> 
					 <a data-remodal-action="close" class="remodal-close"></a>
					<div class="gdocs-preview-container"></div>
				</div>
			<div class="remodal gdocs-preview-standalone" data-id="" data-remodal-id="gdocs_preview_standalone"> 
					 <a data-remodal-action="close" class="remodal-close"></a>
					<div class="gdocs-preview-container-standalone"></div>
				</div>
				</div>';
		}
		function button($html,$r){
			
			
			$local_file = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'';

if(file_exists($local_file) && is_file($local_file))
{	
			
			
		$html  .='<a href="#"  class="doc-preview-item" data-parent="'.$r[0]['id'].'" data-id="'.cdm_find_latest_revision_id($r[0]['id']).'" title="Preview" ><span class="dashicons 
dashicons-welcome-view-site cdm-dashicons"></span> '.__("Preview","sp-cdm").'</a>';
}
			return $html;
		}
		function ajax_enable_preview(){
			global $wpdb,$current_user;
			$message['error'] = '';
			$file_id = $_POST['file_id'];
			$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= '".$wpdb->escape($file_id)."'", ARRAY_A);
				$preview = 1;
				if(cdm_folder_permissions($r[0]['pid']) == 1 or $current_user->ID == $r[0]['uid'] or $preview = 1){
				
				set_transient( 'sp_cdm_gdocs_preview_'.$file_id.'',1, 1 * HOUR_IN_SECONDS );
				$message['file_id'] = base64_encode($r[0]['id'].'|'.$r[0]['date'].'|'.$r[0]['file']);	
				}else{
					
				$message['error'] = 'You do not have permission to preview this file.';	
					
				}
			echo json_encode($message);
			die();
		}
		function ajax_preview_container(){
			global $wpdb,$current_user;
			$message['error'] = '';
			$file_id = $_POST['file_id'];
			if(get_transient( 'sp_cdm_gdocs_preview_'.$_POST['fid'].'') != false){
			
			$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= '".$wpdb->escape($_POST['fid'])."'", ARRAY_A);
			cdm_event_log($r[0]['id'],$current_user->ID,'file',''.__('File Previewed','sp-cdm').'');
			
			$filetype = wp_check_filetype($r[0]['file']);
			
			if(in_array(strtolower($filetype['ext']), array('jpg','jpeg','gif','png','bmp','png'))){
			echo'<img src="'.site_url( '?gdocs_p='.$file_id.'').'">';	
			
			}elseif(in_array(strtolower($filetype['ext']), array('mp4'))){
			echo '<div style="width:100%" class="wp-video">
		<!--[if lt IE 9]><script>document.createElement(\'video\');</script><![endif]-->
		<video %s controls="controls" class="wp-video-shortcode" preload="metadata" style="width:100%">
		<source type="video/mp4" src="' . SP_CDM_UPLOADS_DIR_URL . '' . $r[0]['uid'] . '/' . $r[0]['file'].'" />
		</video></div>';
				
			}elseif(in_array(strtolower($filetype['ext']), array('pdf'))){
			
	echo '<iframe src="'.admin_url('admin-ajax.php?action=cdm_ajax_pdf_preview&fid='.$r[0]['id'].'').'" width="100%" height="780" style="border: none;"></iframe>';
			
			
			}else{
				if(in_array(strtolower($filetype['ext']), array('xls','xlsx','doc','docx','pub','ppt','pptx'))){
				$url = '//view.officeapps.live.com/op/embed.aspx?src=';	
				}else{
				$url = '//docs.google.com/viewer?url=';		
				}
			
				#echo site_url( '?gdocs_p='.$file_id.'');
				echo '<iframe src="'.$url.''.urlencode(site_url( '?gdocs_p='.$file_id.'')).'&embedded=true" width="100%" height="780" style="border: none;"></iframe>';
			}
			}
		die();	
		}
		
				function ajax_preview_url($file_id){
			global $wpdb,$current_user;
			$message['error'] = '';
			
			if(get_transient( 'sp_cdm_gdocs_preview_'.$file_id.'') != false){
			
			$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= '".$wpdb->escape($file_id)."'", ARRAY_A);
			cdm_event_log($r[0]['id'],$current_user->ID,'file',''.__('File Previewed','sp-cdm').'');
			
			$filetype = wp_check_filetype($r[0]['file']);
			
		
			}else{
			if(in_array($filetype['ext'], array('xls','xlsx','doc','docx','pub','ppt','pptx'))){
			$url = '//view.officeapps.live.com/op/embed.aspx?src=';	
			}else{
			$url = '//docs.google.com/viewer?url=';		
			}
			$url = '//docs.google.com/viewer?url=';
			#echo site_url( '?gdocs_p='.$file_id.'');
			 return ''.$url.''.urlencode(site_url( '?gdocs_p='.$file_id.'')).'&embedded=true';
			}
		
		die();	
		}
		function preview(){
			global $wpdb;	
			
			
		$referrer = 	$_SERVER['HTTP_USER_AGENT'] ;
		
	if($_GET['gdocs_p'] != '' ){
				
	$file_id = sanitize_text_field($_GET['gdocs_p']);

	ini_set('memory_limit', '1024M');
	
	
	
	$file_decrypt = base64_decode($file_id);
	$file_arr = explode("|",$file_decrypt);
	
	
	$fid = $file_arr[0];
		if(get_transient( 'sp_cdm_gdocs_preview_'.$fid.'') != false){
	
	$file_date = $file_arr[1];
	$file_name = $file_arr[2];
	if(!is_numeric($fid)){header("HTTP/1.0 404 Not Found");die();}				
	$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu   where id= '".$wpdb->escape($fid)."' AND date = '".$wpdb->escape($file_date)."'  AND file = '".$wpdb->escape($file_name)."' order by date desc", ARRAY_A);
$local_file = ''.SP_CDM_UPLOADS_DIR.''.$r[0]['uid'].'/'.$r[0]['file'].'';

	$mime = mime_content_type($local_file );				

if(file_exists($local_file) && is_file($local_file))
{	

ob_clean();

ob_start();
$mime = mime_content_type($local_file);
 header('Content-Type: '.$mime); 

   $file = fopen($local_file,"r");
echo 	fread($file,filesize($local_file));
	fclose($file);
	exit;
}else {
    die('Error: The file '.$local_file.' does not exist!');
}
	
	



 		
			}else{
			die('Error: Cannot preview this document');	
			}
			
		}
		}
	
	
	
	
}