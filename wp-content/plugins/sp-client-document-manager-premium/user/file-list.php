<?php
$cdm_premium_file_list = new cdm_premium_file_list;


class cdm_premium_file_list
  {
    
    
    function __construct()
      {
        
        add_action('spcdm/file_list/above', array(
            $this,
            'edit_folder_navigation'
        ));
        add_action('sp_cdm/file_list/image', array(
            $this,
            'file_list_image'
        ), 10, 2);
        
        add_action('sp_cdm/enqueue_scripts', array(
            $this,
            'enqueue_scripts'
        ));
        add_action('sp_cdm/enqueue_scripts', array(
            $this,
            'enqueue_scripts'
        ));
        
        add_filter('sp_cdm/search_form', array(
            $this,
            'search_form'
        ), 1, 10);
        
        add_filter('sp_cdm_search_project_query', array(
            $this,
            'search_folder_query'
        ), 10);
        
        add_filter('sp_cdm_file_search_query', array(
            $this,
            'search_file_query'
        ), 10, 2);
        
      }
    
    
    function search_file_query($query, $pid)
      {
        global $wpdb;
        
        #	echo '<pre>';
        #	print_r($_GET);
        #	echo '</pre>';
        
        
        if (get_option('sp_cu_user_disable_search') != 1)
            if (@$_GET['seach_form']['cdm_enable_asearch'] == 1)
              {
                $aquery          = '';
                $search_term     = '';
                $advanced_search = $_GET['seach_form']['advanced_search'];
                
                
                
                if (trim($_GET['search']) != "")
                  {
                    
                    
                    
                    
                    
                    
                    $term = $_GET['search'];
                    
                    
                    
                    
                    
                    
                    if ($advanced_search['file_name'] == 1)
                      {
                        $search_term .= 'or  name LIKE "%' . $term . '%" ';
                      }
                    if ($advanced_search['file_files'] == 1)
                      {
                        $search_term .= 'or  file LIKE "%' . $term . '%" ';
                      }
                    if ($advanced_search['file_tags'] == 1)
                      {
                        $search_term .= 'or tags LIKE "%' . $term . '%" ';
                      }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    if ($pid != '')
                      {
                        #$and_pid = "(pid = '" .$pid . "')  AND ";	
                      }
                    if ($search_term != '')
                      {
                        $aquery .= " AND (" . $and_pid . " (name = 'notnullfiller' " . $search_term . " ))";
                      }
                  }
                else
                  {
                    
                    
                    if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
                      {
                        #$aquery .= " AND pid = 0 ";
                      }
                    else
                      {
                        $aquery .= " AND pid = '" . $_GET['pid'] . "' ";
                      }
                  }
                
                
                #$r = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_comments   " . $search_file . "  order by " . $sort . "  " , ARRAY_A);
                
                if ($advanced_search['file_ext'] != '' && $advanced_search['file_files'] == 1)
                  {
                    $extensions = array();
                    $extensions = explode(",", $advanced_search['file_ext']);
                    
                    if (count($extensions) > 0)
                      {
                        $aquery .= ' AND ( ';
                        foreach ($extensions as $ext)
                          {
                            
                            $aquery .= ' file LIKE "%' . $ext . '" or';
                          }
                        $aquery .= ' file ="notnullfiller") ';
                      }
                    
                  }
                
                
                if ($advanced_search['date_start'] != '' && $advanced_search['date_end'] != '' && $advanced_search['file_files'] == 1)
                  {
                    $aquery .= ' AND  (' . $advanced_search['date_type'] . ' BETWEEN "' . date("Y-m-d", strtotime($advanced_search['date_start'])) . '" AND  "' . date("Y-m-d", strtotime($advanced_search['date_end'])) . '")';
                  }
                
                if ($advanced_search['cid'] != '' && $advanced_search['file_files'] == 1)
                  {
                    $aquery .= ' AND cid = ' . $advanced_search['cid'] . ' ';
                    
                  }
                
                if ($aquery == '')
                  {
                    return $query;
                  }
                
                return apply_filters('sp_cdm/premium/search/file_query', $aquery, $_GET);
              }
            else
              {
                return $query;
              }
        else
          {
            
            return $query;
          }
        
      }
    function search_folder_query($query)
      {
        global $wpdb;
        
        if (get_option('sp_cu_user_disable_search') != 1)
            if ($_GET['seach_form'])
              {
                parse_str($_GET['seach_form'], $output);
                $_GET['seach_form'] = $output;
                
                
              }
        
        $advanced_search = $_GET['seach_form']['advanced_search'];
        
        if ($_GET['seach_form']['cdm_enable_asearch'] == 1)
          {
            
            
            
            
            if (trim($_GET['search']) != "")
              {
                
                $aquery      = '';
                $search_term = '';
                $term        = $_GET['search'];
                
                if ($advanced_search['folders'] == '')
                  {
                    return ' and pid = 0';
                  }
                
                if ($advanced_search['folders'] == 1)
                  {
                    $search_term = '' . $wpdb->prefix . 'sp_cu_project.name LIKE "%' . $term . '%"';
                  }
                
                
                
                if ($search_term != '')
                  {
                    $aquery .= " AND " . $search_term . " ";
                  }
              }
            else
              {
                
                
                if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
                  {
                    $aquery .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '0' ";
                  }
                else
                  {
                    $aquery .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '" . $_GET['pid'] . "' ";
                  }
              }
            
            if ($aquery == '')
              {
                
                if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
                  {
                    $aquery .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '0' ";
                  }
                else
                  {
                    $aquery .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '" . $_GET['pid'] . "' ";
                  }
              }
            
            
            
            return apply_filters('sp_cdm/premium/search/folder_query', $aquery, $_GET);
            
            
            
          }
        else
          {
            return $query;
          }
        
        
        
        
        
        
        
        
        
        
      }
    function search_form($search)
      {
        if (get_option('sp_cu_user_disable_search') != 1)
          {
            unset($search);
            
            ob_start();
            cdm_get_template('parts/' . apply_filters('sp_cdm/templates/parts/search', 'search') . '.php');
            $search = ob_get_contents();
            ob_end_clean();
            
          }
        
        return $search;
        
        
        
      }
    function enqueue_scripts()
      {
        
        if (get_option('sp_cdm_file_list_template', 'responsive') == 'windows')
          {
            
            wp_enqueue_style('cdm-windows-gui', SP_CDM_PREMIUM_TEMPLATE_URL . 'css/windows.css', array(
                'cdm-style',
                'cdm_reverse_order'
            ));
          }
        
      }
    function file_list_templates()
      {
        
        $templates = array(
            'responsive' => 'Responsive Mode',
            'windows' => 'Windows GUI',
            'thumbnails' => 'Thumbnails',
            'file-list' => 'File List'
        );
        return apply_filters('sp_cdm/file_list/templates', $templates);
        
      }
    function file_list_image($file, $width)
      {
        global $wpdb, $current_user;
        $ext        = preg_replace('/^.*\./', '', $file['file']);
        $images_arr = array(
            "jpg",
            "png",
            "jpeg",
            "gif",
            "bmp"
        );
        if ($width == '')
          {
            $width = '32px';
          }
        if (get_option('sp_cu_user_projects_thumbs_pdf') == 1 && class_exists('imagick'))
          {
            
            $info    = new Imagick();
            $formats = $info->queryFormats();
            
          }
        else
          {
            $formats = array();
          }
        
        
        if (in_array(strtolower($ext), $images_arr))
          {
            if (get_option('sp_cu_overide_upload_path') != '' && get_option('sp_cu_overide_upload_url') == '')
              {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="' . $width . '">';
              }
            else
              {
                $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $file['uid'] . '/' . $file['file'] . '', NULL, 70) . '"  rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $file['uid'] . '/' . $file['file'] . '', 250) . '" width="' . $width . '" class="cdm-hover-thumb">';
                
              }
            
            
            
          }
        elseif (in_array($ext, array(
            'mp4',
            'ogg',
            'webm',
            'avi',
            'mpg',
            'mpeg',
            'mkv'
        )))
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/video.png" width="' . $width . '">';
          }
        elseif ($ext == 'xls' or $ext == 'xlsx')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_excel.png" width="' . $width . '">';
          }
        elseif ($ext == 'doc' or $ext == 'docx')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_word.png" width="' . $width . '">';
          }
        elseif ($ext == 'pub' or $ext == 'pubx')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_publisher.png" width="' . $width . '">';
          }
        elseif ($ext == 'ppt' or $ext == 'pptx')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_powerpoint.png" width="' . $width . '">';
          }
        elseif ($ext == 'adb' or $ext == 'accdb')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_access.png" width="' . $width . '">';
          }
        elseif (in_array(strtoupper($ext), $formats))
          {
            if (file_exists('' . SP_CDM_UPLOADS_DIR . '' . $file['uid'] . '/' . $file['file'] . '_small.png'))
              {
                $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $file['uid'] . '/' . $file['file'] . '_small.png', NULL, 70) . '" width="' . $width . '" rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $file['uid'] . '/' . $file['file'] . '', 250) . '" class="cdm-hover-thumb">';
                
              }
            else
              {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="' . $width . '">';
              }
          }
        elseif ($ext == 'pdf' or $ext == 'xod')
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="' . $width . '">';
          }
        else
          {
            $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="' . $width . '">';
          }
        
        echo apply_filters('sp_cdm_viewfile_image', $img, $file);
        
      }
    function edit_folder_navigation()
      {
        global $wpdb, $current_user;
        $user_permissions = cdm_file_permissions($_GET['pid']);
        if (($_GET['pid'] != "0" && $_GET['pid'] != '') && ((get_option('sp_cu_user_projects') == 1 && get_option('sp_cu_user_projects_modify') != 1) or current_user_can('manage_options') or $user_permissions == 1))
          {
            
            if (get_option('sp_cu_project_ordering_method', 'Name') == 'Name')
              {
                $project_listing_type = 'name';
              }
            else
              {
                $project_listing_type = 'default_order';
              }
            
            $r_project_info = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_project where id = " . $_GET['pid'] . "  ORDER by " . $project_listing_type . "", ARRAY_A);
            
            if (get_option('sp_cu_user_delete_folders') != 1)
              {
                echo '<div  class="sp-cdm-folder-navigation"><div  class="sp-cdm-folder-navigation-buttons">';
                
                if ($user_permissions == 1 && get_option('sp_cu_user_projects_modify') != 1)
                  {
                    echo '<a href="javascript:sp_cu_dialog(\'#edit_category_' . sanitize_text_field($_GET['pid']) . '\',550,130)"><img src="' . SP_CDM_PLUGIN_URL . 'images/application_edit.png"> ' . __("Editar nombre de carpeta", "sp-cdm") . '</a>';
                  }
                
                if (cdm_delete_permission($_GET['pid']) == 1 && get_option('sp_cu_user_projects_modify') != 1)
                  {
                    echo '<a href="#" class="sp-cdm-delete-category" data-id="' . sanitize_text_field($_GET['pid']) . '" style="margin-left:20px"> <img src="' . SP_CDM_PLUGIN_URL . 'images/delete_small.png">  ' . __("Eliminar carpeta", "sp-cdm") . ' </a>';
                  }
                
                /* do_action('cdm/ajax/folder/navigation', $_GET['pid']);  */
                
                echo '<div style="clear:both"></div></div><div style="display:none">	

		

		

		
		<div id="delete_category_' . $_GET['pid'] . '" title="' . __("Eliminar Carpeta?", "sp-cdm") . '">

	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' . __("Est seguro que desea eliminar esta carpeta? Si lo hace se eliminarn todas las carpetas y/o archivos contenidos en ella.", "sp-cdm") . '</p>

		</div>



		

		

				<div id="edit_category_' . $_GET['pid'] . '">			

			

			<input type="hidden"  name="edit_project_id" id="edit_project_id_' . $_GET['pid'] . '" value="' . $_GET['pid'] . '">		

			 ' . __("Nombre de Carpeta", "sp-cdm") . ': <input value="' . stripslashes($r_project_info[0]['name']) . '" id="edit_project_name_' . $_GET['pid'] . '" type="text" name="name"  style="width:200px !important"> 

			<input type="submit" value="' . __("Actualizar", "sp-cdm") . '"  class="sp-cdm-save-category" data-id="' . sanitize_text_field($_GET['pid']) . '" >

			

			</div>

			

		

		
<div style="clear:both"></div>
		</div>

		

		


		

		';
              }
          }
        
      }
    function view()
      {
        
        
        
        global $wpdb, $current_user;
        if (!is_user_logged_in())
            exit;
        global $data;
        $data = array();
        
        
        
        if (get_option('sp_cu_project_ordering_method', 'Name') == 'Name')
          {
            $project_listing_type = 'project_name';
          }
        else
          {
            $project_listing_type = 'default_order';
          }
        
        
        $output = array();
        parse_str($_GET['seach_form'], $output);
        
        
        if ($output['order'] == 'name')
          {
            $project_listing_type = 'project_name';
          }
        
        
        if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
          {
            $data['pid'] = 0;
            
          }
        else
          {
            $data['pid'] = $_GET['pid'];
          }
        $data['uid']     = $_GET['uid'];
        $data['user_id'] = $current_user->ID;
        
        
        $back_image           = '' . SP_CDM_PLUGIN_URL . 'images/my_projects_folder.png';
        $data['back_image']   = apply_filters('spcdm/files/images/back_button', $back_image);
        $folder_image         = '' . SP_CDM_PLUGIN_URL . 'images/my_projects_folder.png';
        $data['folder_image'] = apply_filters('spcdm/files/images/folder_button', $folder_image);
        
        
        
        
        if ($_GET['pid'] != 0)
          {
            
            
            $r_projects = $wpdb->get_results("SELECT 
				  
												" . $wpdb->prefix . "sp_cu_project.id,

												" . $wpdb->prefix . "sp_cu_project.id AS pid,

												" . $wpdb->prefix . "sp_cu_project.uid,

												 " . $wpdb->prefix . "sp_cu_project.name AS project_name,
												 	 " . $wpdb->prefix . "sp_cu_project.default_order,

												  " . $wpdb->prefix . "sp_cu_project.parent
												  

									 FROM " . $wpdb->prefix . "sp_cu_project

									WHERE  parent = '" . $_GET['pid'] . "'
									AND recycle = 0 
									ORDER by " . $project_listing_type . "	
									 ", ARRAY_A);
            
            
            $data['current_user_projects'] = sp_cdm_get_user_sub_projects($_GET['pid'], $_GET['uid']);
            
            
          }
        else
          {
            
            
            
            
            if (function_exists('cdmFindGroups'))
              {
                $find_groups = cdmFindGroups($_GET['uid'], 1);
                
              }
            
            
            
            
            
            
            
            
            
            
            $data['current_user_projects'] = sp_cdm_get_user_projects($_GET['uid']);
            
            if ($data['pid'] != 0)
              {
                
                $data['current_user_projects'][] = $_GET['pid'];
              }
            
            if ($_REQUEST['search'] != "")
              {
                $search_project .= " AND " . $wpdb->prefix . "sp_cu_project.name LIKE '%" . $_REQUEST['search'] . "%' ";
              }
            else
              {
                if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
                  {
                    $search_project .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '0' ";
                  }
                else
                  {
                    $search_project .= " AND " . $wpdb->prefix . "sp_cu_project.parent = '" . $_GET['pid'] . "' ";
                  }
              }
            
            
            
            #temp fix
            $r_projects_groups_addon = apply_filters('sp_cdm_projects_query', $r_projects_groups_addon, $_GET['uid']);
            
            $search_project = apply_filters('sp_cdm_search_project_query', $search_project);
            
            
            
            if ($_GET['pid'] == 0 or $_GET['pid'] == '')
              {
                $user_query = " AND " . $wpdb->prefix . "sp_cu_project.uid = '" . $_GET['uid'] . "' ";
              }
            
            
            $r_projects_query = "SELECT 

												" . $wpdb->prefix . "sp_cu_project.id,

												" . $wpdb->prefix . "sp_cu_project.id AS pid,

												" . $wpdb->prefix . "sp_cu_project.uid,

												 " . $wpdb->prefix . "sp_cu_project.name AS project_name,
												 	 " . $wpdb->prefix . "sp_cu_project.default_order,

												  " . $wpdb->prefix . "sp_cu_project.parent

												 

										FROM " . $wpdb->prefix . "sp_cu_project

									  WHERE (" . $wpdb->prefix . "sp_cu_project.id != '' " . $user_query . " " . $find_groups . " " . $r_projects_groups_addon . ")										

										

										" . $search_project . "

										";
            
            $r_projects_query .= "
									AND recycle = 0 
										ORDER by " . $project_listing_type . "";
            
            
            if (get_option('sp_cu_release_the_kraken') == 1)
              {
                unset($r_projects_query);
                $r_projects_query = "SELECT 										 
																					" . $wpdb->prefix . "sp_cu_project.id,
								
																				" . $wpdb->prefix . "sp_cu_project.id AS pid,
								
																				" . $wpdb->prefix . "sp_cu_project.uid,
								
																				 " . $wpdb->prefix . "sp_cu_project.name AS project_name,
																				 " . $wpdb->prefix . "sp_cu_project.default_order,
								
																				  " . $wpdb->prefix . "sp_cu_project.parent
																		FROM " . $wpdb->prefix . "sp_cu_project
																		WHERE id != ''
																		
																		" . $search_project . " AND recycle = 0  ORDER by " . $project_listing_type . "
								";
              }
            
            
            
            
            
            $r_projects_query = apply_filters('sp_cdm_project_query_final', $r_projects_query);
            
            
            
            $r_projects = $wpdb->get_results($r_projects_query, ARRAY_A);
            
            
            
            if ($_GET['pid'] == 'drafts')
              {
                unset($r_projects);
              }
            
          }
        
        
        if ($_GET['pid'] != 0)
          {
            
            $search_project = apply_filters('sp_cdm_search_project_query', '');
            
            $query_project = $wpdb->get_results("SELECT *

									 FROM " . $wpdb->prefix . "sp_cu_project

									WHERE  id = '" . $_GET['pid'] . "'
									

									 ", ARRAY_A);
            
            
            $data['current_folder'] = apply_filters('sp_cdm/file_list/current_folder', $query_project[0]);
            
          }
        else
          {
            $data['current_folder'] = false;
          }
        
        
        $r_projects = apply_filters('sp_cdm_project_array_filter', $r_projects);
        
        
        $data['projects'] = $r_projects;
        
        
        
        $sort = 'name';
        
        if ($_GET['sort'] == '')
          {
            $sort = spdm_ajax::order_by();
            
          }
        else
          {
            $sort = $_GET['sort'];
          }
        
        if ($output['order'] == 'name')
          {
            $sort = 'name';
          }
        if ($output['order'] == 'date')
          {
            $sort = 'date';
          }
        
        $sort_type = 'ASC';
        if ($output['order_type'] != '')
          {
            $sort_type = $output['order_type'];
          }
        $sort = str_replace("asc", "", $sort);
        $sort = str_replace("desc", "", $sort);
        #print_r($output);
        
        if ($_GET['pid'] == '' or $_GET['pid'] == 'undefined')
          {
            $_GET['pid'] = 0;
          }
        
        
        
        if ($_GET['pid'] == "" or $_GET['pid'] == "0" or $_GET['pid'] == "undefined" or $_GET['pid'] == "null")
          {
            if ($_REQUEST['search'] != "")
              {
                
                $search_file .= " AND (name LIKE '%" . $_REQUEST['search'] . "%' or  tags LIKE '%" . $_REQUEST['search'] . "%')  ";
                $r_projects_groups_addon_search = str_replace("" . $wpdb->prefix . "sp_cu_project.id", "pid", $r_projects_groups_addon);
              }
            else
              {
                
                $search_file .= " AND pid = 0  AND parent = 0  ";
              }
            $search_file = apply_filters("sp_cdm_file_search_query", $search_file, $_GET['pid']);
            
            $sort = str_replace("asc", "", $sort);
            $sort = str_replace("desc", "", $sort);
            
            #echo $query;
            $query = "SELECT *  FROM " . $wpdb->prefix . "sp_cu   where (uid = '" . $_GET['uid'] . "' " . $r_projects_groups_addon_search . ")  	 " . $search_file . " and parent = 0 and recycle =0  order by " . $sort . " " . $sort_type . "";
            
            
            $query = apply_filters('sp_cdm_query_string', $query);
            
            $r = $wpdb->get_results($query, ARRAY_A);
            
          }
        else
          {
            $search_file .= " where (pid = '" . $_GET['pid'] . "')";
            if ($_REQUEST['search'] != "")
              {
                $search_file .= " AND (name LIKE '%" . $_REQUEST['search'] . "%' or  tags LIKE '%" . $_REQUEST['search'] . "%') and recycle =0  ";
              }
            else
              {
                $search_file .= "  AND parent = 0  and recycle =0   ";
              }
            $search_file = apply_filters("sp_cdm_file_search_query", $search_file, $_GET['pid']);
            $query       = "SELECT *  FROM " . $wpdb->prefix . "sp_cu   " . $search_file . "  order by " . $sort . "  " . $sort_type . "";
            
            $query = apply_filters("sp_cdm_file_main_responsive_query", $query, $_GET['pid']);
            
            $r = $wpdb->get_results($query, ARRAY_A);
            
          }
        
        
        if (get_option('sp_cu_release_the_kraken') == 1)
          {
			  
			 
            unset($r);
            unset($search_file);
            if ($_GET['pid'] == '')
              {
                
                $_GET['pid'] = 0;
                
              }
            
            if ($_GET['pid'] == "" or $_GET['pid'] == "0" or $_GET['pid'] == "undefined" or $_GET['pid'] == "null")
              {
				   
              #  $search_file .= " WHERE  pid = 0  ";
			  if($_REQUEST['search'] != ''){
			 $search_file .= " AND (name LIKE '%" . $_REQUEST['search'] . "%' or  tags LIKE '%" . $_REQUEST['search'] . "%')  ";    
			  }else{
			$search_file .=' and pid = 0 ';	  
			  }
				$query = "SELECT *  FROM " . $wpdb->prefix . "sp_cu  WHERE  parent = 0 and recycle =0 	 " . $search_file . "  order by " . $sort . " " . $sort_type . "";
				
      
              }
            else
              {
                $search_file .= " AND (pid = '" . $_GET['pid'] . "') ";
              }
            
            
            
            $r = $wpdb->get_results($query, ARRAY_A);
            
            
          }
     
        if ($_REQUEST['search'] == "" && get_option('sp_cu_release_the_kraken') != 1)
          {
            $r = apply_filters('sp_cdm_file_loop_array', $r, $_GET['pid']);
          }
        if ($_REQUEST['search'] != '')
          {
            $data['search'] = $_REQUEST['search'];
          }
        
        $data['files'] = $r;
        
        
        
        #echo $query;
        
        cdm_get_template('file-list/' . apply_filters('sp_cdm/file_list/template', get_option('sp_cdm_file_list_template', 'responsive')) . '.php');
        
        
        
        
        
      }
    
    
    
  }