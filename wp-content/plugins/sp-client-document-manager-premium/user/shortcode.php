<?php

add_filter('sp_cdm_uploader_above', 'cdm_list_thumbnail_icons',1,20); 
add_filter('sp_cdm_order_by_ajax', 'cdm_default_order_list');
add_filter('sp_cdm_view_file_notes', 'cdm_change_status_chooser',8,2);
add_action('sp_cdm_settings_email', 'cdm_change_status_chooser_email',8,2);


	if(get_option('sp_cu_free_file_list') != 1){ 
add_filter('sp_cdm_upload_view', 'cdm_responsive_view_mode',10,2);
add_filter('sp_cdm_above_nav_buttons', 'cdm_responsive_button');
	}
	
	
	
add_action('cdm/ajax/folder/navigation', 'cdm_copy_link_dir');
add_action('cdm/ajax/folder/navigation', 'cdm_view_folder_log');

add_filter('spcdm/files/images/back_button', 'cdm_file_list_replace_folder_back_image');
add_filter('spcdm/files/images/folder_button', 'cdm_file_list_replace_folder_image');


add_filter('cdm_file_permissions' , 'sp_release_kraken_permissions',1,3);
add_filter('cdm_folder_permissions' , 'sp_release_kraken_permissions',1,3);
add_filter('cdm_delete_permissions' , 'sp_release_kraken_permissions',1,3);

add_filter('sp_cu_below_search_nav','sp_latest_files_content');



add_action( 'wp_ajax_sp_latest_files_button','sp_latest_files_button');
add_action( 'wp_ajax_nopriv_sp_latest_files_button', 'sp_latest_files_button');


add_action( 'wp_ajax_sp_top_downloaded_files_button','sp_top_downloaded_files_button');
add_action( 'wp_ajax_nopriv_sp_top_downloaded_files_button', 'sp_top_downloaded_files_button');



add_action( 'wp_ajax_cdm_ajax_cdm_login','cdm_ajax_cdm_login');
add_action( 'wp_ajax_nopriv_cdm_ajax_cdm_login', 'cdm_ajax_cdm_login');


add_action( 'wp_ajax_sp_view_folder_log','sp_view_folder_log');
add_action( 'wp_ajax_nopriv_sp_view_folder_log', 'sp_view_folder_log');


add_filter('cdm/shortcodes/cdm_project', 'sp_cdm_project_premium',10,2);




function cdm_ajax_cdm_login(){
	$message = array();
	parse_str($_POST['post'],$post);
	$message['post'] =$post;
	$message['error'] = 1;
	
	
	   $creds = array(
        'user_login'    =>$post['project_login'],
        'user_password' => $post['project_password'],
        'remember'      => true
    );
 
    $user = wp_signon( $creds, false );
 
    if ( is_wp_error( $user ) ) {
		
		$message['message'] =  $user->get_error_message();
        
    }else{
		$message['error'] = 0;
		$message['message'] = __('Logged in!', 'sp-cdm');
	}
	
	
	if($post['project_login'] == ''){
	$message['message'] = $post['login_error'];	
	}
	echo json_encode($message);
	die();
};

function sp_cdm_project_premium($content,$atts){
	    global $wpdb, $current_user, $user_ID,$data;
    $pid       = $atts['project'];
    $date      = $atts['date'];
    $order     = $atts['order'];
    $direction = $atts['direction'];
    $limit     = $atts['limit'];
	$permissions = $atts['permissions'];
	if($permissions == 1){
	$permissions = 1;	
	}else{
	$permissions = 0;	
	}
	
	
	if($permissions == 1 && $current_user->ID == ''){
		
				ob_start();
				cdm_get_template('parts/'. apply_filters('sp_cdm/templates/parts/locked','locked') .'.php' );
				$content  = ob_get_contents();
				ob_end_clean();	
				return $content;	
		
	}
	
	if($permissions == 1){
		
	
	 if(  cdm_folder_permissions($pid) == 0){
		 
		 $content = '<div class="cdm-public-file-list"><p style="color:red;background-color:#FFF;margin:5px;padding:5px;"><strong>'.__('Error: you do not have access to this folder', 'sp-cdm').'</strong></p></div>';
		 return $content;
		 
	 }
	}
	if ($order == "") {
        $order = 'name';
    } else {
        $order = $order;
    }
    if ($limit != "") {
        $limit = ' LIMIT ' . $limit . '';
    } else {
        $limit = '';
    }
	 if ($pid == '') {
        $content .= '<p style="color:red">'.__('No project selected', 'sp-cdm').'</p>';
	 }else{
	 $r = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where pid = '" . $pid . "'  order by " . $order . " " . $direction . " " . $limit . "", ARRAY_A);
		$data['atts'] = $atts;
		$data['files'] = $r;
		
		
		
	ob_start();
	cdm_get_template('public/'. apply_filters('sp_cdm/public/template','responsive') .'.php' );
	$content  = ob_get_contents();
	ob_end_clean();
	
	 }
	return $content;
	
	
}

function sp_latest_files_content($html){
	
	global $wpdb,$current_user;
	
	if($_GET['page'] == 'sp-client-document-manager-fileview'){
		
		$uid = $_GET['id'];
	}else{
		$uid = $current_user->ID;	
	}
	
			if( sp_cdm_is_featured_disabled('premium', 'file_list_newest_files') == false){
			$html .= '<a href="#" class="show-recently-uploaded-files" data-uid="'.$uid.'"><i class="fa fa-star" aria-hidden="true"></i> '.__('Recently Uploaded Files', 'sp-cdm').'</a>';
			}
	if( sp_cdm_is_featured_disabled('premium', 'file_list_top_downloads') == false){
			$html .= '<a href="#" class="show-most-popular-files" data-uid="'.$uid.'"><i class="fa fa-star" aria-hidden="true"></i> '.__('Most Popular Files', 'sp-cdm').'</a>';
			}
	
	return $html;
	
}


function sp_top_downloaded_files_button(){
	
	
	global $wpdb;
			
			$uid = $_POST['uid'];
			echo '<div class="sp-cdm-latest-file-view"><div class="sp-cdm-latest-file-view-title">
			<span style="float:left"><strong>'.__('Most Popular Files', 'sp-cdm').'</strong></span>
			<span style="float:right"><a href="javascript:cdm_ajax_search();" class="sp-cdm-close-latest-files"><i class="fa fa-times-circle" aria-hidden="true"> '.__('Close', 'sp-cdm').'</i></a></span>			
			<div style="clear:both"></div>
			</div>
			
			
			
			</div>';
			$r_projects_groups_addon = apply_filters('sp_cdm_projects_query', $r_projects_groups_addon ,$uid);		
			$r_projects_groups_addon_search = str_replace("wp_sp_cu_project.id", "pid",$r_projects_groups_addon);
           	$query = "SELECT *  FROM " . $wpdb->prefix . "sp_cu   where (uid = '" . $uid . "' ".$r_projects_groups_addon_search.") and recycle = 0;";
		   	$current_user_projects = sp_cdm_get_user_projects($uid);	
			#echo $query;
		    
			
			$r = $wpdb->get_results($query, ARRAY_A);
			if(count($r) == 0){
				
			echo '<p>'.__("No files uploaded yet!","sp-cdm").'</p>';
			die();	
			}
		#print_r($r);
			 for ($i = 0; $i < count($r); $i++) {
			
			$file_count[$r[$i]['id']] = cdm_get_total_downloads($r[$i]['id']);
				 
			 }
			 
			 if(count($file_count) == 0){
				
			echo '<p>'.__('No files downloaded yet', 'sp-cdm').'!</p>';
			die();	
			}
			 
		asort($file_count, SORT_NUMERIC);
	$preserved = array_reverse($file_count, true);
			

			unset($r);
		
			$i=0;
			foreach($preserved as $key=>$value ){
			
			
			if($i < get_option('sp_cdm_latest_uploads_limit',20)){
			$files[] = 	$key	;	
			$i++;
			}
			
			
			
			}
		
			foreach($files as $file){
			$i = 0;
			$r =cdm_get_file($file);
			
			
			 $ext        = preg_replace('/^.*\./', '',$r[$i]['file']);
            $images_arr = array(
                "jpg",
                "png",
                "jpeg",
                "gif",
                "bmp"
            );
			
					if(get_option('sp_cu_user_projects_thumbs_pdf') == 1 && class_exists('imagick')){
	
			$info = new Imagick();
			$formats = $info->queryFormats();
			
			}else{
				$formats = array();
			}
	  
			
            if (in_array(strtolower($ext), $images_arr)) {
                if (get_option('sp_cu_overide_upload_path') != '' && get_option('sp_cu_overide_upload_url') == '') {
                    $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="32">';
                } else {
                    $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '',NULL, 70) . '"  rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '', 250) . '" width="32" class="cdm-hover-thumb">';
				
                }
            
			 } elseif (in_array( $ext , array('mp4','ogg','webm','avi','mpg','mpeg','mkv'))) {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/video.png" width="32">';
			} elseif ($ext == 'xls' or $ext == 'xlsx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_excel.png" width="32">';
            } elseif ($ext == 'doc' or $ext == 'docx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_word.png" width="32">';
            } elseif ($ext == 'pub' or $ext == 'pubx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_publisher.png" width="32">';
            } elseif ($ext == 'ppt' or $ext == 'pptx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_powerpoint.png" width="32">';
            } elseif ($ext == 'adb' or $ext == 'accdb') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_access.png" width="32">';
            } elseif (in_array(strtoupper($ext),$formats)) {
                if (file_exists('' . SP_CDM_UPLOADS_DIR . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '_small.png')) {
                    $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '_small.png',NULL, 70).'" width="32" rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '', 250) . '" class="cdm-hover-thumb">';
				
                } else {
                    $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="32">';
                }
            } elseif ($ext == 'pdf' or $ext == 'xod') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="32">';
            } else {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="32">';
            }
          
		  	$img = apply_filters('sp_cdm_viewfile_image', $img,$r[$i]);
		    $ext   = preg_replace('/^.*\./', '', $r[$i]['file']);
            $r_cat = $wpdb->get_results("SELECT name  FROM " . $wpdb->prefix . "sp_cu_cats   where id = '" . $r[$i]['cid'] . "' ", ARRAY_A);
            if ($r_cat[0]['name'] == '') {
                $cat = stripslashes($r_cat[0]['name']);
            } else {
                $cat = '';
            }
            if ($_REQUEST['search'] != "" && sp_cdm_get_project_name($r[$i]['pid']) != false) {
                $project_name = ' <em>('.sp_cdm_folder_name() .': ' . sp_cdm_get_project_name($r[$i]['pid']) . ')</em> ';
            } else {
                $project_name = '';
            }
           
		   if(get_option('sp_cu_file_direct_access') == 1){
			$file_link = 	'window.open(\''. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'\'); void(0)'; ;
			}else{
			$file_link =  'cdmViewFile(' . $r[$i]['id'] . ')';	
			}
			
			
			
			
		
			if( in_array( $r[$i]['pid'],$current_user_projects) or   $r[$i]['pid'] == 0 or get_option('sp_cu_release_the_kraken') == 1 ){
		    	
			echo '<div class="sp-cdm-r-file" >
			
			<div class="sp-cdm-r-file-image">
			';
			do_action('spdm_file_list_column_before_file',$r[$i]['id'] );
			$file_link = apply_filters('spcdm/file_list/link', $file_link, $r[$i]);
			echo' <a href="javascript:'.$file_link.'">'.$img.'</a> </div>
			<div class="sp-cdm-r-file-file" onclick="'.$file_link.'" >
			';
			
			
			
			echo apply_filters('spcdm/files/responsive/file_name','<strong>' . stripslashes($r[$i]['name']) . ' ' . $project_name . ' </strong><br>', $r);
			if( sp_cdm_is_featured_disabled('premium', 'file_list_date') == false){
				echo apply_filters('spcdm/files/responsive/file_date','<span class="sp-cdm-r-file-date"> Modified on ' . date("F jS Y g:i A", strtotime(cdm_get_modified_date($r[$i]['id']))) . '</span> ', $r); 
			}
			
			echo '<span class="sp-cdm-r-file-cat">Downloaded '.$preserved[$r[$i]['id']].' times.</span>';
			echo '</div>
				 
				  
				
<div style="clear:both"></div>
		</div>	

		';
		}
        }
	echo '</div>';
	die();
}
function sp_latest_files_button(){
	
	
	global $wpdb;
			
			$uid = $_POST['uid'];
			echo '<div class="sp-cdm-latest-file-view"><div class="sp-cdm-latest-file-view-title">
			<span style="float:left"><strong>'.__("Recent Uploaded Files","sp-cdm").'</strong></span>
			<span style="float:right"><a href="javascript:cdm_ajax_search();" class="sp-cdm-close-latest-files"><i class="fa fa-times-circle" aria-hidden="true"> '.__("Close","sp-cdm").'</i></a></span>			
			<div style="clear:both"></div>
			</div>
			
			
			
			</div>';
			$r_projects_groups_addon = apply_filters('sp_cdm_projects_query', $r_projects_groups_addon ,$uid);	
				
			$r_projects_groups_addon_search = str_replace("" . $wpdb->prefix . "sp_cu_project.id", "pid",$r_projects_groups_addon);
			$r_projects_groups_addon_search = str_replace('OR pid = "0"', "",	$r_projects_groups_addon_search);
			$r_projects_groups_addon_search = str_replace('add', "-1",	$r_projects_groups_addon_search); 
			 
			# $groups_array =   sp_cdm_groups_addon_projects ::findProjectsArray($uid);
				#print_r($groups_array);
           	$query = "SELECT *  FROM " . $wpdb->prefix . "sp_cu   where (uid = '" . $uid . "' ".$r_projects_groups_addon_search.") and recycle = 0 order by date desc limit ".get_option('sp_cdm_latest_uploads_limit',20)."";
		#echo $query;
		   
		   $current_user_projects = sp_cdm_get_user_projects($uid);	

		    $r = $wpdb->get_results($query, ARRAY_A);
		
		
			  for ($i = 0; $i < count($r); $i++) {
			
			 $ext        = preg_replace('/^.*\./', '', $r[$i]['file']);
            $images_arr = array(
                "jpg",
                "png",
                "jpeg",
                "gif",
                "bmp"
            );
			
					if(get_option('sp_cu_user_projects_thumbs_pdf') == 1 && class_exists('imagick')){
	
			$info = new Imagick();
			$formats = $info->queryFormats();
			
			}else{
				$formats = array();
			}
	  
			
            if (in_array(strtolower($ext), $images_arr)) {
                if (get_option('sp_cu_overide_upload_path') != '' && get_option('sp_cu_overide_upload_url') == '') {
                    $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="32">';
                } else {
                    $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '',NULL, 70) . '"  rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '', 250) . '" width="32" class="cdm-hover-thumb">';
				
                }
            
			 } elseif (in_array( $ext , array('mp4','ogg','webm','avi','mpg','mpeg','mkv'))) {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/video.png" width="32">';
			} elseif ($ext == 'xls' or $ext == 'xlsx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_excel.png" width="32">';
            } elseif ($ext == 'doc' or $ext == 'docx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_word.png" width="32">';
            } elseif ($ext == 'pub' or $ext == 'pubx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_publisher.png" width="32">';
            } elseif ($ext == 'ppt' or $ext == 'pptx') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_powerpoint.png" width="32">';
            } elseif ($ext == 'adb' or $ext == 'accdb') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/microsoft_office_access.png" width="32">';
            } elseif (in_array(strtoupper($ext),$formats)) {
                if (file_exists('' . SP_CDM_UPLOADS_DIR . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '_small.png')) {
                    $img = '<img src="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '_small.png',NULL, 70).'" width="32" rel="' . sp_cdm_thumbnail('' . SP_CDM_UPLOADS_DIR_URL . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '', 250) . '" class="cdm-hover-thumb">';
				
                } else {
                    $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="32">';
                }
            } elseif ($ext == 'pdf' or $ext == 'xod') {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/adobe.png" width="32">';
            } else {
                $img = '<img src="' . SP_CDM_PLUGIN_URL . 'images/package_labled.png" width="32">';
            }
          
		  	$img = apply_filters('sp_cdm_viewfile_image', $img,$r[$i]);
		    $ext   = preg_replace('/^.*\./', '', $r[$i]['file']);
            $r_cat = $wpdb->get_results("SELECT name  FROM " . $wpdb->prefix . "sp_cu_cats   where id = '" . $r[$i]['cid'] . "' ", ARRAY_A);
            if ($r_cat[0]['name'] == '') {
                $cat = stripslashes($r_cat[0]['name']);
            } else {
                $cat = '';
            }
            if ($_REQUEST['search'] != "" && sp_cdm_get_project_name($r[$i]['pid']) != false) {
                $project_name = ' <em>('.sp_cdm_folder_name() .': ' . sp_cdm_get_project_name($r[$i]['pid']) . ')</em> ';
            } else {
                $project_name = '';
            }
           
		   if(get_option('sp_cu_file_direct_access') == 1){
			$file_link = 	'window.open(\''. cdm_download_file_link(base64_encode($r[$i]['id'].'|'.$r[$i]['date'].'|'.$r[$i]['file']),get_option('sp_cu_js_redirect')).'\'); void(0)'; ;
			}else{
			$file_link =  'cdmViewFile(' . $r[$i]['id'] . ')';	
			}
			
			
			
			
		
			if( in_array( $r[$i]['pid'],$current_user_projects) or   $r[$i]['pid'] == 0 or get_option('sp_cu_release_the_kraken') == 1 ){
		    	
			echo '<div class="sp-cdm-r-file" >
			
			<div class="sp-cdm-r-file-image">
			';
			do_action('spdm_file_list_column_before_file',$r[$i]['id'] );
			$file_link = apply_filters('spcdm/file_list/link', $file_link, $r[$i]);
			echo' <a href="javascript:'.$file_link.'">'.$img.'</a> </div>
			<div class="sp-cdm-r-file-file" onclick="'.$file_link.'" >
			';
			
			
			
			echo apply_filters('spcdm/files/responsive/file_name','<strong>' . stripslashes($r[$i]['name']) . ' ' . $project_name . '</strong><br>', $r);
			if( sp_cdm_is_featured_disabled('premium', 'file_list_date') == false){
				echo apply_filters('spcdm/files/responsive/file_date','<span class="sp-cdm-r-file-date"> Modified on ' . date("F jS Y g:i A", strtotime(cdm_get_modified_date($r[$i]['id']))) . '</span> ', $r); 
			}
			if( sp_cdm_is_featured_disabled('premium', 'file_list_file_size') == false){
				echo apply_filters('spcdm/files/responsive/file_size','<span class="sp-cdm-r-file-size">'.cdm_file_size(''.SP_CDM_UPLOADS_DIR . '' . $r[$i]['uid'] . '/' . $r[$i]['file'] . '').'</span> ', $r); 
			}
			if( sp_cdm_is_featured_disabled('premium', 'file_list_category') == false){
				echo apply_filters('spcdm/files/responsive/file_cat','<span class="sp-cdm-r-file-cat">'.cdm_get_category($r[$i]['cid']).'</span> ', $r).''; 
			}
			echo '</div>
				 
				  
				
<div style="clear:both"></div>
		</div>	

		';
		}
        }
	echo '</div>';
	die();
}

function sp_release_kraken_permissions($permission,$pid,$uid){
	
	if(get_option('sp_cu_release_the_kraken_permissions') == 1){
	$permission = 1;	

	}
	
	return $permission;
}




function sp_cdm_show_public_file_linked($html){
	global $wpdb;
		$fid = $_GET['fid'];
		
	
		if($fid != ''){
		$fid = base64_decode($fid);
		$r  = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu WHERE id = '" . $wpdb->escape($fid)  . "'", ARRAY_A);
		
		
		$html .='<script type="text/javascript">
					jQuery(document).ready(function() {
				
					';
					
					if($r[0]['pid'] != ''){
					
					$html .= 'sp_cdm_load_project('.$r[0]['pid'].')';	
						
					}
		$html .='	
		
						sp_cdm_showFile(' . $r[0]['id'] . ');
					});
				</script>';
		}
	
	return $html;
}
add_filter('sp_cdm_public_upload_view_above','sp_cdm_show_public_file_linked');


function sp_cdm_premium_link_to_file(){
			
			if($_GET['sp-cdm-public-link'] != ''){		
		
				
			
			$url = cdm_public_shortcode_url('fid='.$_GET['sp-cdm-public-link'].'');
			echo '<script type="text/javascript">
<!--
window.location = "'.$url.'"
//-->
</script>';
			
			
			exit;
			
		
			}		
}
add_action('wp_head', 'sp_cdm_premium_link_to_file');


function cdm_file_list_replace_folder_image($image){
	
	
	if(get_option('sp_cu_folder_button') != false){
		
	$image = get_option('sp_cu_folder_button');	
	}
	
	return $image;
	
}

function cdm_file_list_replace_folder_back_image($image){
	
	
	if(get_option('sp_cu_folder_back_button') != false){
		
	$image = get_option('sp_cu_folder_back_button');	
	}
	
	return $image;
	
}
	
function cdm_view_folder_log($id){
		global $wpdb;
	
	
		echo ' <a style="margin-left:20px" class="cdmViewFolderLog" href="#" data-id="'.$id.'"><img src="' . SP_CDM_PLUGIN_URL . 'images/folder.png" width="16">'.__('View Log', 'sp-cdm').'</a> ';
		

	
	
}

function sp_view_folder_log(){


		
echo cdm_get_event_log($_POST['pid'],'folder',999999);
	
die();	
}
function cdm_copy_link_dir($id){
		global $wpdb;
	
	
		echo ' <a style="margin-left:20px" href="javascript:sp_cu_dialog(\'#cdm_get_folder_link\',580,290);"><img src="' . SP_CDM_PLUGIN_URL . 'images/folder.png" width="16">'.__('Get Folder URL', 'sp-cdm').'</a> ';
		
		$link = sp_cdm_folder_link($id);
		echo '	<div style="display:none">
		<div id="cdm_get_folder_link" style="display:auto">
		<div style="padding:20px">
		<input type="text" name="shortlink" value="'.$link .'" style="width:100%"><br><br> <input type="button" value="Copy to clipboard" onclick="cdm_copyToClipboard(\''.$link .'\')";>
		</div>
		</div>
		</div>';
	
	
}

function cdm_responsive_button($html){
	 if (get_option('sp_cu_user_projects_thumbs') != 1 ){
	
	$html .='

	<style type="text/css">
	
	.cdm_responsive_button a {
    display: inline-block;
    background: #e4e4e4;
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f6f6f6), color-stop(100%,#d7d7d7));
    background: -moz-linear-gradient(center top, #f6f6f6 0%, #d7d7d7 100%);
    -webkit-box-shadow: -1px 1px 0px 0px #dfdcdc inset, 1px -1px 0px 0px #bfbfbf inset;
    -moz-box-shadow: -1px 1px 0px 0px #dfdcdc inset, 1px -1px 0px 0px #bfbfbf inset;
    box-shadow: -1px 1px 0px 0px #dfdcdc inset, 1px -1px 0px 0px #bfbfbf inset;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    padding: 0px 10px;
    display: inline-block;
    font-family: arial;
    font-size: 11px;
    line-height: 30px;
    color: #595959;
    font-weight: bold;
	margin-bottom:10px;margin-left:15px;
}
.cdm_responsive_button a:hover {
    -webkit-box-shadow: 0px 0px 0px 1px #a8a8a8 inset, 1px 1px 4px 0px #bababa;
    -moz-box-shadow: 0px 0px 0px 1px #a8a8a8 inset, 1px 1px 4px 0px #bababa;
    box-shadow: 0px 0px 0px 1px #a8a8a8 inset, 1px 1px 4px 0px #bababa;
    color: #424242;
}
.cdm_responsive_button a:active {
    background: #e6e6e6;
    -webkit-box-shadow: 0px 1px 5px 0px #808080 inset;
    -moz-box-shadow: 0px 1px 5px 0px #808080 inset;
    box-shadow: 0px 1px 5px 0px #808080 inset;
}
	

.cdm_responsive_button{display:none;}
.open-cdm-nav{display:block !important;width:200px;position:absolute; background-color:#FFF;padding:5px;box-shadow:0 2 2 #CCC}
.open-cdm-nav a{display:block !important;width:auto !important;float:none!important}
.close-cdm-nav{display:none;}
@media (max-width:768px) {
#cdm_nav_buttons{display:none;}
.cdm_responsive_button{display:inline;margin:-bottom:10px;}
.dropdown-tip{ background-color:#FFF}
}
	</style>
	<span class="cdm_responsive_button"><a href="#" class="dropdown-button" >Menu</a></span>';
	
	 }
	return $html;
}

function cdm_responsive_view_mode($upload_view,$overide_uid ){
	 if (get_option('sp_cu_user_projects_thumbs') != 1 ){
	
	unset($upload_view);
	
	global $wpdb,$current_user,$user_ID;
	if(get_option('sp_cu_wp_folder') == ''){
	$wp_con_folder = '/';	
	}else{
		$wp_con_folder = get_option('sp_cu_wp_folder') ;
	}
	
	if($overide_uid != ''){
		$user_ID = $overide_uid;
	}
	
	if($_GET['page'] == 'sp-client-document-manager-fileview'){
	$user_ID = $_GET['id'];	

	}
	
	$content .='
	
	<script type="text/javascript">
	
	
		
		
		function sp_cdm_sort(sort,pid){

		
			
		sp_cdm_loading_image();
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_responsive_view","uid":"' . $user_ID . '", "sort":sort,"pid":pid,"cid":cid,"seach_form":jQuery(".sp-cdm-search-form").serialize()},		
				success: function (msg) { 
			jQuery("#cdm-responsive-view").html(msg).fadeIn();
				}
		
			});		
		
		

}



	function cdm_ajax_search(){

		sp_cdm_loading_image();

	var cdm_search = jQuery("#search_files").val();
	var pid = jQuery.cookie("pid");
	var pid_search = "";
	if(pid != null){
	pid_search = "&pid=" + pid
	}
	

	
		sp_cdm_loading_image();
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_responsive_view","uid":"' . $user_ID . '","pid":pid,"search":cdm_search,"seach_form":jQuery(".sp-cdm-search-form").serialize()},		
				success: function (msg) { 
				jQuery("#cdm-responsive-view").html(msg).fadeIn();
				}
		
			});			
	
	
		 cdm_check_file_perms(pid);
		 cdm_check_folder_perms(pid);
	}

	
	function sp_cdm_loading_image(){
		jQuery("#cdm-responsive-view").html(\'<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>\');		
	}
	function sp_cdm_load_file_manager(){
		sp_cdm_loading_image();
		
	
	sp_cdm_loading_image();
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_responsive_view","uid":"' . $user_ID . '","seach_form":jQuery(".sp-cdm-search-form").serialize()},		
				success: function (msg) { 
				jQuery("#cdm-responsive-view").html(msg).fadeIn();
				}
		
			});		
	
		
	
	cdm_ajax_search();
	}
	
	jQuery(document).ready( function() {
			
			
					
var pid = jQuery.cookie("pid");

	if(pid != 0){
	sp_cdm_load_project(pid)
	}else{
	sp_cdm_load_file_manager();	
	}
		

			
		});
		
		
		function sp_cdm_load_project(pid){
			jQuery.cookie("pid", pid, { expires: 7 , path:"/" });
			jQuery(".cdm_premium_pid_field").attr("value", pid);
			sp_cdm_loading_image();
			
				jQuery("#cdm_current_folder").val(pid);
				
				
				if(pid != 0 && jQuery("#cdm_premium_sub_projects").val() != 1){
				jQuery(".cdm_add_folder_button").hide();	
			
				}else{
				jQuery(".cdm_add_folder_button").show();
				
				}
				
				
	
			
	sp_cdm_loading_image();
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_responsive_view","uid":"' . $user_ID . '","pid":pid,"seach_form":jQuery(".sp-cdm-search-form").serialize()},		
				success: function (msg) { 
				jQuery("#cdm-responsive-view").html(msg).fadeIn();
				}
		
			});		
	
			
		 cdm_check_file_perms(pid);	
		  cdm_check_folder_perms(pid);
		}
	</script>
	';
	$content .='
	
	<form id="cdm_wrapper_form">';
  $extra_js = apply_filters('sp_cdm_uploader_above',$extra_js);
  $content .= $extra_js;
	$content .='<input type="hidden" name="cdm_current_folder" id="cdm_current_folder" value="0">
	
	<div id="cdm-responsive-view">
	<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>	
	
	</div>
	<div style="clear:both"></div>
	</form>
	';
	$upload_view = $content;
	 }
	return $upload_view;
	

	
}
function cdm_change_status_chooser_email(){
	
		global $wpdb;
		
		echo '  
<h2>Change Status Notification</h2>
<em>This email is dispatched to the file owner and all parties involved with the file should the status of a file be changed</em>
<table class="wp-list-table widefat fixed posts" cellspacing="0">
     

    <tr>

    <td width="300"><strong>User Email</strong><br><em>This is the email that is dispatched to user.</em><br><br>Template Tags:<br><br>

	

	[file] = Link to File<br>
	[file_name] = Actual File Name<br>
	
	[file_real_path] = Real Path URL to the file<br>
	[notes] = Notes or extra fields<br>

	[user] = users name<br>
	
	[uid] = User ID<br>
	[project] = project<br>

	[category] = category (status)<br>

	[user_profile] = Link to user profile<br>

	[client_documents] = Link to the client document manager</td>

    <td>Subject: <input style="width:100%" type="text" name="sp_cu_change_status_subject" value="' . get_option('sp_cu_change_status_subject') . '"> CC: <input style="width:100%" type="text" name="sp_cu_change_status_cc" value="' . stripslashes(get_option('sp_cu_change_status_cc')) . '" ><br>Body:<br>
	';
	echo wp_editor(  stripslashes(get_option('sp_cu_change_status_email')) , 'sp_cu_change_status_email' );
	echo ' </td>

  </tr>';
  
  do_action('sp_cu_email_extra', 'sp_cu_change_status_email');
  echo'</table>';
	
}

function cdm_change_status_chooser($html,$r){
	global $wpdb;
	  $cats = $wpdb->get_results("SELECT *
	
									 FROM ".$wpdb->prefix."sp_cu_cats
									
									 ", ARRAY_A);	

	if(count($cats) != 0){
	
	
	if($r[0]['cid'] != 0){
		
		$status = sp_cdm_category_value($r[0]['cid']);
		
	}else{
		$status = ''.__("No Status Selected","sp-cdm").'';
	}
	$html .='<div class="sp_su_notes sp_su_category">

<strong>'.get_option('sp_cu_cat_text').': </strong> <em><span class="cdm_current_status">'.$status.' </span></em>
';

$html .='</div>';
	
if(cdm_file_permissions($r[0]['pid']) == 1 && sp_cdm_is_featured_disabled('premium', 'view_file_edit_category') == false){
$category_selector ='

<div class="sp_su_notes">
<script type="text/javascript">

jQuery(document).ready(function() {
			
			jQuery(".cdm_save_cat").click(function() {
				
						jQuery.ajax({
		
					   type: "POST",
					   data: {"action":"cdm_premium_change_status","cid":jQuery(".cdm_change_cat").val(),"id":"'.$r[0]['id'].'"},
					   url:sp_vars.ajax_url,			  
							
					   success: function(msg){
						jQuery(".cdm_current_status").html(msg);
						
					    jQuery( ".sp_su_category" ).effect("highlight", {}, 3000);
						cdm_refresh_file_view('.$r[0]['id'].');
						cdm_ajax_search();
					   }
		
					 });

				return false;
			});
	
});
</script>

'.sp_cdm_categories_dropdown('cdm_change_cat',$r[0]['cid'],'Choose a '.get_option('sp_cu_cat_text').'').' <a href="#" class="cdm_save_cat" style="text-decoration:none;"><img src="'.plugins_url().'/sp-client-document-manager/images/application_edit.png" > Save </a>
</div>';

$html .= apply_filters('sp_cdm/premium/filter/viewfile/category_selector',$category_selector);

}	
	
	}
	return $html;
}

function cdm_default_order_list($orderby){
	

	
	if(get_option('sp_cu_default_ordering') != ''){
			
			$new_order .= get_option('sp_cu_default_ordering');
			if(get_option('sp_cu_default_ordering_type') != ''){
				$new_order  .= ' '.get_option('sp_cu_default_ordering_type').'';
			}
	return $new_order;		
	}else{
	return $orderby;
	}
}

function cdm_list_thumbnail_icons($extra_js){
	
	
	//$h .= '<div class="cdm_change_icon_mode">
//	<img src="'.content_url().'/plugins/sp-client-document-manager-premium/images/thumbs.png">  <img src="'.content_url().'/plugins/sp-client-document-manager-premium/images/list.png">
	//</div>';
	
	
	
	$extra_js .= 	$h ;
	return $extra_js;
}

function display_sp_thumbnails_public($r){
	
	global $wpdb,$current_user,$user_ID;
	if(get_option('sp_cu_wp_folder') == ''){
	$wp_con_folder = '/';	
	}else{
		$wp_con_folder = get_option('sp_cu_wp_folder') ;
	}
	
	
	$content .='
	
	<script type="text/javascript">
		function cdm_ajax_search(){
		sp_cdm_loading_image();
	var cdm_search = jQuery("#search_files").val();

			

		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_public_file_list","uid":"' . $user_ID . '","search":cdm_search},		
				success: function (msg) { 
				jQuery("#cmd_file_thumbs").html(msg).fadeIn();
				}
		
			});		
			
		
	}
	function sp_cdm_sort(sort,pid){
	if(pid != ""){
		var pidurl = "&pid=" + pid;
	}else{
		var pidurl = "&cid=" + pid;	
	}
				
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_public_file_list","uid":"' . $user_ID . '","sort":pidurl},		
				success: function (msg) { 
				jQuery("#cmd_file_thumbs").html(msg).fadeIn();
				}
		
			});		
		
		
}

	
	function sp_cdm_loading_image(){
		jQuery("#cmd_file_thumbs").html(\'<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>\');		
	}
	function sp_cdm_load_file_manager(){
		sp_cdm_loading_image();
	
	jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_public_file_list","uid":"' . $user_ID . '"},		
				success: function (msg) { 
				jQuery("#cmd_file_thumbs").html(msg).fadeIn();
				}
		
			});		
		
	cdm_ajax_search();
	}
	
	jQuery(document).ready( function() {
			
			
		
		 sp_cdm_load_file_manager();

			
		});
		
		
		function sp_cdm_load_project(pid){
			sp_cdm_loading_image();
	
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_public_file_list","uid":"' . $user_ID . '","pid":pid},		
				success: function (msg) { 
				jQuery("#cmd_file_thumbs").html(msg).fadeIn();
				}
		
			});			
			
		}
		
		
		function sp_cdm_showFile(file){
			
		 jQuery(".view-file-public").empty();
				
		
			
		jQuery.ajax({					
				type: "GET",
				url: sp_vars.ajax_url,
				data: {"action":"cdm_premium_view_file","id": file},		
				success: function (data) { 
				
					 jQuery(".view-file-public").html(data);
						
				
				 jQuery(".cdm-modal-public").remodal({ hashTracking: false});	
				 var inst = jQuery.remodal.lookup[jQuery("[data-remodal-id=file-public]").data("remodal")];
				 inst.open();
				
				
				}
		
			});		
		
		
		
			
			
		

		}
	</script>
	
	<div id="cdm_wrapper">
	<div id="cmd_file_thumbs">
	<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>	
	
	</div>
	<div style="clear:both"></div>
	</div>
	
	
		<div style="display:none">
	<div class="cdm-modal-public" data-remodal-options="{ \'hashTracking\': false }" data-remodal-id="file-public">
			<div class="view-file-public">
			
			</div>
		</div>
				
		</div>
	
	
	
	';
	return $content;
	
}

function cdmPublicView($atts){
		global $wpdb;
		
		
		
		$r = $wpdb->get_results("SELECT *  FROM ".$wpdb->prefix."sp_cu     order by date desc", ARRAY_A);


		$html = apply_filters('sp_cdm_public_upload_view_above', $html,$r);
	$html .='Search: <input  onkeyup="cdm_ajax_search()" type="text" name="search" id="search_files">';
if(get_option('sp_cu_user_projects_thumbs') == 1 && CU_PREMIUM == 1){
	$html .=display_sp_thumbnails_public($r);
}else{
		$html .=display_sp_thumbnails_public($r);
}

	$html = apply_filters('sp_cdm_public_upload_view_below', $html,$r);
	return $html;
}
	add_shortcode( 'cdm_public_view', 'cdmPublicView' );	
function cdmFindGroups($id,$projector = NULL){
	
	
	global $wpdb;
	
	
	if($projector == NULL){
		
		
		$table = "".$wpdb->prefix."sp_cu.uid";
		
	}else{
		$table = "uid";
	}
$r_group = $wpdb->get_results("SELECT ".$wpdb->prefix."sp_cu_groups_assign.gid,
											  ".$wpdb->prefix."sp_cu_groups_assign.uid,
											  ".$wpdb->prefix."sp_cu_groups_assign.id AS asign_id,
											  ".$wpdb->prefix."sp_cu_groups.name,
											  ".$wpdb->prefix."sp_cu_groups.id AS group_id
											    FROM ".$wpdb->prefix."sp_cu_groups_assign 
												LEFT JOIN   ".$wpdb->prefix."sp_cu_groups ON ".$wpdb->prefix."sp_cu_groups_assign.gid = ".$wpdb->prefix."sp_cu_groups.id
												WHERE uid = '".$id."' ", ARRAY_A);
		
		
	
		if(count($r_group ) > 0){
			
	for($i=0; $i<count($r_group ); $i++){
		$r_group_find = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."sp_cu_groups_assign 
												
												WHERE gid = '".$r_group[$i]['group_id']."' ", ARRAY_A);
												
												for($j=0; $j<count($r_group_find ); $j++){
													
													$search .= " or ".$table." = ".$r_group_find[$j]['uid']." ";
												}
		
	}
			
		}
		
		return $search;


	
}

function sp_cdm_replace_custom_fields($content,$file){
global $wpdb;

		$r = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_forms", ARRAY_A);	
		$r_entry = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_form_entries 
		
												where file_id = %d",$file['id']), ARRAY_A);	
		
	
		$entries = array();		
		if($r_entry){
			for($i=0; $i<count(	$r_entry); $i++){	  
			
			$entries[$r_entry[$i]['fid']] = $r_entry[$i]['value'];
			}
			
		}
													
if($r){
	for($i=0; $i<count(	$r); $i++){	  
		
			
		
		
		$content = str_replace('[custom_form_field_'.$r[$i]['id'].']',$entries[$r[$i]['id']], $content);
	
	}
	
}
		
	return $content;
}
function sp_cdm_get_form_fields($fid){
		global $wpdb;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT  * FROM ".$wpdb->prefix."sp_cu_form_entries 
		
												where file_id = %d",$fid), ARRAY_A);	
												
									
	if(count($r)>0 && $r[0]['value'] != ''){
		$content .='<p><em>';
	

		for($i=0; $i<count(	$r); $i++){	  
			$r_field = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_forms
		
												where id = ".$r[$i]['fid']." order by sort", ARRAY_A);		
				
				
				if($r_field[0]['name'] == ''){
					$name = 'Notes';
				}else{
					$name = $r_field[0]['name'];
				}
		$content .='<strong>'.stripslashes($name).':</strong> '.stripslashes($r[$i]['value']).'<br>';
		
		}
		$content  .='</em></p>';
	}
	return $content;
	
}
function process_sp_cdm_form_vars($vars,$id){
		global $wpdb,$current_user,$user_ID;
	
	
	if(count($vars) > 0){
	foreach($vars as $key => $value){
		unset($insert);
		$insert['file_id'] = $id;
		$insert['value'] =  $value;
		$insert['fid'] = $key;
		$insert['uid'] = $user_ID;
		 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
		$wpdb->insert("".$wpdb->prefix."sp_cu_form_entries", $insert);
		
	}
	}
	
}

function display_sp_cdm_form(){
	global $wpdb,$current_user,$user_ID;
	
	
		$r = $wpdb->get_results("SELECT  * FROM ".$wpdb->prefix."sp_cu_forms order by sort", ARRAY_A);	
		
		
			for($i=0; $i<count(	$r); $i++){	  
			
			if($r[$i]['required'] == 'Yes'){
			$class = ' class="required" ';	
			$req = ' required ';	
			$required_ast = ' <span style="color:red">*<span> ';	
			}else{
			$class = '';	
			$required_ast = '';	
			$req = ' ';	
			}
			
  $html .='<p><label>'.stripslashes($r[$i]['name']).' '.$required_ast.'</label>';
	
	if($r[$i]['type'] == 'textbox'){
	$html.='<input '.$class.' '.$req.' type="text" name="custom_forms['.$r[$i]['id'].']" name="" value="'.stripslashes($r[$i]['defaults']).'">';	
	}elseif($r[$i]['type'] == 'textarea'){
	
	$html.='<textarea '.$class.' style="width:75%;height:70px" name="custom_forms['.stripslashes($r[$i]['id']).']">'.stripslashes($r[$i]['defaults']).'</textarea>';
	}elseif($r[$i]['type'] == 'select'){
		
		$selects = explode(",",$r[$i]['defaults']);
		$html .='<select '.$class.' name="custom_forms['.$r[$i]['id'].']">';
		foreach($selects as $key => $value){
			
			$html .='<option value="'.$value.'">'.$value.'</option>';
		}
		$html .='</select>';
		
		
	}
	$html .='
  </p>
  ';
	
			}
			
			return $html;
	
	
}
function display_sp_cdm_thumbnails($r,$overide_uid = false ){
	global $wpdb,$current_user,$user_ID;
	if(get_option('sp_cu_wp_folder') == ''){
	$wp_con_folder = '/';	
	}else{
		$wp_con_folder = get_option('sp_cu_wp_folder') ;
	}
	
	if($overide_uid != false){
		
		$user_ID = $overide_uid;
		
	}
	$content .='
	
	<script type="text/javascript">
	
		function sp_cdm_sort(sort,pid){

		
	if(pid != ""){

		var pidurl = "&pid=" + pid;

	}else{

		var pidurl = "&cid=" + pid;	

	}

		
		jQuery.get(sp_vars.ajax_url, {action: "cdm_thumbnails", uid: "'.$user_ID.'", sort: sort}, function(response){
				jQuery("#cmd_file_thumbs").html(response).hide().fadeIn();
		})
		 cdm_check_folder_perms(pid);
		 cdm_check_file_perms(pid);
		
}		



	
	
	function sp_cdm_loading_image(){
		jQuery("#cmd_file_thumbs").html(\'<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>\');		
	}
	function sp_cdm_load_file_manager(){
				sp_cdm_loading_image();


	jQuery.get(sp_vars.ajax_url, {action: "cdm_thumbnails", uid: "'.$user_ID.'"}, function(response){
				jQuery("#cmd_file_thumbs").html(response).hide().fadeIn();
	})
	
	cdm_ajax_search();
	
	}
	
	jQuery(document).ready( function() {
			
			
					
var pid = jQuery.cookie("pid");

	if(pid != 0){
	sp_cdm_load_project(pid)
	}else{
	sp_cdm_load_file_manager();	
	}
		

			
		});
		
		
		function sp_cdm_load_project(pid){
					sp_cdm_loading_image();
	jQuery("#cdm_current_folder").val(pid);
	
	jQuery(".cdm_premium_pid_field").attr("value", pid);
	jQuery.cookie("pid", pid, { expires: 7 , path:"/" });
	
			if(pid != 0 && jQuery("#cdm_premium_sub_projects").val() != 1){
				jQuery(".cdm_add_folder_button").hide();	
			
				}else{
				jQuery(".cdm_add_folder_button").show();
			
				}
	    cdm_check_folder_perms(pid);
		  cdm_check_file_perms(pid);
		    jQuery("#sub_category_parent").val(pid);
		  jQuery(".cdm_premium_pid_field").val(pid);
		  
		  
		  
		
		jQuery.get(sp_vars.ajax_url, {action: "cdm_thumbnails", uid: "'.$user_ID.'", pid: pid}, function(response){
						jQuery("#cmd_file_thumbs").html(response).hide().fadeIn();
		})
			
			
		}

		
			function cdm_ajax_search(){

		
		sp_cdm_loading_image();
	var cdm_search = "";
	cdm_search = jQuery("#search_files").val();
	
	var pid = jQuery.cookie("pid");

	if(! pid){
	var pid = 0;	
	}

				
	
	jQuery.get(sp_vars.ajax_url, {action: "cdm_thumbnails", uid: "'.$user_ID.'", pid: pid, search: cdm_search}, function(response){
				jQuery("#cmd_file_thumbs").html(response).hide().fadeIn();
	})
		
 		  cdm_check_folder_perms(pid);
		  cdm_check_file_perms(pid);

	}

	</script>
	';
	$content .='<form id="cdm_wrapper_form">';
  $extra_js = apply_filters('sp_cdm_uploader_above',$extra_js);
  $content .= $extra_js;
	$content .='<input type="hidden" name="cdm_current_folder" id="cdm_current_folder" value="0">
	<div id="cdm_wrapper">
	<div id="cmd_file_thumbs">
	<div style="padding:100px; text-align:center"><img src="'.SP_CDM_PLUGIN_URL.'images/loading.gif"></div>	
	
	</div>
	<div style="clear:both"></div>
	</div></form>
	';
	return $content;
	
}
function sp_cdm_categories_dropdown_admin($id,$selected, $default = NULL){
	global $wpdb,$current_user;
	
	
  $cats = $wpdb->get_results("SELECT *
	
									 FROM ".$wpdb->prefix."sp_cu_cats
									
									 ", ARRAY_A);	

	if($default == NULL){
	$default = ''.__("No ".get_option('sp_cu_cat_text')."","sp-cdm").'';	
	}
	 if(get_option('sp_cu_require_category') == 1){
		$required = ' required'; 
	 }
	$html .='	<select name="'.$id.'" class="'.$id.'" id="'.$id.'" '.$required.'>
	
	<option value=""  selected="selected">'.$default.'</option>';

		for($i=0; $i<count( $cats ); $i++){
			
			
			if(get_option('sp_cu_cat_default') != '' && $cats[$i]['id'] == get_option('sp_cu_cat_default')){
				
			$selected = ' selected="selected"';		
			}else{
			$selected = '';		
			}
	  $html .= '<option value="'. $cats[$i]['id'].'" '.$selected.'>'.stripslashes($cats[$i]['name']).'</option>';	
		}
	
	$html .='</select>';
	
	
	return $html;
}

function sp_cdm_categories_dropdown($id,$selected= NULL, $default = NULL){
	global $wpdb,$current_user;
	
	
  $cats = $wpdb->get_results("SELECT *
	
									 FROM ".$wpdb->prefix."sp_cu_cats
									
									 ", ARRAY_A);	

	if($default == NULL){
	$default = ''.__("No ".get_option('sp_cu_cat_text')."","sp-cdm").'';	
	}
	 if(get_option('sp_cu_require_category') == 1){
		$required = ' required'; 
	 }
	$html .='	<select name="'.$id.'" class="'.$id.'" id="'.$id.'" '.$required.'>
	
	<option value=""  selected="selected">'.$default.'</option>';

		for($i=0; $i<count( $cats ); $i++){
			
			if($cats[$i]['id'] == $selected){
			$selected_text = ' selected="selected"';	
			}else{
			$selected_text = '';		
			}
			
	  $html .= '<option value="'. $cats[$i]['id'].'" '.$selected_text.'>'.stripslashes($cats[$i]['name']).'</option>';	
		}
	
	$html .='</select>';
	
	
	return $html;
}
function sp_cdm_display_categories(){
	
	
	global $wpdb,$current_user;


  $cats = $wpdb->get_results("SELECT *
	
									 FROM ".$wpdb->prefix."sp_cu_cats
									
									 ", ARRAY_A);	


  if(count($cats) > 0 ){
	 
	 if(get_option('sp_cu_require_category') == 1){
		$required = ' required'; 
	 }
	  $html .= ' 
	  <p ><label>'.get_option('sp_cu_cat_text').' </label></p>
	  <span class="sp-category-select-error"></span>
	<select name="cid" '.$required.' class="sp-category-select">';
	
	if(get_option('sp_cu_require_category') != 1){
		$html .='<option value="" selected="selected">'.__(sprintf("No %s",get_option('sp_cu_cat_text')),"sp-cdm").'</option>';
	}else{
		$html .='<option value="">'.__(sprintf("Choose A %s",get_option('sp_cu_cat_text')),"sp-cdm").'</option>';	
	}
		for($i=0; $i<count( $cats ); $i++){
	  $html .= '<option value="'. $cats[$i]['id'].'">'.stripslashes($cats[$i]['name']).'</option>';	
		}
	
	$html .='</select><input type="hidden" class="can-upload-cat" value="1"><span class="cat-upload-message"></span>';
	

	  
  }

	return $html;
	
}


?>