<?php
$cdmContextMenu = new cdmContextMenu;
class cdmContextMenu{
	
	
	function __construct(){
	add_action('admin_init', array($this,'js'));	
	add_action('init', array($this,'js'));	
	add_action('wp_head', array($this,'css'));
	add_action('admin_head', array($this,'css'));
	add_filter('sp_cdm_uploader_above', array($this,'makeMenu'));
	
	
	}
	
	function  js(){
	
	  		 wp_enqueue_script('jquery-context-menu-js', plugins_url('sp-client-document-manager-premium/js/jquery.auderoContextMenu.js'), array('jquery'));
		
	}
	
	function css(){
		
	 wp_register_style( 'jquery-context-menu', plugins_url('sp-client-document-manager-premium/css/jquery.auderoContextMenu.css'));       
	 wp_enqueue_style( 'jquery-context-menu' );
		
	}
	
	
	function makeMenu($extra_js){
		
	global $wpdb;	
		
		if( sp_cdm_is_featured_disabled('premium', 'context_menu') == false){
		
		$extra_js .= '    <script type="text/javascript">
        	
		 jQuery(document).ready(function() {
         
		 
 
		 
		    jQuery("#cdm_wrapper,#cdm-responsive-view").auderoContextMenu(
			{
         idMenu: "cdm_context_menu"
      });
            
         });
      </script>
	  
	   <ul id="cdm_context_menu" class="audero-context-menu">
     
	 ';
	  if (get_option('sp_cu_user_uploads_disable') != 1  or 
			(get_option('sp_cdm_groups_addon_global_add_roles_'.sp_cdm_get_current_user_role_name ().'') == '' or
			get_option('sp_cdm_groups_addon_global_add_roles_'.sp_cdm_get_current_user_role_name ().'') == 1 )
			
			) {
				
				
		if(get_option('sp_cu_user_projects') == 1  or current_user_can( 'manage_options' )){	
	 $extra_js .='
	     <li class="cdm_add_folder_button hide_add_folder_permission"><a href="#folder" ><img src="'.SP_CDM_PLUGIN_URL.'images/folder_add.png"> '.__("New","sp-cdm").' '.sp_cdm_folder_name().'</a></li>';
		}
		 if (cdm_user_can_add($current_user->ID) == true){
		 $extra_js .='
         <li class="hide_add_file_permission"><a href="#premium-upload" ><img src="'.SP_CDM_PLUGIN_URL.'images/add_small.png"> New File</a></li>';
		 }
		 if(class_exists('sp_cdm_dropbox')){
		$extra_js .= '<li class="hide_add_file_permission"><a href="#dropbox" ><img src="'.plugins_url('sp-cdm-dropbox/images/Dropbox.png').'" style="height:16px;width:16px"> '.__("New File From DropBox","sp-cdm").'</a></li>'; 
		 }
		 
			}
		 $extra_js .= '   <li><a href="javascript:sp_cdm_context_refresh()" ><img src="'.SP_CDM_PLUGIN_URL.'images/arrow_refresh.png"> '.__("Refresh","sp-cdm").'</a></li>
         <li><a href="'.wp_logout_url().'"><img src="'.SP_CDM_PLUGIN_URL.'images/delete_small.png"> '.__("Signout","sp-cdm").'</a></li>
      </ul>
	
	
	<script type="text/javascript">
	
function sp_cdm_context_refresh(){
	
	
	sp_cdm_load_project(jQuery(\'#cdm_current_folder\').val());
}
	function sp_cdm_context_add_folder(){
		var context_folder_pid = jQuery(\'#cdm_current_folder\').val();
		
	
			jQuery.ajax({

			   type: "POST",

			   url: sp_vars.ajax_url,
			   data:{"action":"cdm_premium_add_folder","project-name": jQuery(\'#project-name-context\').val(),"parent":context_folder_pid},			  

			   success: function(msg){

			 sp_cdm_load_project(context_folder_pid);
			 jQuery(\'#sp_cdm_add_folder\').dialog(\'close\');
			 jQuery(\'#project-name-context\').val(\'\');
			 sp_cu_reload_all_projects(context_folder_pid);
			   }

			 });

		
	}
	
	</script>
	  

	  ';
		}
		return $extra_js;
	}
	
	
	
}
?>