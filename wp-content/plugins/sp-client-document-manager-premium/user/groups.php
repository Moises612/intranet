<?php
$sp_cdm_groups_permissions = new sp_cdm_groups_permissions;
add_filter('sp_cdm_has_permission',array($sp_cdm_groups_permissions,'check_permissions'),10,5);

add_filter('cdm_file_permissions',array($sp_cdm_groups_permissions,'file_permissions'),8,3);
add_filter('cdm_folder_permissions',array($sp_cdm_groups_permissions,'file_permissions'),8,3);

add_filter('cdm_delete_permissions',array($sp_cdm_groups_permissions,'file_delete_permissions'),8,3);


add_filter('cdm/common/get_projects',array($sp_cdm_groups_permissions,'find_share_space_projects'),8,2);
#add_filter('sp_cdm_projects_query',array($sp_cdm_groups_permissions,'find_share_space_folders'),8,2);





class sp_cdm_groups_permissions{
	
	
		function find_share_space_folders($search_file,$pid){
		
		global $current_user,$wpdb;
		
		
		#sp_cdm_file_search_query
		$r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  uid = '" . $current_user->ID . "'", ARRAY_A);
		
		
		if($r != false){
			
				
		for($i=0; $i<count($r ); $i++){
			$r_check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  gid = '" . $r[$i]['gid'] . "'", ARRAY_A);
			
			for($i=0; $i<count($r_check ); $i++){
				$ids[] =$r_check [$i]['uid'];
			}
			
		}
			$result = array_unique($ids);
		
			if(count($result) >0  ){
				foreach ($result as $uid){
				$search_file .= ' or ' . $wpdb->prefix . 'sp_cu_project.uid = "'.$uid.' "';
				}
				
			}
			
		}
		
		return $search_file;
	}
	function find_share_space_projects($projects,$uid){
		
		
		global $current_user,$wpdb;
		if($projects == ''){
			
			$projects = array();
		}
		$found_projects = array();
		#sp_cdm_file_search_query
		$r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  uid = '" . $current_user->ID . "'", ARRAY_A);
		
		
		if($r != false){
			
				
		for($i=0; $i<count($r ); $i++){
			$r_check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  gid = '" . $r[$i]['gid'] . "'", ARRAY_A);
			
			for($i=0; $i<count($r_check ); $i++){
				$ids[] =$r_check [$i]['uid'];
			}
			
		}
			$result = array_unique($ids);
		
			if(count($result) >0 && $pid == 0){
				foreach ($result as $uid){
				$r_projects = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project WHERE  uid = %d", $uid), ARRAY_A);
				
					for($i=0; $i<count($r_projects); $i++){
						
						
						$found_projects[] = $r_projects[$i]['id'];
						
						
					}
				}
				
			}
			
		}
		
		if(count($found_projects) > 0){
		$projects_rough = array_merge($found_projects, $projects);
		$final_list = array_unique($projects_rough);
		return $final_list;
		}else{
		$final_list = array_unique($projects);	
		return $projects;	
		}
		
	
	}
	
	function file_delete_permissions($permission,$pid,$uid){
		
			if(class_exists('sp_cdm_groups_addon_projects')){
			$pide = sp_cdm_groups_addon_projects ::findDirectParent($pid);
		}else{
			$pide = $pid;
		}
	
		if(get_option('sp_cdm_groups_share_user_'.$pide.'') != false){
		$folders = unserialize(get_option('sp_cdm_groups_share_user_'.$pide.''));
		
		if($folders[$uid]['delete'] == 1){
		$permission = 1;	
		}
		
		}
		
	return $permission;	
	}
	
	function file_permissions($permission,$pid,$uid){
		global $wpdb,$current_user;
			#	echo $pid;
	 		#$tree = $this->get_folder_tree($pid);
			#print_r($tree);exit;
		
		$r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project WHERE  id = %d", $pid), ARRAY_A);
		
		if($r[0]['uid'] !=  $current_user->ID && $pid != 0){
		
		if(class_exists('sp_cdm_groups_addon_projects')){
		
			$pide = findRootParent($pid);
			
		}else{
			$pide = $pid;
		}

		if(get_option('sp_cdm_groups_share_user_'.$pide.'') != false){
		
		$folders = unserialize(get_option('sp_cdm_groups_share_user_'.$pide.''));
		
		if($folders[$uid]['write'] == 1){
		$permission = 1;	
		}else{
		$permission = 0;		
		}
		
		}
		}
		
		
	return $permission;	
	}
		
		function has_children($pid){
			global $wpdb;
			$r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project WHERE  parent = %d", $pid), ARRAY_A);	
			
			if($r == false){
				
				return false;
			}else{
				return true;	
			}
		}
		function get_folder_tree($pid){
			global $wpdb;
			$sp_cdm_groups_addon_projects  = new sp_cdm_groups_addon_projects ;
			$groups = array();
			
			if($pid != 0){
			
			$groups = $sp_cdm_groups_addon_projects->findChildrenArray($pid);
			
			if(count($groups)>0){
					foreach($groups as $group){
					 $permissions = @unserialize(get_option('sp_cdm_groups_addon_role_permission_'.$group.''));
					 
					 print_r($permissions);	
						
					}
				
			}
			}
			return $groups;
			
			
		}
		function folder_permissions($permission,$pid,$uid){
		global $wpdb,$current_user;
			
	
			
		$r = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "sp_cu_project WHERE  id = %d", $pid), ARRAY_A);
		
		if($r[0]['uid'] !=  $current_user->ID && $pid != 0){
		
		if(class_exists('sp_cdm_groups_addon_projects')){
		
			$pide = findRootParent($pid);
			
		}else{
			$pide = $pid;
		}

		if(get_option('sp_cdm_groups_share_user_'.$pide.'') != false){
		
		$folders = unserialize(get_option('sp_cdm_groups_share_user_'.$pide.''));
		
		if($folders[$uid]['write'] == 1){
		$permission = 1;	
		}else{
		$permission = 0;		
		}
		
		}
		}
		
		
	return $permission;	
	}
	
	
	function check_permissions($view,$uid,$file_uid,$id,$type){
		global $wpdb,$current_user;
	
	
		$r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  uid = '" . $file_uid . "'", ARRAY_A);
	
		for($i=0; $i<count($r ); $i++){
		
				
			$r_check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "sp_cu_groups_assign WHERE  gid = '" . $r[$i]['gid'] . "'", ARRAY_A);
			
			for($i=0; $i<count($r_check ); $i++){
				$ids[] =$r_check [$i]['uid'];
			}
		}
		if(in_array($uid,$ids)){
		$view = 1;	
		}
		
		
		return $view;
		
	}


	
}

?>