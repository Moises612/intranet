<?php

function cdm_woocommerce_settings(){
			
			
			$args = array(
	'post_type' => 'product',	
	'posts_per_page' => -1,	
			);
		$query = new WP_Query( $args );

	$options .='<select name="cdm_woocommerce_product">
		<option value="" '.$selected.'>Select a product to associate digital products</option>';
	if($query->found_posts > 0){
		foreach($query->posts as $post){
		if(get_option('cdm_woocommerce_product') == $post->ID){
		$selected = 'selected="selected"';	
		}else{
		$selected = '';	
		}
		$options .= '<option value="'.$post->ID.'" '.$selected.'>'.$post->post_title.'</option>';	
			
		}
		
	}	
	
		if($_POST['save_options'] != ''){		
		if($_POST['sp_cu_user_enable_woocommerce'] == "1"){update_option('sp_cu_user_enable_woocommerce','1' ); }else{update_option('sp_cu_user_enable_woocommerce','0' );	}	
		}
	
		if(get_option('sp_cu_user_enable_woocommerce') == 1){ $sp_cu_user_enable_woocommerce = ' checked="checked" ';	}else{ $sp_cu_user_enable_woocommerce = '  '; }
		
			
	
	$options .='</select>';
			echo '   <h2>Woocommerce Settings</h2>
		
		 <table class="wp-list-table widefat fixed posts" cellspacing="0">
		    <tr>
    <td width="300">Enable Woocommerce Features <br></td>
    <td><input type="checkbox" name="sp_cu_user_enable_woocommerce"   value="1" '. $sp_cu_user_enable_woocommerce.'> </td>
  </tr>
<tr>
    <td width="300"><strong>Product for digitial products</strong><br><em>Choose a product for digital products, this will be used as the base product for adding items to the cart. You can set the price in the product itself.</em></td>
    <td>'.$options.'</td>
  </tr>
    <tr>
    <td width="300"><strong>Add to cart text</strong><br><em>Text used to add to cart.</em></td>
    <td><input type="text" name="cdm_woocommerce_add_to_cart"    value="'.get_option('cdm_woocommerce_add_to_cart', 'Add to cart').'"  size="80"> </td>
  </tr>
  <tr>
    <td width="300"><strong>Added to cart text</strong><br><em>Text when item is added to cart.</em></td>
    <td><input type="text" name="cdm_woocommerce_added_to_cart"    value="'.get_option('cdm_woocommerce_added_to_cart', 'Added to cart').'"  size="80"> </td>
  </tr>
<tr>

    
<tr>
    <td>&nbsp;</td>
    <td><input type="submit" name="save_options" value="Save Options"></td>
  </tr></table>';
			
			
		wp_reset_query();	
		}
add_action('cdm_premium_settings', 'cdm_woocommerce_settings');


if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && get_option('sp_cu_user_enable_woocommerce') == 1 ) {

			
class sp_cdm_woocommerce{
	
	
		function __construct(){
			
			
			
		}
		
		function add_to_cart_ajax(){
			
		global $woocommerce;
		
		$data['cart_url'] = $woocommerce->cart->get_cart_url();
		$data['cart_message'] = get_option('cdm_woocommerce_added_to_cart', 'Added to cart');
		$data['file_id'] = $_POST['file_id'];
		$product_id = get_option('cdm_woocommerce_product') ;
		$quantity = 1;
		$variation_id = '';
		$variation = array();
		$cart_item_data = 		array(
							'file_id' => $_POST['file_id']
							);
		
		 WC()->cart->add_to_cart( $product_id, $quantity , $variation_id , $variation,$cart_item_data);
			
			echo json_encode($data); 
		 exit;	
			
		}
		function disable_features(){
			
			
			
		}
		
		function add_cart_button($html,$r){
			
				if( sp_cdm_is_featured_disabled('premium', 'woocommerce_viewfile_add_cart') == false ){	
		
			$html .= '	<a href="#"  title="'.get_option('cdm_woocommerce_add_to_cart', 'Add to cart').'" class="cdm-add-to-cart" data-id="'.$r[0]['id'].'"><span class="dashicons 
dashicons-cart cdm-dashicons"></span>'.get_option('cdm_woocommerce_add_to_cart', 'Add to cart').'</a>';

				}
			
			return $html;
			
		}
		
		
	
	function view_cart( $title, $cart_item, $cart_item_key){
		global $wpdb;
		#print_r($cart_item);
		
		if($cart_item['file_id'] != ''){
			
			 $r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d",  $cart_item['file_id'] ), ARRAY_A);	
			$title = '<a href="javascript:cdmViewFile('. $cart_item['file_id'].')">'.$r[0]['name'].'</a>';
		}
	
	return $title;	
	}
	function view_order( $title,$item, $is_visible){
		global $wpdb;
	
	
		if($item['file_id'] != ''){
			
			 $r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d",  $item['file_id'] ), ARRAY_A);	
			$title = '<a href="javascript:cdmViewFile('. $item['file_id'].')">'.$r[0]['name'].'</a>';
		}
	
	return $title;	
	}
	
	function view_cart_image( $image, $cart_item, $cart_item_key){
		global $wpdb;
		#print_r($cart_item);
		
		if($cart_item['file_id'] != ''){
			
			 $r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d",  $cart_item['file_id'] ), ARRAY_A);	
			$image = cdm_premium_file_list::file_list_image($r[0],'200');
		}
	
	return $image;	
	}
	
	
	function add_order_item_meta( $item_id, $item ){


	wc_add_order_item_meta( $item_id, 'file_id', $item['file_id'] );
	
	}
	function add_oid_to_item($item_id, $values, $cart_item_key){


	wc_add_order_item_meta( $item_id, 'file_id', $values['file_id'] );
	}
	
	function admin_order( $_product, $item, $item_id){
	global $wpdb;


	// Meta data
	
							if($item['file_id'] != '' ){
								$r = $wpdb->get_results($wpdb->prepare("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = %d",  $item['file_id'] ), ARRAY_A);	
						echo '<td>';
						echo '<strong>File:</strong> <a href="javascript:cdmViewFile('. $item['file_id'].')">'.$r[0]['file'].'</a> <br>';
						echo '</td>';
							}
	
}

function edit_profile($html,$r){
	
	
	
	$html .= '<h3 style="margin-bottom:15px">'.__('Product Settings','sp-cdm').'</h3>';
	 $html .='<p><strong>'.__('Increase Price','sp-cdm').'</strong><br> <i>Add an additional cost for this file, the cost will be added to the digital product base price.<br><br> <input style="width:100%"  placeholder="' . __("File Name", "sp-cdm") . '"  type="text" name="product_price"  value="'.cdm_get_meta($r[0]['id'],'product_price',true,'0.00').'"></p>';
	 
	 return $html;
}

function save_profile($file_id,$update){
		global $wpdb;
	
	
	if($_POST['product_price'] != ''){
		
		 cdm_update_meta($file_id,'product_price',$_POST['product_price']);
	}
	
}

function add_custom_price( $cart_object ) {
    $custom_price = 10; // This will be your custome price  
    foreach ( $cart_object->cart_contents as $key => $value ) {
  		if($value['file_id'] != ''){
	    if(cdm_get_meta($value['file_id'],'product_price',true) != false){		
		$value['data']->price = $value['data']->price + cdm_get_meta($value['file_id'],'product_price',true) ;
		}
		}
    }
}
function show_details_price($html,$r){
	
	
	if( get_option('cdm_woocommerce_product') != ''){
	$_product = wc_get_product( get_option('cdm_woocommerce_product'));
	
	
	$_product->get_regular_price();
	$_product->get_sale_price();
	$_product->get_price();
	$price = $_product->get_price();
	$extra_price = cdm_get_meta($r['id'],'product_price',true);
	if($extra_price != false){		
	$price = $price +$extra_price;
	}
	 $html .= '<div class="sp_su_project">
		
		<strong>' . __("Price", "sp-cdm") . ': </strong> '.wc_price($price) .'  <a href="#"  title="'.get_option('cdm_woocommerce_add_to_cart', 'Add to cart').'" class="cdm-add-to-cart" data-id="'.$r['id'].'"><span class="dashicons 
dashicons-cart cdm-dashicons"></span>'.get_option('cdm_woocommerce_add_to_cart', 'Add to cart').'</a><br>
		
		
		</div>';
	}
	return apply_filters('cdm/view_file/edit_fields/product_details',$html,$r);
}

function batch_operations_select($html){
	
	$html .= '<option value="add_to_cart" >' . __("Add To Cart", "sp-cdm") . '</option>';
	
	
	return $html;
}

function batch_operations_select_js($html){
	
	
	$html .= '//Start Woocommerce
				
							if(jQuery("#spcdm_batch_plug").val() == "add_to_cart"){	
								
									var file_id = jQuery(this).attr("data-id");
		 
											jQuery.ajax({					
												type: "POST",
												url: cdm_scripts.ajax_url,
												data: {"action": "cdm_ajax_woocommerce_add_to_cart_batch",
													   "post":jQuery("#cdm_wrapper_form").serialize(), 
													  },		
												success: function (msg) { 					
													obj = jQuery.parseJSON( msg );
													console.log(obj);
													if(obj.error == 1){
													alert(obj.cart_message);	
													}else{
										jQuery(".cdm-woocommerce-message").html(\'<div class="modal-error"><h2>\'+obj.cart_message+\'</h2><a href="\'+obj.cart_url+\'" class="cdm_button">View Cart</a> <a href="" class="cdm_button cdm-refresh-files" >Back to files</a> </div>\');
													cdmOpenModal("cdm-woocommerce-message");
													
													}
													
												}
										
											});	
							}
							
				//End Woocommerce';
	
	
return $html;	
}

	function add_to_cart_ajax_batch(){
			
		global $woocommerce;
		parse_str($_POST['post'], $output);
	
		$data['error'] =0;
		if(is_array($output['download']['file']) && count($output['download']['file'])>0){
		
		foreach($output['download']['file'] as $file){
			
			$data['cart_url'] = $woocommerce->cart->get_cart_url();
			$data['cart_message'] = get_option('cdm_woocommerce_added_to_cart', 'Added to cart');
			$data['file_id'][] =  $file;
			$product_id = get_option('cdm_woocommerce_product') ;
			$quantity = 1;
			$variation_id = '';
			$variation = array();
			$cart_item_data = 		array(
								'file_id' =>  $file
								);
			
			 WC()->cart->add_to_cart( $product_id, $quantity , $variation_id , $variation,$cart_item_data);
				
		}
		}else{
		$data['error'] =1;
		$data['cart_message'] = __('No items to purchase!','sp-cdm');	
		}
		echo json_encode($data); 
		exit;	
			
		}
		
		function bottom(){
			
			echo '<div style="display:none">
				<div class="remodal" data-remodal-id="cdm-woocommerce-message">
				<div class="cdm-woocommerce-message"></div>
				</div>
				</div>';
			
			
		}
}

$sp_cdm_woocommerce = new sp_cdm_woocommerce;

#CDM filters


add_filter('cdm/batch_operations/select/js',array(	$sp_cdm_woocommerce, 'batch_operations_select_js'));
add_filter('cdm/batch_operations/select/options',array(	$sp_cdm_woocommerce, 'batch_operations_select'));
add_filter('sp_cdm_file_view_info',array(	$sp_cdm_woocommerce, 'show_details_price') ,10,2);
add_action('cdm/view_file/ajax/save',array(	$sp_cdm_woocommerce, 'save_profile') ,10,2);
add_action('cdm/view_file/edit_fields',array(	$sp_cdm_woocommerce, 'edit_profile') ,10,2);
add_action('sp_cdm_disable_features',array($sp_cdm_woocommerce,'disable_features'));
add_action('cdm/viewfile/top_navigation',array($sp_cdm_woocommerce,'add_cart_button'),10,2);	
add_action('sp_cdm_bottom_uploader_page',array($sp_cdm_woocommerce,'bottom'));	

#Woocommerce Filters
add_action('woocommerce_admin_order_item_values',array(	$sp_cdm_woocommerce, 'admin_order') ,10,3);
add_action( 'woocommerce_before_calculate_totals',  array(	$sp_cdm_woocommerce, 'add_custom_price' ));
add_action( 'woocommerce_add_order_item_meta', array(	$sp_cdm_woocommerce, 'add_oid_to_item'),10,3);
add_action( 'woocommerce_ajax_add_order_item_meta',array(	$sp_cdm_woocommerce, 'add_order_item_meta'), 10,2);
add_filter('woocommerce_cart_item_name', array($sp_cdm_woocommerce,'view_cart'),10,3);
add_filter('woocommerce_order_item_name', array($sp_cdm_woocommerce,'view_order'),10,3);
add_filter('woocommerce_cart_item_thumbnail', array($sp_cdm_woocommerce,'view_cart_image'),10,3);

#wordpress ajax
add_action( 'wp_ajax_cdm_ajax_woocommerce_add_to_cart',  array($sp_cdm_woocommerce,'add_to_cart_ajax'));
add_action( 'wp_ajax_nopriv_cdm_ajax_woocommerce_add_to_cart', array($sp_cdm_woocommerce,'add_to_cart_ajax'));

add_action( 'wp_ajax_cdm_ajax_woocommerce_add_to_cart_batch',  array($sp_cdm_woocommerce,'add_to_cart_ajax_batch'));
add_action( 'wp_ajax_nopriv_cdm_ajax_woocommerce_add_to_cart_batch', array($sp_cdm_woocommerce,'add_to_cart_ajax_batch'));
		

}