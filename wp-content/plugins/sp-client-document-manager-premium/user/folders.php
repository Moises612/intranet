<?php
$sp_cdm_premium_folder_settings = new sp_cdm_premium_folder_settings;
add_action('sp_cdm_edit_project_main_form', array($sp_cdm_premium_folder_settings,'folder_template_settings'));
add_action('sp_cdm_edit_project_save', array($sp_cdm_premium_folder_settings,'folder_template_settings_save'));

add_action( 'user_register', array($sp_cdm_premium_folder_settings,'create_user_folder'), 10, 1 );
#add_action( 'wpmu_new_user', array($sp_cdm_premium_folder_settings,'create_user_folder'), 10, 1 );
add_action('sp_cdm_edit_project_main_form',array($sp_cdm_premium_folder_settings,'sp_cdm_projects_admin_expiration_date'));
add_action('sp_cdm_edit_project_save', array($sp_cdm_premium_folder_settings,'sp_cdm_projects_admin_expiration_date_save'));
add_action('init',array($sp_cdm_premium_folder_settings,'expiration_date_cron'));
add_action( 'wp',array($sp_cdm_premium_folder_settings,'sp_cdm_expiration_schedule_cron'));
add_action( 'sp_cdm_expiration_schedule_cron',array($sp_cdm_premium_folder_settings,'expiration_date_cron'));
class sp_cdm_premium_folder_settings{
	function dateDiff($start, $end) {
$start = strtotime($start);
$end = strtotime( $end);

$days_between = ceil(abs($end - $start) / 86400);
return $days_between ;
}
	  

function sp_cdm_expiration_schedule_cron() {
	if ( ! wp_next_scheduled( 'sp_cdm_expiration_schedule_cron' ) ) {
		wp_schedule_event( time(), 'daily', 'sp_cdm_expiration_schedule_cron');
	}
}

  
	    function delete_file($id)
    {
        global $wpdb, $current_user;
        $r = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu   where id = '" . $id . "'  order by date desc", ARRAY_A);
  
            $wpdb->query("

	DELETE FROM " . $wpdb->prefix . "sp_cu WHERE id = " . $id  . "

	");
            unlink('' . SP_CDM_UPLOADS_DIR . '' . $r[0]['uid'] . '/' . $r[0]['file'] . '');
			        $ext        = preg_replace('/^.*\./', '', $r[0]['file']);
					$small = '' . SP_CDM_UPLOADS_DIR . '' . $r[0]['uid'] . '/'.$r[0]['file'].'_small.png';
					$big = '' . SP_CDM_UPLOADS_DIR . '' . $r[0]['uid'] . '/'.$r[0]['file'].'_big.png';
			@unlink($small);
			@unlink($big);
      
    }
	function expiration_date_cron(){
		
		global $wpdb;
		
		
			      
		 $query = "SELECT * FROM  " . $wpdb->prefix . "options where option_name LIKE  'sp_cdm_project_retention_date_%'";
										
	 	  $projects_info = $wpdb->get_results($query, ARRAY_A);
		
			for ($i = 0; $i < count($projects_info ); $i++) {
				 $expiration = $projects_info[$i]['option_name'];
			     $expiration_time = $projects_info[$i]['option_value'];
				 $project_array = explode('sp_cdm_project_retention_date_', $expiration);
				# print_r(  $project_array);
				 $project_id = $project_array[1];	
				#echo "SELECT * FROM  " . $wpdb->prefix . "sp_cu where pid = ".$project_id."";
					$r = $wpdb->get_results("SELECT * FROM  " . $wpdb->prefix . "sp_cu where pid = ".$project_id."", ARRAY_A);
					
						for ($j = 0; $j < count($r ); $j++) {
								
								$diff= $this->dateDiff($r[$j]['date'],date("Y-m-d"));
								if($diff >$expiration_time){
								$this-> delete_file($r[$j]['id']);	
								#echo 'Remove 	'.$r[$j]['file'].': '.$diff.' Days Old<br>';
								}
								
								unset($diff);
								
						}
				
			}
		
	}
 function sp_cdm_projects_admin_expiration_date_save($id)
    {
        global $wpdb;
		
		if ($_POST['save-project'] != "" && $_POST['retention'] != '') {
			
		update_option('sp_cdm_project_retention_date_' . $id . '', $_POST['retention']);
			
		}
	
	
 }
function sp_cdm_projects_admin_expiration_date($r){
	
	
	if($r[0]['id'] != ''){
	$retention = 	get_option('sp_cdm_project_retention_date_' . $r[0]['id'] . '');	
		
	}
	
	echo ' <tr>

    <td width="200"><strong>' . __("File Retention Period:", "sp-cdm") . '</strong> <em>Setting a file retention period will delete files in this folder after the set amount of days. This only deletes files and not folders and does not work recursively through each sub folder. Leaving this setting blank will preserve all files forever.</em></td>

    <td><input type="text" name="retention" value="' .$retention. '"> Days</td>

  </tr>

  <tr>';
	
}
	




function folder_template_settings_save($id){
	
	
	update_option('cdm_folder_template_'.$id.'', $_POST['folder-template']);
	
}
function folder_template_settings($r){
	
	
	global $wpdb;
	
	
	 if($r[0]['id'] != ''){
	 $folder_template = get_option('cdm_folder_template_'.$r[0]['id'].'');
	 
	 if(	 $folder_template  == 1){
		$checked = 'checked="checked';
	 }else{
		$checked = ''; 
	 }
	 
	 }
	 
	/*
	echo '
	

  <tr>

    <td width="200">' . __("Make A Template?:", "sp-cdm") . '<br><em>Making this folder a template will add this folder and all sub folders to a new user once created so when the user first logs into their client area they will see a list of folders by default. Folders must be deleted manually once the user is removed</em></td>

    <td><input type="checkbox" name="folder-template" value="1" '.$checked.'></td>

  </tr>
 
  </table>
';
*/
	
}

	function copy_files($vars,$pid,$user_id ){
		 global $wpdb, $current_user;
			 
			 
			 
			  
        $dir = '' . SP_CDM_UPLOADS_DIR . '' . $user_id . '/';
        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }
			 
			 
			 //folders
			 if(count($vars['folder']) > 0){
				
				
				 $folders_id = implode(',',$vars['folder']);	
				$folders   = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_project where id  IN(".$folders_id.") order by name ", ARRAY_A);
		
				
					 for ($j = 0; $j < count($folders); $j++) {
							
							$update_folder['name'] =$folders[$j]['name'];
							$update_folder['uid'] = $user_id;
							#$update_folder['permissions'] = $folders[$j]['permissions'];
							$update_folder['parent'] = $pid;	
							
							foreach($update_folder as $key=>$value){ if(is_null($value)){ unset($update_folder[$key]); } }					
							$wpdb->insert("" . $wpdb->prefix . "sp_cu_project",$update_folder);
							$this->copy_sub_folder($folders[$j]['id'],$wpdb->insert_id, $user_id);
							unset($update_folder);	
					 }
				 
							 
			 }
			 
			 //folders
			 
			 
			 //files
			 if(count($vars['file']) > 0){
				$files_id = implode(',',$vars['file']);
			 	$files   = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu where id IN(".$files_id.") ", ARRAY_A);
				
				
				
				//remove files from server
				 for ($i = 0; $i < count($files); $i++) {
							
						
							$update_file['pid'] = $pid;							
							
							$copy_from = '' . SP_CDM_UPLOADS_DIR . '' .$files[$i]['uid']. '/' .$files[$i]['file']. '';
							$new_file = $this->rename_file($copy_from);
							$copy_to = '' . SP_CDM_UPLOADS_DIR . '' .$user_id. '/' .$new_file. '';
							
							
							$insert['name'] = $files[$i]['name'];
							$insert['notes'] = $files[$i]['notes'];
							$insert['tags'] = $files[$i]['tags'];
							$insert['uid'] = $user_id;
							$insert['cid'] = $files[$i]['cid'];
							$insert['parent'] = $files[$i]['parent'];
							$insert['pid'] = $pid;
							$insert['file'] = $new_file;							
							
							copy($copy_from, $copy_to);
							 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
							$wpdb->insert("" . $wpdb->prefix . "sp_cu",$insert);
						
						unset($insert);
				 }
			 	//remove the database entries
			
			 
			 }
			 //end files
			
	}
	
		function copy_sub_folder($id,$parent,$uid){
			 global $wpdb, $current_user;
			 
			$folders = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_project where parent  = '".$id."' ", ARRAY_A); 
		
			//copy files
						$files   = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu where pid = '".$id."'", ARRAY_A);
					
						if(count($files) >0){
						 for ($i = 0; $i < count($files); $i++) {
										
											
																
										
										$copy_from = '' . SP_CDM_UPLOADS_DIR . '' .$files[$i]['uid']. '/' .$files[$i]['file']. '';
										$new_file = $this->rename_file($copy_from);
										$copy_to = '' . SP_CDM_UPLOADS_DIR . '' .$uid. '/' .$new_file. '';
										
										
										$insert['name'] = $files[$i]['name'];
										$insert['notes'] = $files[$i]['notes'];
										$insert['tags'] = $files[$i]['tags'];
										$insert['uid'] = $uid;
										$insert['cid'] = $files[$i]['cid'];
										
										$insert['pid'] = $parent;
										$insert['file'] = $new_file;							
										
										copy($copy_from, $copy_to);
										 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
										$wpdb->insert("" . $wpdb->prefix . "sp_cu",$insert);
										
									unset($insert);
							 }
						}
							
			
			
			
			
			if(count($folders)>0){
						
				for ($j = 0; $j < count($folders); $j++) {
			
				
			
			
						
							
							
							
							//copy folder and check for subs
							$update_folder['name'] =$folders[$j]['name'];
							$update_folder['uid'] = $uid;
							#$update_folder['permissions'] = $folders[$j]['permissions'];
							$update_folder['parent'] = $parent;			
							foreach($update_folder as $key=>$value){ if(is_null($value)){ unset($update_folder[$key]); } }						
							$wpdb->insert("" . $wpdb->prefix . "sp_cu_project",$update_folder);
							$this->copy_sub_folder($folders[$j]['id'],$wpdb->insert_id,$uid);	
					unset($update_folder);
				}
			
			
			
				
			}
		}

	function rename_file($file){
			
			$path_parts = pathinfo($file);
			
			return ''.$path_parts['filename'].'_'.time().'.'.$path_parts['extension'].'';
			
		}	
function create_user_folder( $user_id ){
		global $wpdb;
		
		
	
		if(get_option('sp_cu_user_create_folder') == 1){
				
		$user =  get_userdata( $user_id );
		
		if(get_option('sp_cu_user_create_folder_name') == 1){
			
			if($user->user_firstname != '' && $user->user_lastname != ''){
			$insert['name'] = ''.$user->user_lastname.'_'.$user->user_firstname.'';
			}else{
			$insert['name'] =$user->display_name;
			}

			$insert['uid'] =  $user_id ;
 foreach($insert as $key=>$value){ if(is_null($value)){ unset($insert[$key]); } }
			$wpdb->insert( "".$wpdb->prefix . "sp_cu_project",$insert );
			$insert_id = $wpdb->insert_id;
			
			
		}else{
			
			$insert_id = 0;	
		}
			if(get_option('sp_cu_user_create_folder_template') != ''){
					
				$folders = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu_project where parent  = '".get_option('sp_cu_user_create_folder_template')."' ", ARRAY_A); 
				
					for ($i = 0; $i < count($folders); $i++) {						
					$vars['folder'][]  = 	$folders[$i]['id'];
					}
				
				$files = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "sp_cu where pid  = '".get_option('sp_cu_user_create_folder_template')."' ", ARRAY_A); 	
					for ($i = 0; $i < count($files); $i++) {						
					$vars['file'][]  = 	$files[$i]['id'];
					}
			
		
				 $this->copy_files($vars,$insert_id,$user_id );
			
			
			}
			
			do_action('sp_cdm_auto_create_folder',$wpdb->insert_id);		
			
		}
			
	
	
}
}


	
?>