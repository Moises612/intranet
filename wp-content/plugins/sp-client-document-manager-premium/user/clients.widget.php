<?php
// register Foo_Widget widget
function register_sp_cdm_clients_widget() {
    register_widget( 'sp_cdm_clients_widget' );
}
add_action( 'widgets_init', 'register_sp_cdm_clients_widget' );

/**
 * Adds Foo_Widget widget.
 */
class sp_cdm_clients_widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'sp_cdm_clients_widget', // Base ID
			__( 'CDM: Clients Widget', 'text_domain' ), // Name
			array( 'description' => __( 'This widget displays the client tree for client document manager.', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		global $wpdb,$current_user,$sp_cdm_clients;
     	        echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		
		$clients = $sp_cdm_clients->find_my_clients();
		if(count($clients)>0){
				echo '<ul class="sp-cdm-client-widget">';
				$your_files = '<a href="#" class="sp-cdm-load-your-files fa fa-folder">'.__('Your Files', 'sp-cdm').'</a>';
				echo  apply_filters('sp_cdm/premium/widget/clients/your_files', $your_files);
			foreach($clients as $client_id){
					
					echo '<li>';
					
					do_action('sp_cdm_client_widget_before_group_loop', $client_id);
					#sp-cdm-load-client-files
					echo'<a href="#" class="fa fa-folder sp-cdm-load-client-files" data-id="'.$client_id.'">'.$sp_cdm_clients->get_client_name($client_id).'</a>';
					
					do_action('sp_cdm_client_widget_after_group_loop', $client_id);
					
					echo '</li>';	
				
			}
			echo '</ul>';
		}
	;
		
		
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
     	        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Groups', 'text_domain' );
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Foo_Widget

