<?php
  include 'db.php';
  //include (TEMPLATEPATH ."/scripts/db.php"); 
  $database = new DB();
  mysql_query("SET NAMES 'utf8'");
  $sql_unidades = "SELECT wu.id, wu.descripcion  FROM wp_unidad wu";
  $unidades = $database->select($sql_unidades);
?>
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Webslesson Tutorial | Make accordion by using Bootstrap Collapse and Panels with PHP Script</title>  
           <script src="libs/jquery/jquery.js"></script>  
           <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css" />  
           <script src="libs/bootstrap/js/bootstrap.js"></script>  
           <script src="script.js"></script>
      </head>  
      <body>
        <div class="container" id="main">
          <h2 class="page-header">Directorio</h2>
          <form>
			<input id="searchTerm" placeholder="Buscar ..." class="form-control" type="text" />
	      </form>
          </br>
          <?php
          $i=0;
          foreach ($unidades as $value) {
            $i++;
            $id= $value['id'];
            $descripcion = $value['descripcion'];
            echo "
              <div class='panel-group' id='accordion".$i."'>
                <div class='panel panel-default' id='tab".$i."'>
                  <div class='panel-heading'>
                    <h4 class='panel-title'>
                      <a data-toggle='collapse' data-parent='#accordion".$i."' href='#collapse".$i."'>"
                           .$descripcion."
                      </a>
                    </h4>
                  </div>
                  <div id='collapse".$i."' class='panel-collapse collapse'>
                    <div class='panel-body'>
                        <table class='table table-bordered' id='tab".$i."'>
                           <tr>
                              <th>Nombres</th>
                              <th>Cargo</th>
                              <th>Telefono</th>
                           </tr>";
                          $sql_contactos = "SELECT nombres, cargo, telefono FROM wp_contacto where id_unidad='$id'";
                          $contactos = $database->select($sql_contactos);
                          foreach ($contactos as $li) {
                            $nombres = $li['nombres'];
                            $cargo = $li['cargo'];
                            $telefono = $li['telefono'];
                            echo 
                            "<tr>
                              <td>".$nombres."</td>
                              <td>".$cargo."</td>
                              <td>".$telefono."</td>
                             </tr>";
                           }
                          echo 
                        "</table>
                    </div>
                  </div>
                </div>
              </div>";
          }
        ?>
        </div>
      </body>
  </html>

              
      
